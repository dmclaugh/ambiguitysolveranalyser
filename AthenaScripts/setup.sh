setupATLAS
cd build
asetup 21.0.77,Athena,here
cmake ../athena/Projects/WorkDir
make
cd ../run
source ../build/*/setup.sh
nohup sh run.sh

<h1> Setting up sparse athena </h1>

<h3> Cloning Athena with ambi changes </h3>
Within the AmbiScripts directory:

<h3> Building Athena </h3>

Building for first time (this will clone the athena sparse checkout repo and build it):
```
source setupOnce.sh
```

Building after the initial build change to the appropiate branch and run the setup (each time there is a change to athena):
```
cd athena
git checkout <Branch Name e.g. NominalTracks>
cd ../
source setup.sh
```

<h3> Run Athena Locally </h3>

- `nohup sh run.sh`

<h3> Run Athena On the Grid </h3>

Setup up panda and rucio: 
- `lsetup panda rucio; voms-proxy-init -voms atlas` and then: <br> 
- `nohup sh run_grid.sh`

**N.B.** Change:
- `--fileList` to input dataset, 
- `--outDS` to your own CERN username:output_name, 
- `--nEventsPerFile` or `--maxEvents` to how many events you want.



<h3> Changing Branches </h3>

Branch names:

- BIdentifier:          Ambi changes with B identifer inserted before ambi.
- NominalTracks:       Ambi changes with nominal cuts and SiSp track candidates as ambi input.
- PseudoTrackStudies:    Ambi changes with nominal cuts and Pseudo track candidates as ambi input.

``` 
cd athena
git checkout <branch name>
```

<h3> Key files for Ambiguity Solving </h3>

- InnerDetector/InDetRecTools/InDetAmbiTrackSelectionTool/src/InDetDenseEnvAmbiTrackSelectionTool.cxx
- InnerDetector/InDetRecTools/InDetTrackScoringTools/src/InDetAmbiScoringTool.cxx
- Tracking/​TrkTools/​TrkAmbiguityProcessor/​src/​DenseEnvironmentsAmbiguityProcessorTool.cxx

Changing cuts:
- InnerDetector/InDetExample/InDetRecExample/share/ConfiguredNewTrackingSiPattern.py
- InnerDetector/​InDetExample/​InDetRecExample/​python/​ConfiguredNewTrackingCuts.py
- InnerDetector/InDetRecTools/InDetAmbiTrackSelectionTool/src/InDetDenseEnvAmbiTrackSelectionTool.cxx

<h3> Sparse checkout key tools </h3>

``` 
git atlas addpkg InnerDetector/InDetExample/InDetRecExample
git atlas addpkg Tracking/TrkTools/TrkAmbiguityProcessor
git atlas addpkg InnerDetector/InDetRecTools/InDetAmbiTrackSelectionTool
git atlas addpkg PhysicsAnalysis/DerivationFramework/DerivationFrameworkInDet
git atlas addpkg Tracking/TrkValidation/TrkValTools
git atlas addpkg Tracking/TrkValidation/TrkValInterfaces
git atlas addpkg InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD
git atlas addpkg Tracking/TrkTools/TrkToolInterfaces
git atlas addpkg InnerDetector/InDetRecTools/InDetTrackScoringTools
``` 

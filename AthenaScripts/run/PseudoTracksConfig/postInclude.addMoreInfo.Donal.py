# Don't think this is needed as only running on simulation for these tests
#from AthenaCommon.GlobalFlags import globalflags
#from InDetRecExample.InDetJobProperties import InDetFlags
#InDetFlags.doTruth = (globalflags.DataSource == 'geant4' and globalflags.InputFormat() == 'pool')


# add pixel hit info
from InDetPrepRawDataToxAOD.InDetPrepRawDataToxAODConf import PixelPrepDataToxAOD
xAOD_PixelPrepDataToxAOD = PixelPrepDataToxAOD( name = "xAOD_PixelPrepDataToxAOD")
xAOD_PixelPrepDataToxAOD.OutputLevel=INFO
xAOD_PixelPrepDataToxAOD.UseTruthInfo = True
print "Add Pixel xAOD PrepRawData:"
print xAOD_PixelPrepDataToxAOD
topSequence += xAOD_PixelPrepDataToxAOD

# add SCT hit info
from InDetPrepRawDataToxAOD.InDetPrepRawDataToxAODConf import SCT_PrepDataToxAOD
xAOD_SCT_PrepDataToxAOD = SCT_PrepDataToxAOD( name = "xAOD_SCT_PrepDataToxAOD")
xAOD_SCT_PrepDataToxAOD.OutputLevel=INFO
xAOD_SCT_PrepDataToxAOD.UseTruthInfo=True
print "Add SCT xAOD TrackMeasurementValidation:"
print xAOD_SCT_PrepDataToxAOD
topSequence += xAOD_SCT_PrepDataToxAOD

augmentationTools = []

FlagStoreTRT = False

# add msos to reco tracks
from DerivationFrameworkInDet.DerivationFrameworkInDetConf import DerivationFramework__TrackStateOnSurfaceDecorator
DFTSOS = DerivationFramework__TrackStateOnSurfaceDecorator(name = "DFTrackStateOnSurfaceDecorator",
                                                           ContainerName = InDetKeys.xAODTrackParticleContainer(),
                                                           DecorationPrefix = "",
                                                           TRT_ToT_dEdx = "",
                                                           #PixelMsosName = "Reco_Pixel_MSOSs",
                                                           #SctMsosName = "Reco_SCu_MSOSs",
                                                           #TrtMsosName = "Reco_TRT_MSOSs",
                                                           StoreTRT = FlagStoreTRT,
                                                        #    StoreHoles = False,
                                                           OutputLevel = INFO)
ToolSvc += DFTSOS
augmentationTools += [DFTSOS]


# add msos to pseudo tracks
DFTSOS_PT = DerivationFramework__TrackStateOnSurfaceDecorator(name = "DFPTTrackStateOnSurfaceDecorator",
                                                           ContainerName = InDetKeys.xAODPseudoTrackParticleContainer(),
                                                           DecorationPrefix = "Pseudo_",
                                                           PixelMsosName = "Pseudo_Pixel_MSOSs",
                                                           SctMsosName = "Pseudo_SCT_MSOSs",
                                                           TrtMsosName = "Pseudo_TRT_MSOSs",
                                                           TRT_ToT_dEdx = "",
                                                           StoreTRT = FlagStoreTRT,
                                                        #    StoreHoles = False,
                                                           OutputLevel =INFO)
ToolSvc += DFTSOS_PT
augmentationTools += [DFTSOS_PT]


# add msos to candidates - do not have Trk::Track links so does not give MSOS?
#Turn off SiSP
# DFTSOS_SiSpCand = DerivationFramework__TrackStateOnSurfaceDecorator(name = "DFSiSpCandTrackStateOnSurfaceDecorator",
#                                                            ContainerName = "SiSpTrackCandidatesTrackParticle",
#                                                            DecorationPrefix = "SiSpCand_",
#                                                            PixelMsosName = "SiSpCand_Pixel_MSOSs",
#                                                            SctMsosName = "SiSpCand_SCT_MSOSs",
#                                                            TrtMsosName = "SiSpCand_TRT_MSOSs",
#                                                            TRT_ToT_dEdx = "",
#                                                            AddPulls = False,
#                                                            StoreTRT = False,
#                                                            StoreHoles = False,
#                                                            OutputLevel =INFO)
# ##DFTSOS_SiSpCand.OutputLevel=10000
# #Turn off SiSP
# ToolSvc += DFTSOS_SiSpCand
# augmentationTools += [DFTSOS_SiSpCand]

## FOR DONAL
DFTSOS_ObservedTrack = DerivationFramework__TrackStateOnSurfaceDecorator(name = "DFObservedTrackStateOnSurfaceDecorator",
                                                           ContainerName = "InDetObservedTrackParticles",
                                                           DecorationPrefix = "ObservedTrack_",
                                                           PixelMsosName = "ObservedTrack_Pixel_MSOSs",
                                                           SctMsosName = "ObservedTrack_SCT_MSOSs",
                                                           TrtMsosName = "ObservedTrack_TRT_MSOSs",
                                                           TRT_ToT_dEdx = "",
                                                           StoreTRT = False,
                                                           AddPulls = False,
                                                           StoreHoles = False,
                                                           OutputLevel = INFO)
ToolSvc += DFTSOS_ObservedTrack
augmentationTools += [DFTSOS_ObservedTrack]


# Add decoration with truth parameters if running on simulation
#from DerivationFrameworkInDet.DerivationFrameworkInDetConf import DerivationFramework__TrackParametersForTruthParticles
#TruthDecor = DerivationFramework__TrackParametersForTruthParticles(name = "TruthTPDecor",
#                                                                   DecorationPrefix = "")
#ToolSvc += TruthDecor
#augmentationTools += [TruthDecor]
#print TruthDecor

                   
                   
DerivationFrameworkJob = CfgMgr.AthSequencer("MySeq2")
from DerivationFrameworkCore.DerivationFrameworkCoreConf import DerivationFramework__CommonAugmentation
DerivationFrameworkJob += CfgMgr.DerivationFramework__CommonAugmentation("DFTSOS_KERN",
                                                                         AugmentationTools = augmentationTools,
                                                                         OutputLevel = INFO)
                                                 
topSequence += DerivationFrameworkJob


StreamAOD.ForceRead=True
StreamAOD.ItemList+=["xAOD::TrackParticleContainer#InDetPseudoTrackParticles",
        "xAOD::TrackParticleAuxContainer#InDetPseudoTrackParticlesAux.",
        "xAOD::TrackParticleContainer#InDetIdealTrackParticles",
        "xAOD::TrackParticleAuxContainer#InDetIdealTrackParticlesAux."]

#Turn off SiSP
# StreamAOD.ItemList+=["xAOD::TrackParticleContainer#SiSpTrackCandidatesTrackParticle"] #Doal
# StreamAOD.ItemList+=["xAOD::TrackParticleAuxContainer#SiSpTrackCandidatesTrackParticleAux."] #donal
 
StreamAOD.ItemList+=["xAOD::TrackParticleContainer#InDetObservedTrackParticles"]
StreamAOD.ItemList+=["xAOD::TrackParticleAuxContainer#InDetObservedTrackParticlesAux."]

StreamAOD.ItemList+=["xAOD::TrackParticleContainer#InDetObservedTrackParticles"]
StreamAOD.ItemList+=["xAOD::TrackParticleAuxContainer#InDetObservedTrackParticlesAux."]

StreamAOD.ItemList+=["xAOD::TrackParticleContainer#ObservedTracks"]
StreamAOD.ItemList+=["xAOD::TrackParticleAuxContainer#ObservedTracksAux."]

#HitInfoStream.AddItem("xAOD::TrackParticleAuxContainer#SiSpTrackCandidatesTrackParticleAux.-caloExtension.-cellAssociation.-clusterAssociation.-trackParameterCovarianceMatrices.-parameterX.-parameterY.-parameterZ.-parameterPX.-parameterPY.-parameterPZ.-parameterPosition")

StreamAOD.ItemList+=["xAOD::TruthEventContainer#*"]
StreamAOD.ItemList+=["xAOD::TruthEventAuxContainer#*"]
StreamAOD.ItemList+=["xAOD::TruthPileupEventContainer#*"]
StreamAOD.ItemList+=["xAOD::TruthPileupEventAuxContainer#*"]
StreamAOD.ItemList+=["xAOD::TruthParticleContainer#*"]
StreamAOD.ItemList+=["xAOD::TruthParticleAuxContainer#"]
StreamAOD.ItemList+=["xAOD::TruthVertexContainer#*"]
StreamAOD.ItemList+=["xAOD::TruthVertexAuxContainer#*"]

# the following is for the additional information added in postInclude.addMoreInfo.py
#StreamAOD.ItemList+=["xAOD::TrackParticleContainer#InDetTrackParticles"]
#StreamAOD.ItemList+=["xAOD::TrackParticleAuxContainer#InDetTrackParticlesAux.-caloExtension.-cellAssociation.-clusterAssociation.-trackParameterCovarianceMatrices.-parameterX.-parameterY.-parameterZ.-parameterPX.-parameterPY.-parameterPZ.-parameterPosition"]
StreamAOD.ItemList+=["xAOD::TrackMeasurementValidationContainer#*"]
StreamAOD.ItemList+=["xAOD::TrackMeasurementValidationAuxContainer#*"]
StreamAOD.ItemList+=["xAOD::TrackStateValidationContainer#*"]
StreamAOD.ItemList+=["xAOD::TrackStateValidationAuxContainer#*"]
StreamAOD.ItemList+=["xAOD::TrackParticleClusterAssociationContainer#*"]
StreamAOD.ItemList+=["xAOD::TrackParticleClusterAssociationAuxContainer#*"]

# SiSp business - goes with InDetFlags.doStoreTrackCandidates in RAW and AOD step


#Turn off SiSP
# StreamAOD.ItemList+=["xAOD::SiSPS_ResolvedTrackConnection#*"] #donal
# StreamAOD.ItemList+=["xAOD::SiSPSeededTracksTruthCollection#*"]


# for pseudo-tracks
#StreamAOD.ItemList+=["xAOD::TrackParticleContainer#InDetPseudoTrackParticles"]
#StreamAOD.ItemList+=["xAOD::TrackParticleAuxContainer#InDetPseudoTrackParticlesAux.-caloExtension.-cellAssociation.-clusterAssociation.-trackParameterCovarianceMatrices.-parameterX.-parameterY.-parameterZ.-parameterPX.-parameterPY.-parameterPZ.-parameterPosition"]

# for ideal-tracks
#StreamAOD.ItemList+=["xAOD::TrackParticleContainer#InDetIdealTrackParticles"]
#StreamAOD.ItemList+=["xAOD::TrackParticleAuxContainer#InDetIdealTrackParticlesAux.-caloExtension.-cellAssociation.-clusterAssociation.-trackParameterCovarianceMatrices.-parameterX.-parameterY.-parameterZ.-parameterPX.-parameterPY.-parameterPZ.-parameterPosition"]


# print("STREAM TO AOD")
# print(StreamAOD.ItemList)

# Create xAOD tracks for SiSp tracks (some bug in JOs so flag does not work - do it here instead)
#Turn off SiSP
from xAODTrackingCnv.xAODTrackingCnvConf import xAODMaker__TrackParticleCnvAlg
xAODTrkCanTrackParticleCnvAlg = xAODMaker__TrackParticleCnvAlg( InDetKeys.xAODSiSPTrackCandidates()+"TrackParticle" )
xAODTrkCanTrackParticleCnvAlg.xAODContainerName = InDetKeys.xAODSiSPTrackCandidates()+"TrackParticle"
xAODTrkCanTrackParticleCnvAlg.xAODTrackParticlesFromTracksContainerName = InDetKeys.xAODSiSPTrackCandidates()+"TrackParticle"
xAODTrkCanTrackParticleCnvAlg.TrackParticleCreator = InDetxAODParticleCreatorTool
xAODTrkCanTrackParticleCnvAlg.TrackContainerName = InDetKeys.SiSpSeededTrackCandidates()
xAODTrkCanTrackParticleCnvAlg.TrackTruthContainerName = InDetKeys.SiSpSeededTrackCandidates()+'TruthCollection'
xAODTrkCanTrackParticleCnvAlg.ConvertTrackParticles = doConversion
xAODTrkCanTrackParticleCnvAlg.ConvertTracks = doCreation
xAODTrkCanTrackParticleCnvAlg.AddTruthLink = InDetFlags.doTruth()

# xAODTrkCanTrackParticleCnvAlg.PrintIDSummaryInfo = True
#xAODTrkCanTrackParticleCnvAlg.OutputLevel = VERBOSE

#Turn off SiSP
topSequence += xAODTrkCanTrackParticleCnvAlg


# get the observed tracks with truth information
ObservedOut = "ObservedTrackCollection"
InDetTracksTruth = ConfiguredInDetTrackTruth(ObservedOut,
        "InDetObservedTrackDetailedTruth",
        "InDetObservedTrackTruthCollection",
        PixelClusterTruth,
        SCT_ClusterTruth,
        TRT_DriftCircleTruth)

from xAODTrackingCnv.xAODTrackingCnvConf import xAODMaker__TrackParticleCnvAlg
xAODObservedTrackParticleCnvAlg = xAODMaker__TrackParticleCnvAlg("InDetObservedTrackParticles")
xAODObservedTrackParticleCnvAlg.xAODContainerName = "InDetObservedTrackParticles"
xAODObservedTrackParticleCnvAlg.xAODTrackParticlesFromTracksContainerName = "InDetObservedTrackParticles"
xAODObservedTrackParticleCnvAlg.TrackContainerName = ObservedOut
xAODObservedTrackParticleCnvAlg.TrackParticleCreator = InDetxAODParticleCreatorTool
xAODObservedTrackParticleCnvAlg.AODContainerName = ObservedOut
xAODObservedTrackParticleCnvAlg.AODTruthContainerName = InDetKeys.TrackParticlesTruth()
xAODObservedTrackParticleCnvAlg.ConvertTrackParticles = doConversion
xAODObservedTrackParticleCnvAlg.ConvertTracks = doCreation
xAODObservedTrackParticleCnvAlg.AddTruthLink = InDetFlags.doTruth()
xAODObservedTrackParticleCnvAlg.TrackTruthContainerName = "InDetObservedTrackTruthCollection"
# xAODObservedTrackParticleCnvAlg.PrintIDSummaryInfo = True
xAODObservedTrackParticleCnvAlg.OutputLevel = INFO

topSequence += xAODObservedTrackParticleCnvAlg




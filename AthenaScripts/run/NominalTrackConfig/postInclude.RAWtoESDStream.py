#print("PRINTING SOME DEBUG")
#print(StreamESD.ItemList)
#print("ADDING TO ESD")
StreamESD.ForceRead=True
StreamESD.ItemList+=["InDetSimDataCollection#*",
        "SiHitCollection#*",
        "SCT_RDO_Container#SCT_RDOs",
        "TrackTruthCollection#InDetIdealTrackTruthCollection",
        "DetailedTrackTruthCollection#InDetIdealTrackDetailedTruth",
        "TrackCollection#InDetIdealTracks",
        "xAOD::TrackParticleContainer#InDetIdealTrackParticles",
        "xAOD::TrackParticleAuxContainer#InDetIdealTrackParticlesAux.",
        "xAOD::AuxContainerBase#InDetIdealTrackParticlesAux.",
        "xAOD::IParticleContainer#InDetIdealTrackParticles",
        "TrackTruthCollection#SiSPSeededTrackTruthCollection",
        "DetailedTrackTruthCollection#SiSPSeededTrackDetailedTruth"]
        # "xAOD::IParticleContainer#InDetIdealTrackParticles"]#,
        #Turn off SiSP
        # "TrackTruthCollection#SiSPSeededTrackTruthCollection",
        # "DetailedTrackTruthCollection#SiSPSeededTrackDetailedTruth"]

StreamESD.ItemList+=["TrackCollection#ObservedTrackCollection",
        "TrackTruthCollection#InDetObservedTrackTruthCollection",
        "DetailedTrackTruthCollection#InDetObservedTrackDetailedTruth"]

#Turn off SiSP
StreamESD.ItemList+=["TrackCollection#" + InDetKeys.SiSpSeededTrackCandidates()]
# #Turn off SiSP
StreamESD.ItemList+=["xAOD::TrackParticleContainer#SiSpTrackCandidatesTrackParticle"]
StreamESD.ItemList+=["xAOD::TrackParticleAuxContainer#SiSpTrackCandidatesTrackParticleAux."]

StreamESD.ItemList+=["xAOD::TrackParticleContainer#InDetObservedTrackParticles",
"xAOD::TrackParticleAuxContainer#InDetObservedTrackParticlesAux.",
"xAOD::AuxContainerBase#InDetObservedTrackParticlesAux.",
"xAOD::IParticleContainer#InDetObservedTrackParticles"]

StreamESD.ItemList+=["xAOD::TrackParticleContainer#ObservedTracks"]
StreamESD.ItemList+=["xAOD::TrackParticleAuxContainer#ObservedTracksAux."]


# print("STREAM TO ESD")
# print(StreamESD.ItemList)
#print("ADDED")

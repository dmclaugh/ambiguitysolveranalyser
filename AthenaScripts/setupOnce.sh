#sparse checkout
setupATLAS
lsetup git
git atlas init-workdir ssh://git@gitlab.cern.ch:7999/dmclaugh/athena.git
cd athena
git checkout 21.0.77-branch
git atlas addpkg InDetRecExample
git atlas addpkg TrkAmbiguityProcessor
git atlas addpkg InDetAmbiTrackSelectionTool
git atlas addpkg DerivationFrameworkInDet
git atlas addpkg TrkValTools
git atlas addpkg TrkValInterfaces
git atlas addpkg InDetPrepRawDataToxAOD
git atlas addpkg TrkToolInterfaces
git atlas addpkg InDetTrackScoringTools
mkdir ../build && cd ../build
asetup 21.0.77,Athena,here
cd ../

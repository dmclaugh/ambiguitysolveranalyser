<h1> Ambiguity Solver Analyser </h1>

The Ambiguity Solver Analyser Tool segregates the tracks that have been documented by the TrkObserverTool into seed families. A seed family consists of a ``parent'' candidate track that was input into the ambiguity solver and all subsequent sub-tracks that have been created and traced back to the parent candidate track. A parent track can only have one sub-track however, the sub-track can have a sub-track, and so on. The truth particle track information is matched to the all of the tracks that have been influenced by the ambiguity solver by ensuring the two tracks do not vary beyond tight trigonometric and kinematic differences. Track candidates and their subsequent sub-tracks can be analysed in-depth by exploring: track content within track generations, the consequence of creating stripped-down sub-tracks, causes of rejections within the ambiguity solver and methods to compare tracks that were not reconstructed but were matched to a reconstructed truth particle.

<h2> Setup Ambiguity Solver Package </h2>

To clone and install the Ambiguity Solver Package (run only once): <br>
- `git clone ssh://git@gitlab.cern.ch:7999/dmclaugh/ambiguitysolveranalyser.git` <br>
- `source init.sh` <br>

And if you are returning to the analyser after closing the terminal: <br>
- `source setup.sh`

<h2> Run the Ambiguity Solver Package </h2>

Change the inputs, outputs and number of events to process within the joboptions file before running:
`source/AmbiguitySolverAnalyser/share/AmbiguitySolverAnalyserAlgJobOptions.py`

From the `run/` directory: `acm compile; nohup athena AmbiguitySolverAnalyser/AmbiguitySolverAnalyserAlgJobOptions.py > logs/AmbiAnalyserLogs.log`

<h2> Running the Reconstruction transformation (Reco_tf.py) with the 
TrkObserverTool </h2>

Build, compile and run athena within the AthenaScripts Directory.

<h2> Doing a spare-checkout of Athena </h2>
Packages used:

```
+ Control/AthenaExamples/AthExHelloWorld
+ PhysicsAnalysis/DerivationFramework/DerivationFrameworkInDet
+ InnerDetector/InDetRecTools/InDetAmbiTrackSelectionTool
+ InnerDetector/InDetExample/InDetRecExample
+ Tracking/TrkTools/TrkAmbiguityProcessor
+ Tracking/TrkValidation/TrkValInterfaces
+ InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD
```

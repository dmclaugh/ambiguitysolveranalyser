// IdealTrackStudy includes
#include "IdealTrackStudyAlg.h"

//#include "xAODEventInfo/EventInfo.h"

#include "AsgTools/AnaToolHandle.h"
#include "xAODCore/ShallowCopy.h"
#include "xAODCore/tools/PrintHelpers.h"
#include "AthContainers/ConstDataVector.h"

// truth 
#include "xAODTruth/TruthParticleContainer.h"

// tracking
#include "InDetTrackSystematicsTools/InDetTrackTruthOriginTool.h"
#include "InDetTrackSystematicsTools/InDetTrackTruthOriginDefs.h"
#include "InDetTrackSelectionTool/IInDetTrackSelectionTool.h"
#include "xAODTruth/TruthVertex.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/TrackMeasurementValidation.h"

// jets
#include "JetCalibTools/IJetCalibrationTool.h"


IdealTrackStudyAlg::IdealTrackStudyAlg( const std::string& name, ISvcLocator* pSvcLocator ) : trackAnalysisAlg( name, pSvcLocator ),
  m_trackOriginTool("InDet::InDetTrackTruthOriginTool",this),
  m_trackSelTool("InDet::InDetTrackSelectionTool/TrackSelectionTool",this),
  m_jetCalibrationTool("")
{

  //declareProperty( "Property", m_nProperty = 0, "My Example Integer Property" ); //example property declaration

  declareProperty( "ApplyTrackSelection", m_applyTrackSelection = false, "Apply Track Selection");
  declareProperty( "TrackSelectionTool", m_trackSelTool );

  declareProperty( "StreamName", m_streamName = "trkStudy", "Stream Name used in THistSvc");
  
  declareProperty( "RunPulls",  m_runPulls  = false, "Add Pull Plots to output");
  declareProperty( "RunRates",  m_runRates  = false, "Add Rate Plots to output");
  declareProperty( "HasIdeal",  m_hasIdeal  = false, "Ideal tracks are in input file");
  declareProperty( "HasJets",   m_hasJets  = false, "Jets are in input file");
  declareProperty( "StudyMatchedTracks", m_studyMatchedTracks = false, "Separate matched tracks in pull plots");
  declareProperty( "RunZ0Bias", m_runZ0Bias   = false, "Add Z0 Bias Plots to output");
  declareProperty( "TrackPtMin", m_trackPtMin = 400, "Track pT MIN (MeV)");
  declareProperty( "JetPtMin",   m_jetPtMin   = 120, "Jet pT MIN (GeV)"); // commented out
  declareProperty( "NJets",      m_nJetCut    = 3, "Considering N of the leading jets");
  declareProperty( "AddLargeD0Trks", m_addLargeD0Trks = false, "Including Large D0 Tracks in reco collection");

}


IdealTrackStudyAlg::~IdealTrackStudyAlg() {}


StatusCode IdealTrackStudyAlg::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");

  ATH_CHECK(m_thistSvc.retrieve());
  //m_thistSvc->initialize();
  //m_h_efield = new TH1F("efield", "", 100, 200., 800.);
  //ATH_CHECK(m_thistSvc->regHist("/file1/efield", m_h_efield));


  //HERE IS AN EXAMPLE
  //We will create a histogram and a ttree and register them to the histsvc
  //Remember to configure the histsvc stream in the joboptions
  //
  //m_myHist = new TH1F("myHist","myHist",100,0,100);
  //CHECK( histSvc()->regHist("/MYSTREAM/myHist", m_myHist) ); //registers histogram to output stream
  //m_myTree = new TTree("myTree","myTree");
  //CHECK( histSvc()->regTree("/MYSTREAM/SubDirectory/myTree", m_myTree) ); //registers tree to output stream inside a sub-directory

  ATH_MSG_INFO("Get track origin tool");
  // https://acode-browser.usatlas.bnl.gov/lxr/source/athena/PhysicsAnalysis/TrackingID/InDetTrackSystematicsTools/Root/InDetTrackTruthOriginTool.cxx?v=21.2
  //m_trackOriginTool = new InDet::InDetTrackTruthOriginTool("InDet::InDetTrackTruthOriginTool");
  //wrap below is one of those check commands
  //m_trackOriginTool->initialize();
  ATH_CHECK( m_trackOriginTool.retrieve() );

  // https://twiki.cern.ch/twiki/bin/view/AtlasProtected/InDetTrackSelectionTool
  if( m_applyTrackSelection ) {
    ATH_MSG_INFO("Get track selection tool");
    ATH_CHECK( m_trackSelTool.retrieve() );
  }

  //TODO from TrackSelection TWiki (above)
  //#include "xAODTracking/TrackParticlexAODHelpers.h"
  //[...]
  //xAOD::EventInfo *evt = ...; // the EventInfo xAOD object
  //xAOD::TrackParticle *tp = ...; //your input track particle
  //double d0sig = xAOD::TrackingHelpers::d0significance( tp, evt->beamPosSigmaX(), evt->beamPosSigmaY(), evt->beamPosSigmaXY() );

  ATH_MSG_INFO("Get jet calibration tool");
  //m_jetCalibrationTool.setTypeAndName("JetCalibrationTool/MCJetCalibToolEMTopo"); // connect to JO
  //CHECK( m_jetCalibrationTool.retrieve() );


  // book histograms
  ATH_MSG_INFO("Booking histograms.");
  CHECK( bookEventHistograms() );
  if( m_runPulls ) {
    CHECK( bookPullHistograms() );
  }

  if( m_runRates) {
    CHECK( bookRateHistograms() );
  }

  if( m_runZ0Bias ) { CHECK( bookZ0BiasHistograms() ); }
  m_pvZ0 = -999;

  // set counters
  m_eventCounter  = 0;
  m_recoCounter   = 0;
  m_pseudoCounter = 0;
  m_idealCounter  = 0;

  ATH_MSG_INFO("Initiliation complete.");
  return StatusCode::SUCCESS;
}

StatusCode IdealTrackStudyAlg::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");
  //
  //Things that happen once at the end of the event loop go here
  //
  ATH_MSG_INFO ("Processed " << m_eventCounter << " events." 
      << "\n\t Reco:   " << m_recoCounter 
      << "\n\t Pseudo: " << m_pseudoCounter
      << "\n\t Ideal:  " << m_idealCounter
      );

  //CHECK( m_jetCalibrationTool.release() );


  return StatusCode::SUCCESS;
}


#define accessor(n,t,v) SG::AuxElement::ConstAccessor< t > n (v)
// parameters/hits
accessor(a_d0, float, "d0");
accessor(a_z0, float, "z0");
accessor(a_theta, float, "theta");
accessor(a_pixelHits,       uint8_t, "numberOfPixelHits");
accessor(a_pixelHoles,      uint8_t, "numberOfPixelHoles");
accessor(a_pixelSharedHits, uint8_t, "numberOfPixelSharedHits");
accessor(a_pixelSplitHits,  uint8_t, "numberOfPixelSplitHits");
accessor(a_sctHits,       uint8_t, "numberOfSCTHits");
accessor(a_sctHoles,      uint8_t, "numberOfSCTHoles");
accessor(a_sctSharedHits, uint8_t, "numberOfSCTSharedHits");
// TrackStateOnSurfaceDecorator
accessor(a_isShared, bool, "isShared");
// https://acode-browser.usatlas.bnl.gov/lxr/source/athena/InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/src/PixelPrepDataToxAOD.cxx?v=21.2
// Pixel Cluster Properties
accessor(a_sizePhi, int, "sizePhi");
accessor(a_sizeZ, int, "sizeZ");
accessor(a_charge, float, "charge");
accessor(a_tot, int, "ToT");
accessor(a_isFake, char, "isFake");
accessor(a_gangedPixel, char, "gangedPixel");
accessor(a_isSplit, int, "isSplit");
accessor(a_splitProb1, float, "splitProbability1");
accessor(a_splitProb2, float, "splitProbability2");
accessor(a_trkErrBiased,   float, "TrackError_biased");
accessor(a_trkErrUnbiased, float, "TrackError_unbiased");
// All the NN Variables
accessor(a_skip_cluster, bool, "skip_cluster"); // ?
accessor(a_sizeX, int, "NN_sizeX");
accessor(a_sizeY, int, "NN_sizeY");
accessor(a_posX, std::vector<float>, "NN_positions_indexX");
accessor(a_posY, std::vector<float>, "NN_positions_indexY");
accessor(a_posX_mm, std::vector<float>, "NN_positionsX");
accessor(a_posY_mm, std::vector<float>, "NN_positionsY");
accessor(a_localEtaPixelIndexWeightedPosition, float, "NN_localEtaPixelIndexWeightedPosition");
accessor(a_localPhiPixelIndexWeightedPosition, float, "NN_localPhiPixelIndexWeightedPosition");
accessor(a_layer, int, "layer");
accessor(a_bec, int, "bec");
accessor(a_etaModule, int, "eta_module");
accessor(a_matrix, std::vector<float>, "NN_matrixOfCharge");
accessor(a_pitches, std::vector<float>, "NN_vectorOfPitchesY");
accessor(a_NNtheta, std::vector<float>,  "NN_theta");
accessor(a_phi, std::vector<float>,  "NN_phi");
accessor(a_globalX, float, "globalX");
accessor(a_globalY, float, "globalY");
accessor(a_globalZ, float, "globalZ");
// sihit
accessor(a_sihit_barcode, std::vector<int>, "sihit_barcode");
accessor(a_sihit_pdgid, std::vector<int>, "sihit_pdgid");
accessor(a_sihit_energyDeposit, std::vector<float>, "sihit_energyDeposit");
accessor(a_sihit_startPosX, std::vector<float>, "sihit_startPosX");
accessor(a_sihit_startPosY, std::vector<float>, "sihit_startPosY");
accessor(a_sihit_startPosZ, std::vector<float>, "sihit_startPosZ");
accessor(a_sihit_endPosX, std::vector<float>, "sihit_endPosX");
accessor(a_sihit_endPosY, std::vector<float>, "sihit_endPosY");
accessor(a_sihit_endPosZ, std::vector<float>, "sihit_endPosZ");
// sdo
accessor(a_sdo_depositsBarcode, std::vector<std::vector<int>>, "sdo_depositsBarcode"); 
// track location on layer
accessor(a_trkIBLX, float, "TrkIBLX");
accessor(a_trkIBLY, float, "TrkIBLY");
accessor(a_trkIBLZ, float, "TrkIBLZ");
// sct
accessor(a_siWidth, int, "SiWidth");
// other
accessor(a_truthProb, float, "truthMatchProbability");
// for this class
accessor(a_matchReco, int, "matchReco");
accessor(a_matchPseudo, int, "matchPseudo");
accessor(a_matchIdeal, int, "matchIdeal");
accessor(a_minDR, float, "minDR");
accessor(a_minDRTruth, float, "minDRTruth");
accessor(a_minDRPt, float, "minDRPt");
accessor(a_minDRFlavor, int, "minDRFlavor");
accessor(a_nWrongSplit,      int, "nWrongSplit");
accessor(a_nWrongSplitTight, int, "nWrongSplitTight");
accessor(a_nUnSplit,         int, "nUnSplit");
accessor(a_nUnSplitTight,    int, "nUnSplitTight");
accessor(a_nSplitShare,      int, "nSplitShare");
accessor(a_nWrongSharePixel, int, "nWrongSharePixel");
accessor(a_nWrongShareSCT,   int, "nWrongShareSCT");
accessor(a_nUnSharePixel,    int, "nUnSharePixel");
accessor(a_nUnShareSCT,      int, "nUnShareSCT");
accessor(a_nPart,            int, "nParticle");
accessor(a_nPartTight,       int, "nParticleTight");
accessor(a_nUsed,            int, "nUsed");
// jet
accessor(a_flavor, int, "HadronConeExclTruthLabelID");
#undef accessor


#define decorator(n,t,v) SG::AuxElement::Decorator< t > n (v)
decorator(d_matchReco, int, "matchReco");
decorator(d_matchPseudo, int, "matchPseudo");
decorator(d_matchIdeal, int, "matchIdeal");
decorator(d_minDR, float, "minDR");
decorator(d_minDRTruth, float, "minDRTruth");
decorator(d_minDRPt, float, "minDRPt");
decorator(d_minDRFlavor, int, "minDRFlavor");
decorator(d_nWrongSplit,      int, "nWrongSplit");
decorator(d_nWrongSplitTight, int, "nWrongSplitTight");
decorator(d_nUnSplit,         int, "nUnSplit");
decorator(d_nUnSplitTight,    int, "nUnSplitTight");
decorator(d_nSplitShare,      int, "nSplitShare");
decorator(d_nWrongSharePixel, int, "nWrongSharePixel");
decorator(d_nWrongShareSCT,   int, "nWrongShareSCT");
decorator(d_nUnSharePixel,    int, "nUnSharePixel");
decorator(d_nUnShareSCT,      int, "nUnShareSCT");
decorator(d_nPart,            int, "nParticle");
decorator(d_nPartTight,       int, "nParticleTight");
decorator(d_nUsed,            int, "nUsed");
#undef decorator

StatusCode IdealTrackStudyAlg::execute() {  
  ATH_MSG_DEBUG ("Executing " << name() << "...");
  setFilterPassed(false); //optional: start with algorithm not passed


  m_eventCounter++;
  if (m_eventCounter % 100 == 0) {
    ATH_MSG_INFO("Processed "<< m_eventCounter << " events.");
  }

  
  // find z0 of primary vertex
  m_primaryVertex = nullptr;
  if( m_runZ0Bias or m_applyTrackSelection ) {
    const xAOD::VertexContainer* vertices = nullptr;
    CHECK( evtStore()->retrieve( vertices, "PrimaryVertices" ) ); 
    for( auto iVtx : *vertices) {
      if(iVtx->vertexType() != xAOD::VxType::VertexType::PriVtx) { continue; }
      if((int)(iVtx)->nTrackParticles() < 2 ) { continue; }
      m_pvZ0 = iVtx->z();
      m_primaryVertex = iVtx;
      break;
    }
    if( m_pvZ0 == -999) { 
      ATH_MSG_WARNING("No PV Found! Skipping event.");
      return StatusCode::SUCCESS; // probably not a success...
    }
  } // m_runZ0Bias


  // Get Collections
  const xAOD::TruthParticleContainer* m_truthParticles = nullptr;
  CHECK( evtStore()->retrieve( m_truthParticles , "TruthParticles" ) ); 

  const xAOD::TrackParticleContainer* m_recoTracksInitial = nullptr;
  CHECK( evtStore()->retrieve( m_recoTracksInitial , "InDetTrackParticles" ) );
  //m_recoCounter   += m_recoTracksInitial   ->size();

  // remove TRT tracks 
  const xAOD::TrackParticleContainer* m_recoTracks = nullptr;
  ConstDataVector<xAOD::TrackParticleContainer> selRecoTracksCDV(SG::VIEW_ELEMENTS);
  for ( const auto itrk : *m_recoTracksInitial) { 
    const std::bitset<xAOD::NumberOfTrackRecoInfo> patternReco = itrk->patternRecoInfo();
    if( patternReco.test(xAOD::TrackPatternRecoInfo::TRTSeededTrackFinder) ) { continue; } // 4
    if( patternReco.test(xAOD::TrackPatternRecoInfo::TRTStandalone) ) { continue; } // 20
    if(!m_addLargeD0Trks) {
      if( patternReco.test(xAOD::TrackPatternRecoInfo::SiSpacePointsSeedMaker_LargeD0) ) { continue; } // 49
    }
    selRecoTracksCDV.push_back( itrk );
  }
  m_recoTracks = selRecoTracksCDV.asDataVector();
  m_recoCounter   += m_recoTracks   ->size();

  const xAOD::TrackParticleContainer* m_pseudoTracks = nullptr;
  CHECK( evtStore()->retrieve( m_pseudoTracks , "InDetPseudoTrackParticles" ) ); 
  m_pseudoCounter += m_pseudoTracks ->size();
  //std::cout << "N pseudo tracks " << m_pseudoTracks->size() << std::endl;

  const xAOD::TrackParticleContainer* m_idealTracks = nullptr;
  if( m_hasIdeal ) {
    CHECK( evtStore()->retrieve( m_idealTracks , "InDetIdealTrackParticles" ) ); 
    m_idealCounter  += m_idealTracks  ->size();
  }



/*
      xAOD::dump( *track );

  const xAOD::TrackParticleContainer* m_obsTracks = nullptr;
  CHECK( evtStore()->retrieve( m_obsTracks , "InDetObservedTrackParticles" ) ); 

  std::cout << "ObsTrk " << m_obsTracks->size() << std::endl;
  static SG::AuxElement::ConstAccessor< MeasurementsOnTrack > acc_MeasurementsOnTrack( measurementNames  );
  for ( const auto itrk : *m_obsTracks) {
    auto truth = getTruth(itrk);
    if(!truth) { continue; } // needed?

    // find matching pseudo track
    const xAOD::TrackParticle* pseudo = nullptr;
    for ( const auto ptrk : *m_pseudoTracks ) {
      auto ptruth = getTruth(ptrk);
      if(!ptruth) { continue; }
      if(truth->barcode() != ptruth->barcode()) { continue; }
      // match found
      pseudo = ptrk;
      break;
    }

    if(!pseudo) { continue; } // no match

    // now compare cluster content
    const MeasurementsOnTrack& measurementsSeed   = acc_MeasurementsOnTrack( *itrk );
    const MeasurementsOnTrack& measurementsPseudo = acc_MeasurementsOnTrack( *pseudo );

    // calculate denominator
    float denom = 10.0*pseudo->auxdata<uint8_t>("numberOfPixelHits")
      + 5.0*pseudo->auxdata<uint8_t>("numberOfSCTHits");

    // calculate numerator
    float numer = 0.0;
    // loop over hits on seed
    for ( auto msos_iter_seed=measurementsSeed.begin(); 
        msos_iter_seed!=measurementsSeed.end(); ++msos_iter_seed ) {
      const xAOD::TrackStateValidation* msos_seed = getMSOS( msos_iter_seed ); 
      if ( msos_seed == nullptr) { continue; }
      // go back to cluster
      const xAOD::TrackMeasurementValidation* clus_seed =  *(msos_seed->trackMeasurementValidationLink());

      // look for match on pseudo track
      bool onPseudo(false);
      for ( auto msos_iter_pseudo=measurementsPseudo.begin(); 
          msos_iter_pseudo!=measurementsPseudo.end(); ++msos_iter_pseudo ) {
        const xAOD::TrackStateValidation* msos_pseudo = getMSOS( msos_iter_pseudo ); 
        if ( msos_pseudo == nullptr) { continue; }
        const xAOD::TrackMeasurementValidation* clus_pseudo =  *(msos_pseudo->trackMeasurementValidationLink());
        if ( clus_seed != clus_pseudo ) { continue; }
        onPseudo = true;
        break;
      }

      if(onPseudo) {
        if( msos_seed->detType() == detPixel ) { numer += 10.0; } 
        else { numer += 5.0; } // SCT
      }

    } // loop over seed MSOS
    std::cout << "score " << numer << "\t" << denom << "\t" << numer/denom << std::endl;
  } // loop over observed tracks

  return StatusCode::SUCCESS;
  */

  const xAOD::TruthParticleContainer* primaryTruth = nullptr;
  ConstDataVector< xAOD::TruthParticleContainer > primaryTruthCDV(SG::VIEW_ELEMENTS);
  if(m_runRates) {
    primaryTruthCDV = GetPrimaryTruth(m_truthParticles);
    primaryTruth    = primaryTruthCDV.asDataVector();
  }

  const xAOD::JetContainer* inJets = nullptr;
  ConstDataVector<xAOD::JetContainer> selJetsCDV(SG::VIEW_ELEMENTS);
  const xAOD::JetContainer* selJets = nullptr;

  if(m_hasJets) { 
    CHECK( evtStore()->retrieve( inJets , "AntiKt4EMTopoJets" ) ); 
    std::pair< xAOD::JetContainer*, xAOD::ShallowAuxContainer* > calibJetsSC = xAOD::shallowCopyContainer( *inJets );

    int nJet(0);
    const xAOD::Jet* jet0 = nullptr;
    const xAOD::Jet* jet1 = nullptr;

//    TH1* h_jetPt = hist("EventInfo/h_jetPt");
    for ( auto jet_itr : *(calibJetsSC.first) ) {
      if(nJet == m_nJetCut) { break; } 

      // jet cone 0.239 + exp(−1.220 − pT ⋅ 1.64 ⋅ 10−5)

//      if( m_jetCalibrationTool->applyCorrection( *jet_itr ) == CP::CorrectionCode::Error ) {
//        ATH_MSG_ERROR( "JetCalibration tool reported a CP::CorrectionCode::Error");
//        return StatusCode::FAILURE;
//      }

      if( fabs(jet_itr->eta()) > 2.5 ) { continue; } // only jets in the ID
      //if( jet_itr->pt() <  m_jetPtMin*1e3 )   { continue; } // pile-up


      if(nJet == 0) {
//        hist("EventInfo/h_jet0Pt")->Fill( jet_itr->pt()/1e3 ); // GeV
        jet0 = jet_itr;
      }
      if(nJet == 1) {
//        hist("EventInfo/h_jet1Pt")->Fill( jet_itr->pt()/1e3 ); // GeV
        jet1 = jet_itr;
      }
//      h_jetPt->Fill( jet_itr->pt()/1e3 ); // GeV

      selJetsCDV.push_back( jet_itr );
      nJet++;
    } // loop over jets

//    if(jet0 && jet1) {
//      hist("EventInfo/h_jetDPhi")->Fill( jet0->p4().DeltaPhi( jet1->p4() ) );
//    }
//    hist("EventInfo/h_nJets")->Fill( nJet );

    selJets = selJetsCDV.asDataVector();

    // decorae mindR to jet
    if(m_runRates) {
      CHECK( DecorateMinDR( primaryTruth,   selJets ) );
      CHECK( DecorateMinDR( m_recoTracks,   selJets ) );
      CHECK( DecorateMinDR( m_pseudoTracks, selJets ) );
      if(m_hasIdeal) { 
        CHECK( DecorateMinDR( m_idealTracks, selJets ) );
      }
    }

  } // hasJets


  // basic track count
//  hist("EventInfo/h_nRecoTracks"  )->Fill( m_recoTracks  ->size() );
//  hist("EventInfo/h_nPseudoTracks")->Fill( m_pseudoTracks->size() );
//  if(m_hasIdeal) { 
//    hist("EventInfo/h_nIdealTracks" )->Fill( m_idealTracks ->size() );
//  }

  /*
  ATH_MSG_INFO ("N Tracks " << m_truthParticles->size() 
      << "\t" << m_recoTracks  ->size()
      << "\t" << m_pseudoTracks->size()
      << "\t" << m_idealTracks ->size());
      */

  // Get the pixel clusters 
  //const xAOD::TrackMeasurementValidationContainer *clusters;
  //CHECK(evtStore()->retrieve(clusters, "PixelClusters"));

  if( m_runPulls ) { 
    CHECK( GetListOfReconstructableBarcodes( m_pseudoTracks ) );
    CHECK( DecorateSplitShareQual( m_recoTracks,   0 ) );
    CHECK( DecorateSplitShareQual( m_pseudoTracks, 1 ) );
    if(m_hasIdeal) { 
      CHECK( DecorateSplitShareQual( m_idealTracks,  2 ) );
    }
  }


  // loop over tracks and compare
  // loop over pseudo as there are rare instances where an ideal track does not exist (NEEDS TO BE UNDERSTOOD)
  if( m_runPulls ) {

    // look at all tracks or separate those that are matched between reco-pseudo-ideal
    if (m_studyMatchedTracks) {
      int matchReco(0);
      int matchIdeal(0);


      // initalize index for triplets
      for ( const auto itrk : *m_recoTracks) { 
        d_matchPseudo( *itrk ) = -1;
        d_matchIdeal( *itrk )  = -1;
      }
      if ( m_hasIdeal ) {
        for ( const auto itrk : *m_idealTracks ) { 
          d_matchReco( *itrk )   = -1;
          d_matchPseudo( *itrk ) = -1;
        }
      }
      //pseudo done below


      int index_reco(0);
      int index_pseudo(0);
      int index_ideal(0);
      const xAOD::TrackParticle* reco  = nullptr;
      const xAOD::TrackParticle* ideal = nullptr;
      for ( const auto pseudo : *m_pseudoTracks ) {

        d_matchReco( *pseudo )  = -1;
        d_matchIdeal( *pseudo ) = -1;

        // reset
        reco  = nullptr;
        ideal = nullptr;
        index_reco = 0;
        index_ideal = 0;


        // find the associated truth particle
        auto truth = getTruth(pseudo);
        if(!truth) { 
          ATH_MSG_WARNING ("Pseudo Track truth not available! " );
          index_pseudo++;
          continue;
        }

        // find the associated ideal track
        if( m_hasIdeal ) {
          for ( const auto itrk : *m_idealTracks ) { 
            auto iTruth = getTruth(itrk);
            if(!iTruth) { index_ideal++; continue; }
            if ( truth == iTruth ) {
              ideal = itrk;
              d_matchIdeal( *pseudo ) = index_ideal;
              d_matchPseudo( *itrk ) = index_pseudo;
              matchIdeal++;
              break;
            }
            index_ideal++;
          } // loop over ideal
        } // has ideal

        // find the associated reco  track
        for ( const auto itrk : *m_recoTracks ) { 
          auto iTruth = getTruth(itrk);
          if(!iTruth) { index_reco++; continue; }
          if ( truth == iTruth ) {
            reco = itrk;
            d_matchReco( *pseudo ) = index_reco;
            d_matchPseudo( *itrk ) = index_pseudo;
            // if the pseudo track was matched to an ideal 
            // then pair this reco to the ideal as well
            if( ideal ) {
              d_matchReco( *ideal ) = index_reco;
              d_matchIdeal( *itrk ) = index_ideal;
            }
            matchReco++;
            break;
          }
          index_reco++;
        } // loop over reco


        // pseudo matched to ideal but not reconstructed
        // does not include tracks that were reconstructed but rejected by cuts
        if( !reco ) {
          if( pseudo ) {
            CHECK( fillHitPatterns( pseudo, "Pseudo", "Lost") );
            CHECK( fillResiduals( pseudo, "Pseudo", "Lost") );
            CHECK( fillTrkPResiduals( pseudo, "Pseudo", "Lost") );
          }

          if(ideal) { // will never be satisfied when !m_hasIdeal
            CHECK( fillHitPatterns( ideal, "Ideal", "Lost") );
            CHECK( fillResiduals( ideal, "Ideal", "Lost") );
            CHECK( fillTrkPResiduals( ideal, "Ideal", "Lost") );
          }
        } // not reconstructed

        index_pseudo++;

        // only look at tracks that are matched in all three collections
        // if ideal tracks not available, only check pseudo
        if( !reco  ) { continue; }
        if( !ideal and m_hasIdeal ) { continue; }


        // apply track selection on matched reco here
        if( m_applyTrackSelection ) {
          if( ! m_trackSelTool->accept( *reco , m_primaryVertex ) ) continue;
        }
        //    if( a_pixelHits( *itrk ) == 0 ) { continue; }
        //    if( a_pixelHits( *itrk ) != 0 ) { continue; }

        int origin = m_trackOriginTool->getTrackOrigin( pseudo );
        std::string type = getOriginLabel( origin );

        CHECK( fillHitPatterns( reco, "Reco", "") );
        CHECK( fillResiduals( reco, "Reco", "") );
        CHECK( fillTrkPResiduals( reco, "Reco", "") );
        
        CHECK( fillHitPatterns( reco, "Reco", type ) );
        CHECK( fillResiduals( reco, "Reco", type ) );
        CHECK( fillTrkPResiduals( reco, "Reco", type ) );


        CHECK( fillHitPatterns( pseudo, "Pseudo", "") );
        CHECK( fillResiduals( pseudo, "Pseudo", "") );
        CHECK( fillTrkPResiduals( pseudo, "Pseudo", "") );
        
        CHECK( fillHitPatterns( pseudo, "Pseudo", type ) );
        CHECK( fillResiduals( pseudo, "Pseudo", type ) );
        CHECK( fillTrkPResiduals( pseudo, "Pseudo", type ) );


        if(m_hasIdeal) {
          CHECK( fillHitPatterns( ideal, "Ideal", "") );
          CHECK( fillResiduals( ideal, "Ideal", "") );
          CHECK( fillTrkPResiduals( ideal, "Ideal", "") );
        
          CHECK( fillHitPatterns( ideal, "Ideal", type ) );
          CHECK( fillResiduals( ideal, "Ideal", type ) );
          CHECK( fillTrkPResiduals( ideal, "Ideal", type ) );
        }


        // to check the truthMatchProb (<0.75=Fake)
        int originReco = m_trackOriginTool->getTrackOrigin( reco );
        if( InDet::TrkOrigin::isFake(originReco) ) { 
          CHECK( fillHitPatterns( reco,   "Reco", "Fake" ) );
          CHECK( fillResiduals( reco,     "Reco", "Fake" ) );
          CHECK( fillTrkPResiduals( reco, "Reco", "Fake" ) );

          CHECK( fillHitPatterns( pseudo,   "Pseudo", "Fake") );
          CHECK( fillResiduals( pseudo,     "Pseudo", "Fake") );
          CHECK( fillTrkPResiduals( pseudo, "Pseudo", "Fake") );

          if(m_hasIdeal) {
            CHECK( fillHitPatterns( ideal,   "Ideal", "Fake") );
            CHECK( fillResiduals( ideal,     "Ideal", "Fake") );
            CHECK( fillTrkPResiduals( ideal, "Ideal", "Fake") );
          }
        }

      } // loop over pseudo tracks

      ATH_MSG_DEBUG ("Matches to " << m_pseudoTracks->size() << " pseudo : " << matchReco << "\t" << matchIdeal
          << "\t rate " << float(matchReco/m_pseudoTracks->size()) 
          << "\t" << float(matchIdeal/m_pseudoTracks->size()));

//      std::cout << "Matches to " << m_pseudoTracks->size() << " pseudo : " << matchReco << "\t" << matchIdeal
//          << "\t rate " << float(matchReco/m_pseudoTracks->size()) 
//          << "\t" << float(matchIdeal/m_pseudoTracks->size())
//          << std::endl;

      // have not plotted 
      //  - reco tracks not matched to pseudo/ideal
      for ( const auto itrk : *m_recoTracks ) {
        if( a_matchIdeal( *itrk )  >= 0 and m_hasIdeal ) { continue; }
        if( a_matchPseudo( *itrk ) >= 0 ) { continue; } 
        // apply selection if needed
        if( m_applyTrackSelection ) {
          if( ! m_trackSelTool->accept( *itrk , m_primaryVertex ) ) continue;
        }
        
        int originReco = m_trackOriginTool->getTrackOrigin( itrk );
        if( InDet::TrkOrigin::isFake(originReco) ) { 
          CHECK( fillHitPatterns( itrk, "Reco", "NoMatchFake" ) );
          CHECK( fillResiduals( itrk, "Reco", "NoMatchFake" ) );
          CHECK( fillTrkPResiduals( itrk, "Reco", "NoMatchFake" ) );
        } else {
          CHECK( fillHitPatterns( itrk, "Reco", "NoMatch" ) );
          CHECK( fillResiduals( itrk, "Reco", "NoMatch" ) );
          CHECK( fillTrkPResiduals( itrk, "Reco", "NoMatch" ) );
        }
      } // loop over reco

    } else { // not matching tracks for study

      for ( const auto iReco : *m_recoTracks ) {
        int origin = m_trackOriginTool->getTrackOrigin( iReco ); // default truth prob cut is 0.75...change to 0.7!

        // in tool - matched but truth link is broken: call it from pileup (not true for < 100 MeV!)
        if( InDet::TrkOrigin::isPileup(origin) ) { continue; }

        // not matched to truth: call it fake, default cut is 0.75
        if( InDet::TrkOrigin::isFake(origin) ) { 
          CHECK( fillHitPatterns( iReco, "Reco", "Fake" ) );
          CHECK( fillResiduals( iReco, "Reco", "Fake" ) );
          CHECK( fillTrkPResiduals( iReco, "Reco", "Fake" ) );
        } else {
          // all good reco tracks
          CHECK( fillHitPatterns( iReco, "Reco", "") );
          CHECK( fillResiduals( iReco, "Reco", "") );
          CHECK( fillTrkPResiduals( iReco, "Reco", "") );
          std::string type = getOriginLabel( origin );
          CHECK( fillHitPatterns( iReco, "Reco", type ) );
          CHECK( fillResiduals( iReco, "Reco", type ) );
          CHECK( fillTrkPResiduals( iReco, "Reco", type ) );
        }
      }

      // plot hit patterns for all of the truth-based tracks
      CHECK( fillHitPatterns( m_pseudoTracks, "Pseudo" ) );
      if(m_hasIdeal) { 
        CHECK( fillHitPatterns( m_idealTracks,  "Ideal"  ) );
      }

      // fill residuals for all of the truth-based tracks
      CHECK( fillResiduals( m_pseudoTracks, "Pseudo" ) );
      if(m_hasIdeal) { 
        CHECK( fillResiduals( m_idealTracks,  "Ideal"  ) );
      }

      // plot track parameter residuals for all of the truth-based tracks
      CHECK( fillTrkPResiduals( m_pseudoTracks, "Pseudo" ) );
      if(m_hasIdeal) { 
        CHECK( fillTrkPResiduals( m_idealTracks,  "Ideal"  ) );
      }
    } // if matching tracks or not

  } // runPulls

  if( m_runRates ) { 
    CHECK( fillRateHists( m_recoTracks,   "Reco"   ) );
    CHECK( fillRateHists( m_pseudoTracks, "Pseudo" ) );
    if(m_hasIdeal) {
      CHECK( fillRateHists( m_idealTracks,  "Ideal"  ) );
    }
    CHECK( fillRateHists( primaryTruth,   "Truth"  ) );
  }


  if( m_runZ0Bias ) {
    for ( const auto iReco : *m_recoTracks ) {
      if ( iReco->pt() < 3000 ) { continue; }           // > 3 GeV
      if ( a_pixelHoles( *iReco ) > 0 ) {  continue; }   // 0 Pixel Holes
      CHECK( fillZ0Bias( iReco, "Reco" ) );

      // find the associated ideal track
      auto truth = getTruth(iReco);
      for ( const auto itrk : *m_idealTracks ) { 
        auto iTruth = getTruth(itrk);
        if ( truth == iTruth ) {
          CHECK( fillZ0Bias( iReco, "RecoMatched" ) );
          break;
        }
      } // loop over ideal
    } // loop over reco

    for ( const auto iIdeal : *m_idealTracks ) { 
      if ( iIdeal->pt() < 3000 ) { continue; }           // > 3 GeV
      if ( a_pixelHoles( *iIdeal ) > 0 ) {  continue; }   // 0 Pixel Holes
      CHECK( fillZ0Bias( iIdeal, "Ideal" ) );
    } // loop over ideal

  } // if m_runZ0Bias



  setFilterPassed(true); //if got here, assume that means algorithm passed

  return StatusCode::SUCCESS;
}


std::string IdealTrackStudyAlg::getOriginLabel( int origin ) {
  // https://acode-browser.usatlas.bnl.gov/lxr/source/athena/PhysicsAnalysis/TrackingID/InDetTrackSystematicsTools/InDetTrackSystematicsTools/InDetTrackTruthOriginDefs.h?v=21.2
  //if( InDet::TrkOrigin::isFromDNotFromB(origin) ) { return "FromDnotB"; }
  //if( InDet::TrkOrigin::isFromDfromB(origin) )    { return "FromBtoD";  }
  //if( InDet::TrkOrigin::isFromB(origin) )         { return "FromBnoD";  }
  if( InDet::TrkOrigin::isFromB(origin) )         { return "FromB";  } // includes B->D
  if( InDet::TrkOrigin::isFromD(origin) )         { return "FromC";  }
  //if( InDet::TrkOrigin::isFromB(origin) )         { return "FromHF";  } // includes B->D
  //if( InDet::TrkOrigin::isFromD(origin) )         { return "FromHF";  }
  /** from long living particle decays (V0) or gamma conversions or hadronic interactions and anything else with barcode > 200000 */
  if( InDet::TrkOrigin::isSecondary(origin) )     { return "Secondary";     }
  if( InDet::TrkOrigin::isFragmentation(origin) ) { return "Fragmentation"; }
  return "Rest";
}


ConstDataVector< xAOD::TruthParticleContainer > IdealTrackStudyAlg::GetPrimaryTruth( const xAOD::TruthParticleContainer* truthParts ) {
  ConstDataVector< xAOD::TruthParticleContainer > primaryTruthCDV(SG::VIEW_ELEMENTS);
  for( auto truth : *truthParts ) {

    // isPrimary from old stuff ~/work/tracks/trackingInJets/cmsplot/MyAnalysis/Root/MyxAODAnalysis.cxx
    // modified to meet newer defs
    //Check to see if its a stable particle
    if( truth->status() != 1 ) { continue; }

    // obviously
    if( truth->isNeutral())    { continue; }

    if( truth->barcode() == 0 ||
        truth->barcode() >= 200e3 ) { continue; }

    // Check the particle is within acceptance 
    if( truth->pt() < 400.  ||  fabs(truth->eta()) > 2.5 ) { continue; }

    // make sure particle decays before the last pixel layer
    if( !truth->hasProdVtx() || truth->prodVtx()->perp() > 100) { continue; }

    // make sure particle makes it through all the SCT layers
    if( !truth->hasDecayVtx() || truth->decayVtx()->perp() < 520) { continue; }

    // Kill particle if has same pdgid as parent ( no abs )
    int pdgid(truth->pdgId());
    bool selfDecay(false);
    if( truth->hasProdVtx() ) {
      size_t nParents = truth->prodVtx()->nIncomingParticles();
      for( size_t iparent=0; iparent < nParents; iparent++ ) {
        const xAOD::TruthParticle* parent = truth->prodVtx()->incomingParticle(iparent);
        if( parent->pdgId() == pdgid ) { selfDecay = true; break; }  // do not check sign
      }
    } // has prodVtx
    if(selfDecay) {
      continue;
    }

    primaryTruthCDV.push_back( truth );
  }

  return primaryTruthCDV;
}

StatusCode IdealTrackStudyAlg::GetListOfReconstructableBarcodes( const xAOD::TrackParticleContainer* tracks ) {

  m_canRecoBarcodes.clear();
  m_canRecoBarcodes.reserve(tracks->size());
  for ( const auto itrk : *tracks ) { 
    auto truth = getTruth( itrk );
    if(!truth) { 
      std::cout << "No truth: " << itrk->pt() << "\t" << itrk->eta() << std::endl;
      continue; 
    }
    m_canRecoBarcodes.push_back( truth->barcode() ); 
  }
  if( m_canRecoBarcodes.size() < tracks->size() ) { 
    ATH_MSG_WARNING("Expect " << tracks->size() << " reconstructable particles. Count " << m_canRecoBarcodes.size());
  }

  return StatusCode::SUCCESS;
}

StatusCode IdealTrackStudyAlg::DecorateSplitShareQual( const xAOD::TrackParticleContainer* tracks, int trkColNum ) {

  int nWrongSplit(0);
  int nWrongSplitTight(0);
  int nUnSplit(0);
  int nUnSplitTight(0);
  int nSplitShare(0);
  int nWrongSharePixel(0);
  int nWrongShareSCT(0);
  int nUnSharePixel(0);
  int nUnShareSCT(0);

  for ( const auto itrk : *tracks ) { 
    d_nWrongSplit( *itrk )      = nWrongSplit;
    d_nWrongSplitTight( *itrk ) = nWrongSplitTight;
    d_nUnSplit( *itrk )         = nUnSplit;
    d_nUnSplitTight( *itrk )    = nUnSplitTight;
    d_nSplitShare( *itrk )      = nSplitShare;
    d_nWrongSharePixel( *itrk ) = nWrongSharePixel;
    d_nWrongShareSCT( *itrk )   = nWrongShareSCT;
    d_nUnSharePixel( *itrk )    = nUnSharePixel;
    d_nUnShareSCT( *itrk )      = nUnShareSCT;
  }

  // NEED MORE INFO TO RUN
  // delete loop above
  return StatusCode::SUCCESS;

  std::multimap< const xAOD::TrackMeasurementValidation*, const xAOD::TrackParticle* >* multiMap 
    = new std::multimap< const xAOD::TrackMeasurementValidation*, const xAOD::TrackParticle* >;

  // build the map to be able to count if a cluster was shared or not
  for ( const auto itrk : *tracks ) { 

    static SG::AuxElement::ConstAccessor< MeasurementsOnTrack > acc_MeasurementsOnTrack( measurementNames  );
    const MeasurementsOnTrack& measurementsOnTrack = acc_MeasurementsOnTrack( *itrk );
    for ( auto msos_iter=measurementsOnTrack.begin(); 
        msos_iter!=measurementsOnTrack.end(); ++msos_iter ) {
      // TrackStateValidation is the track at the surface 
      const xAOD::TrackStateValidation* msos = getMSOS( msos_iter ); 
      if ( msos == nullptr) { continue; }
      // The cluster
      const xAOD::TrackMeasurementValidation* clus =  *(msos->trackMeasurementValidationLink());
      if( !clus ) { continue; }
      multiMap->insert( std::make_pair(clus, itrk) );
    }



  } // loop over tracks to make map

  for ( const auto itrk : *tracks ) { 
    nWrongSplit = 0;
    nWrongSplitTight = 0;
    nUnSplit = 0;
    nUnSplitTight = 0;

    nSplitShare = 0;

    nWrongSharePixel = 0;
    nWrongShareSCT = 0;
    nUnSharePixel = 0;
    nUnShareSCT = 0;

    static SG::AuxElement::ConstAccessor< MeasurementsOnTrack > acc_MeasurementsOnTrack( measurementNames  );
    const MeasurementsOnTrack& measurementsOnTrack = acc_MeasurementsOnTrack( *itrk );
    for ( auto msos_iter=measurementsOnTrack.begin(); 
        msos_iter!=measurementsOnTrack.end(); ++msos_iter ) {
      // TrackStateValidation is the track at the surface 
      const xAOD::TrackStateValidation* msos = getMSOS( msos_iter ); 
      if ( msos == nullptr) { continue; }
      const xAOD::TrackMeasurementValidation* clus =  *(msos->trackMeasurementValidationLink());
      if( !clus ) { continue; }

      if( !(msos->detType() == detPixel || msos->detType() == detSCT) ) { continue; }

      // count how many particles contribute
      std::vector<int>   si_barcode   = a_sihit_barcode( *clus );
      std::vector<int>   uniqueBarcode( si_barcode ); // copy

      // some barcodes in  sdo that are not in sihit vector ... matching difference!? see 
      //https://acode-browser.usatlas.bnl.gov/lxr/source/athena/InnerDetector/InDetEventCnv/InDetPrepRawDataToxAOD/src/PixelPrepDataToxAOD.cxx?v=21.2#0491
      std::vector<std::vector<int>> sdo_barcodes = a_sdo_depositsBarcode( *clus );
      for( int i=0; i<sdo_barcodes.size(); i++ ) {
        for( int j=0; j<sdo_barcodes.at(i).size(); j++ ) {
          uniqueBarcode.push_back( sdo_barcodes.at(i).at(j) ); // pack them in, to be cleaned out...
        }
      } // sdo_barcodes

      std::sort(uniqueBarcode.begin(), uniqueBarcode.end()); // sort so identical barcodes are consecutive
      // std::unique acts on consecutive duplicates and does not change vector size
      auto last = std::unique(uniqueBarcode.begin(), uniqueBarcode.end());
      uniqueBarcode.erase(last, uniqueBarcode.end());


      // strictly number of particles
      int nParticle      = uniqueBarcode.size();
      // number of reconstructable particles
      int nParticleTight = uniqueBarcode.size();
      for ( int i=0; i<uniqueBarcode.size(); i++ ) {
        auto res = std::find( std::begin(m_canRecoBarcodes), std::end(m_canRecoBarcodes), uniqueBarcode.at(i) );
        if( res != std::end(m_canRecoBarcodes) ) { continue; }
        nParticleTight--;
      }

      // pixel
      if( msos->detType() == detPixel ) { 

        /*
        std::vector<int>   si_pdgid     = a_sihit_pdgid( *clus ); // only saved for pixel hits !!! 
        // with pdgid information, can reduce
        for(int i=0; i<uniqueBarcode.size(); i++) { 
          if( uniqueBarcode.at(i) > 200e3 ) {  // is GEANT, but is it a delta ray?
            bool isDelta(false);
            // loop on the original vector to find the same barcode
            for(int j=0; j<si_pdgid.size(); j++) {
              if( uniqueBarcode.at(i) != si_barcode.at(j) ) { continue; }
              // once have index in original vector, check pdgid
              if( fabs(si_pdgid.at(j)) == 13 ) { isDelta = true; }
              break;
            }
            if(isDelta) { 
              nParticleTight--;
            }
          }
        }
        */


        if( a_isSplit( *clus ) ) { 
          if(nParticle      == 1) { nWrongSplit++; }
          if(nParticleTight == 1) { nWrongSplitTight++; }
          // split and shared should have at least 3 particles contributing
          if( multiMap->count( clus ) > 1 ) { // is shared
            nSplitShare++; // count cluster that is both split and shared
            // if split, need to see how many split to before being able to say if over/under shared TODO
//            // is split (2) + shared so should have 3 particles contributing
//            if(nParticleTight < 3) { nWrongSharePixel++; }
          }
        } else { // not split
          if(nParticle      >  1) { nUnSplit++; }
          if(nParticleTight >  1) { nUnSplitTight++; }
          // not-split and shared 
          // is shared with more than N particles contributing?
          // too many
          if( multiMap->count( clus ) > nParticleTight ) { 
            nWrongSharePixel++;

            /*
            if( trkColNum == 1 ) {
              std::cout << "shared on too many clusters " << multiMap->count( clus ) 
                << "\t" << nParticleTight << std::endl;
              std::cout << "\t" << nParticle << "\t" << nParticleTight << std::endl;
              std::cout << " barcode of tracks using cluster" << std::endl;
              auto range = multiMap->equal_range( clus );
              for (auto i = range.first; i != range.second; ++i) { // cluster, track
                auto truth = getTruth( i->second );
                if( truth ) { 
                  std::cout << "\t" << truth->barcode() << "\t" << truth->pt() << std::endl;
                } else { 
                  std::cout << "\t missing truth" << std::endl;
                }
              }
              std::cout << " barcode of particles in cluster" << std::endl;
              for ( int i=0; i<uniqueBarcode.size(); i++ ) {
                std::cout << "\t" << uniqueBarcode.at(i);
                auto res = std::find( std::begin(m_canRecoBarcodes), 
                    std::end(m_canRecoBarcodes), 
                    uniqueBarcode.at(i) );
                if( res != std::end(m_canRecoBarcodes) ) {  // found
                  std::cout << std::endl;
                } else {
                  std::cout << "\t not reco'd" << std::endl;
                }
              } // loop over uniqueBarcode
              std::cout << " barcode of particles in sdo" << std::endl;
              std::vector<std::vector<int>> sdo_barcodes = a_sdo_depositsBarcode( *clus );
              for( int i=0; i<sdo_barcodes.size(); i++ ) {
                for( int j=0; j<sdo_barcodes.at(i).size(); j++ ) {
                  std::cout << "\t" << i << "\t" << j << "\t" << sdo_barcodes.at(i).at(j) << std::endl;
                }
              } // sdo_barcodes
            } // trkColNum
            */
          }
          // not enough
          if( multiMap->count( clus ) < nParticleTight ) { nUnSharePixel++; }
        }

        /*
        if( trkColNum == 1 && multiMap->count( clus ) > 1 ) { // pseudotracks 
          std::cout << " have a shared cluster " << std::endl;
          std::cout << " \t trk " << itrk->pt() << std::endl;
          std::cout << " \t bcode " << si_barcode.size() << "\t" << uniqueBarcode.size()
            << "\t" << nParticle << "\t" << nParticleTight << std::endl;
          std::cout << "\t";
          for ( int i = 0; i<uniqueBarcode.size(); i++ ) {
            std::cout << uniqueBarcode.at(i) << "\t";
          }
          std::cout << std::endl;
          auto range = multiMap->equal_range( clus );
          for (auto i = range.first; i != range.second; ++i) { // cluster, track
            //std::cout << i->first << ": " << i->second << '\n';
            auto truth = getTruth( i->second );
            if( truth ) { 
              std::cout << i->second->pt() << "\t" << truth->barcode() << std::endl;
            } else { 
              std::cout << i->second->pt() << std::endl;
            }
          }
        }
        */

      } // pixel

      // sct
      else if ( msos->detType() == detSCT ) {
        if( multiMap->count( clus ) > 1 ) { // is shared
          // too many
          if( multiMap->count( clus ) > nParticleTight ) { nWrongShareSCT++; }
          // not enough
          if( multiMap->count( clus ) < nParticleTight ) { nUnShareSCT++; }
        } else { // not shared
          if (nParticleTight > 1) { nUnShareSCT++; }
        }

        d_nPart( *msos )     = nParticle;
        d_nPartTight( *msos) = nParticleTight;
        d_nUsed( *msos )     = multiMap->count( clus );


      } // pixel or sct
    } // loop over msos

    d_nWrongSplit( *itrk )      = nWrongSplit;
    d_nWrongSplitTight( *itrk ) = nWrongSplitTight;
    d_nUnSplit( *itrk )         = nUnSplit;
    d_nUnSplitTight( *itrk )    = nUnSplitTight;
    d_nSplitShare( *itrk )      = nSplitShare;
    d_nWrongSharePixel( *itrk ) = nWrongSharePixel;
    d_nWrongShareSCT( *itrk )   = nWrongShareSCT;
    d_nUnSharePixel( *itrk )    = nUnSharePixel;
    d_nUnShareSCT( *itrk )      = nUnShareSCT;

  } // loop over tracks

  if( trkColNum == 0 ) { m_recoClusterMap   = multiMap; }
  if( trkColNum == 1 ) { m_pseudoClusterMap = multiMap; }
  if( trkColNum == 2 ) { m_idealClusterMap  = multiMap; }

  return StatusCode::SUCCESS;
}

StatusCode IdealTrackStudyAlg::DecorateMinDR( const xAOD::TruthParticleContainer* particles, 
    const xAOD::JetContainer* jets) {

  for ( const auto ipart : *particles) { // const okay?
    float minDR(999);
    int minDRIndex(-1);
    int index_jet(0);
    for ( auto ijet : *jets ) {
      if ( ipart->p4().DeltaR( ijet->p4() ) < minDR ) { 
        minDR      = ipart->p4().DeltaR( ijet->p4() );
        minDRIndex = index_jet;
      } 
      index_jet++;
    }
    if( minDRIndex >= 0 ) {
      d_minDR( *ipart )       = minDR;
      d_minDRPt( *ipart )     = jets->at(minDRIndex)->pt();
      d_minDRFlavor( *ipart ) = a_flavor( *(jets->at(minDRIndex)) );
    } else {
      d_minDR( *ipart )       = -999;
      d_minDRPt( *ipart )     = -999;
      d_minDRFlavor( *ipart ) = -999;
    }
  }

  return StatusCode::SUCCESS;
}

StatusCode IdealTrackStudyAlg::DecorateMinDR( const xAOD::TrackParticleContainer* tracks, 
    const xAOD::JetContainer* jets) {

  for ( const auto itrk : *tracks ) { // const okay?
    float minDR(999);
    int minDRIndex(-1);
    int index_jet(0);
    for ( auto ijet : *jets ) {
      if ( itrk->p4().DeltaR( ijet->p4() ) < minDR ) { 
        minDR      = itrk->p4().DeltaR( ijet->p4() );
        minDRIndex = index_jet;
      } 
      index_jet++;
    }

    // get the associated truth
    // if the truth particle has been decorated, use that info, if not, calculate it
    // ..will this ever be different?
    if( minDRIndex >= 0 ) {
      auto truth = getTruth( itrk );
      if( truth ) {
        d_minDRTruth( *itrk ) = truth->p4().DeltaR( jets->at(minDRIndex)->p4() );
      } else {
        d_minDRTruth( *itrk ) = -999;
      }
      d_minDR( *itrk ) = minDR;
      d_minDRPt( *itrk )     = jets->at(minDRIndex)->pt();
      d_minDRFlavor( *itrk ) = a_flavor( *(jets->at(minDRIndex)) );
    } else {
      d_minDR( *itrk )       = -999;
      d_minDRTruth( *itrk )  = -999;
      d_minDRPt( *itrk )     = -999;
      d_minDRFlavor( *itrk ) = -999;
    }
  }

  return StatusCode::SUCCESS;
}

//--------------------------------------------------------------------------//
//--------------------------------------------------------------------------//
// SOLVING FOR LOCAL PARAMETERS
//--------------------------------------------------------------------------//
//--------------------------------------------------------------------------//

bool IdealTrackStudyAlg::SolveForErrorsAndLocations(float rbx, float rby, float pbx, float pby, float rux, float ruy, float pux, float puy, float trkbx, float trkby, float trkux, float trkuy,
    bool firstPass,
    float& trkBiasedCovXY, float& trkBiasedCovXX, float& trkBiasedCovYY,
    float& clusCovXX, float& clusCovYY, 
    float& trkUnbiasedCovXX, float& trkUnbiasedCovYY) {

  // first get the cross term ... it sucks
  // there are two roots - take one, and get other parameters
  // if a diagonal element of any covariance matrix is negative, 
  // call this function again using the other solution for the cross-term
  // the smaller one tends to be correct, so start with that
  std::pair<float,float> covXYs = GetTrackBiasedCovXY(rbx, rby, pbx, pby, rux, pux, trkbx, trkux);
  if(firstPass) { trkBiasedCovXY = covXYs.first;  } // smaller one in abs value
  else          { trkBiasedCovXY = covXYs.second; } // larger  one in abs value

  // with that in hand, do the rest 
  // biased track errors in X and Y
  trkBiasedCovXX = GetTrackBiasedCovXX(rbx, rby, pbx, pby, trkbx, trkux, trkBiasedCovXY);
  trkBiasedCovYY = GetTrackBiasedCovYY(rbx, rby, pbx, pby, trkby, trkuy, trkBiasedCovXY);

  // cluster errors
  clusCovXX = GetClusterCovXX(rbx, pbx, trkBiasedCovXX);
  clusCovYY = GetClusterCovYY(rby, pby, trkBiasedCovYY);

  // complete the set and get the track unbiased error (only x and y, not the cross term)
  trkUnbiasedCovXX = GetTrackUnbiasedCovXX(rbx, rux, pbx, pux, trkBiasedCovXX);
  trkUnbiasedCovYY = GetTrackUnbiasedCovYY(rby, ruy, pby, puy, trkBiasedCovYY);

  bool isGood(true);

  // check for negatives where they cannot be (real numbers only here!)
  if(trkBiasedCovXX<0 || trkBiasedCovYY<0 ||
      clusCovXX<0 || clusCovYY<0 ||
      trkUnbiasedCovXX<0 || trkUnbiasedCovYY<0) {

    if(firstPass) {
      // call again with firstPass = false
      isGood = this->SolveForErrorsAndLocations(
          rbx, rby, pbx, pby, 
          rux, ruy, pux, puy, 
          trkbx, trkby, trkux, trkuy,
          false,
          trkBiasedCovXY, trkBiasedCovXX, trkBiasedCovYY,
          clusCovXX, clusCovYY, 
          trkUnbiasedCovXX, trkUnbiasedCovYY);
    } else { 
      // issue a warning and return false so user can decide what to do next
      ATH_MSG_WARNING("Negative solution for square of track or cluster errors");
      isGood = false;
//      std::cout << "\t" << trkBiasedCovXX << "\t" << trkBiasedCovYY << std::endl
//          << "\t" << clusCovXX << "\t" << clusCovYY << std::endl
//          << "\t" << trkUnbiasedCovXX << "\t" << trkUnbiasedCovYY << std::endl;
    }
  } // have negatives

  return isGood;
}

std::pair<float,float> IdealTrackStudyAlg::GetTrackBiasedCovXY(float rbx, float rby, float pbx, float pby, float rux, float pux, float trkbx, float trkux) {

  double a = std::pow(pbx,2)*std::pow(pby,2)*rbx*rby*std::pow(rux,2);
  double b = std::pow(pbx,2)*std::pow(pby,2)*std::pow(pux,2)*std::pow(rbx,2)*std::pow(rby,2);
  double c = (std::pow(pbx,2)-std::pow(pby,2))*std::pow(rux,2);
  double d = std::pow(pux,2)*std::pow(rbx+trkbx-trkux,2);
  double e = std::pow(rbx+trkbx-trkux,2);
  double f = std::pow(pbx,2)*std::pow(pby,2)*(std::pow(pby,2)*std::pow(rux,2)+std::pow(pux,2)*std::pow(rbx+trkbx-trkux,2));
  
  float res_a = -((a+sqrt(-b*(c-d)*e))/f);
  float res_b = (-a+sqrt(-b*(c-d)*e))/f;

  // return with smaller in abs value first
  if(fabs(res_a)<fabs(res_b)) {
    return {res_a, res_b};
  } else {
    return {res_b, res_a};
  }
}

float IdealTrackStudyAlg::GetTrackBiasedCovXX(float rbx, float rby, float pbx, float pby, float trkbx, float trkux, float covTrkbxy) {
  float result = (-(covTrkbxy*std::pow(pby,2)*std::pow(rbx,2)*rby) + std::pow(rbx,2)*std::pow(rby,2)*(trkbx - trkux) - std::pow(covTrkbxy,2)*std::pow(pbx,2)*std::pow(pby,2)*(rbx + trkbx - trkux))/(std::pow(pbx,2)*rby*(covTrkbxy*std::pow(pby,2) + rbx*rby));
  
  return result;
}

float IdealTrackStudyAlg::GetTrackBiasedCovYY(float rbx, float rby, float pbx, float pby, float trkby, float trkuy, float covTrkbxy) {
  float result = (-(covTrkbxy*std::pow(pbx,2)*rbx*std::pow(rby,2)) + std::pow(rbx,2)*std::pow(rby,2)*(trkby - trkuy) - std::pow(covTrkbxy,2)*std::pow(pbx,2)*std::pow(pby,2)*(rby + trkby - trkuy))/(std::pow(pby,2)*rbx*(covTrkbxy*std::pow(pbx,2) + rbx*rby));

  return result;
}

/*
float IdealTrackStudyAlg::GetClusterCovXX(float rbx, float rby, float pbx, float pby, float trkbx, float trkux, float covTrkbxy) {
  float result = -(((std::pow(covTrkbxy,2)*std::pow(pbx,2)*std::pow(pby,2) - std::pow(rbx,2)*std::pow(rby,2))*(rbx + trkbx - trkux))/(std::pow(pbx,2)*rby*(covTrkbxy*std::pow(pby,2) + rbx*rby)));

  return result;
}

float IdealTrackStudyAlg::GetClusterCovYY(float rbx, float rby, float pbx, float pby, float trkby, float trkuy, float covTrkbxy) {
  float result = -(((std::pow(covTrkbxy,2)*std::pow(pbx,2)*std::pow(pby,2) - std::pow(rbx,2)*std::pow(rby,2))*(rby + trkby - trkuy))/(std::pow(pby,2)*rbx*(covTrkbxy*std::pow(pbx,2) + rbx*rby)));

  return result;
}
*/

// use the simple pull formula to get the cluster error and the unbiased track error

float IdealTrackStudyAlg::GetClusterCovXX(float rbx, float pbx, float covTrkbxx) {
  float result = (std::pow(rbx,2)/std::pow(pbx,2)) + covTrkbxx;
  return result;
}

float IdealTrackStudyAlg::GetClusterCovYY(float rby, float pby, float covTrkbyy) {
  float result = (std::pow(rby,2)/std::pow(pby,2)) + covTrkbyy;
  return result;
}

float IdealTrackStudyAlg::GetTrackUnbiasedCovXX(float rbx, float rux, float pbx, float pux, float covTrkbxx) {
  float result = (std::pow(rux,2)/std::pow(pux,2)) - (std::pow(rbx,2)/std::pow(pbx,2)) - covTrkbxx;
//  covTrkuxx -> (covTrkbxy*std::pow(pbx,2)*std::pow(pby,2)*rby*std::pow(rux,2) + rbx*std::pow(rby,2)*(std::pow(pbx,2)*std::pow(rux,2) - std::pow(pux,2)*rbx*(rbx + trkbx - trkux)) + std::pow(covTrkbxy,2)*std::pow(pbx,2)*std::pow(pby,2)*std::pow(pux,2)*(rbx + trkbx - trkux))/(std::pow(pbx,2)*std::pow(pux,2)*rby*(covTrkbxy*std::pow(pby,2) + rbx*rby))
  return result;
}

float IdealTrackStudyAlg::GetTrackUnbiasedCovYY(float rby, float ruy, float pby, float puy, float covTrkbyy) {
  float result = (std::pow(ruy,2)/std::pow(puy,2)) - (std::pow(rby,2)/std::pow(pby,2)) - covTrkbyy;
//  covTrkuyy -> (covTrkbxy*std::pow(pbx,2)*std::pow(pby,2)*rbx*std::pow(ruy,2) + std::pow(rbx,2)*rby*(std::pow(pby,2)*std::pow(ruy,2) - std::pow(puy,2)*rby*(rby + trkby - trkuy)) + std::pow(covTrkbxy,2)*std::pow(pbx,2)*std::pow(pby,2)*std::pow(puy,2)*(rby + trkby - trkuy))/(std::pow(pby,2)*std::pow(puy,2)*rbx*(covTrkbxy*std::pow(pbx,2) + rbx*rby))
  return result;
}



//--------------------------------------------------------------------------//
//--------------------------------------------------------------------------//
// FILL HISTOGRAMS 
//--------------------------------------------------------------------------//
//--------------------------------------------------------------------------//

StatusCode IdealTrackStudyAlg::fillResiduals( const xAOD::TrackParticleContainer *tracks, 
    const std::string trkColName) {

  // NEED MORE INFO TO RUN
  return StatusCode::SUCCESS;

  for ( const auto itrk : *tracks ) {
    // all tracks
    CHECK( fillResiduals( itrk, trkColName, "" ) );
    // break up into categories
    std::string type = getOriginLabel( m_trackOriginTool->getTrackOrigin( itrk ) );
    CHECK( fillResiduals( itrk, trkColName, type ) );
  }

  return StatusCode::SUCCESS;
}

StatusCode IdealTrackStudyAlg::fillResiduals( const xAOD::TrackParticle *track, 
    const std::string trkColName, const std::string type ) {

  // NEED MORE INFO TO RUN
  return StatusCode::SUCCESS;

  if ( track->pt() < m_trackPtMin ) { return StatusCode::SUCCESS; }

  // no space between type and layer since type can be ""
  // move to helper function?
  std::string baseHistName = "Residuals/" + trkColName + "/" + type + "/h_" + trkColName + type + "_Layer";


  // Find truth position and compare to track/cluster position
  // 
  // in header of trackAnalysis class
  // typedef std::vector<ElementLink< xAOD::TrackStateValidationContainer > > MeasurementsOnTrack;
  //
  static SG::AuxElement::ConstAccessor< MeasurementsOnTrack > acc_MeasurementsOnTrack( measurementNames  );
  const MeasurementsOnTrack& measurementsOnTrack = acc_MeasurementsOnTrack( *track );

  // loop through msos
  for ( auto msos_iter=measurementsOnTrack.begin(); 
      msos_iter!=measurementsOnTrack.end(); ++msos_iter ) {

    // TrackStateValidation is the track at the surface 
    const xAOD::TrackStateValidation* msos = getMSOS( msos_iter ); 
    if ( msos == nullptr) {
      //ATH_MSG_WARNING(trkColName + ": Could not get MSOS");
      continue;
    }

    // looking at NN stuff, so only pixels allowed
    if( msos->detType() != detPixel ) { continue; }


    // TrackMeasurementValidation is the cluster
    // setTrackMeasurementValidationLink on line 637 https://acode-browser.usatlas.bnl.gov/lxr/source/athena/PhysicsAnalysis/DerivationFramework/DerivationFrameworkInDet/src/TrackStateOnSurfaceDecorator.cxx?v=21.2
    // This cluster is not updated with the track parameters - incidence angle 
    // This is the analogue cluster position - same an NN_localEtaPixelIndexWeightedPosition (Phi)
    const xAOD::TrackMeasurementValidation* clus =  *(msos->trackMeasurementValidationLink());
    if( !clus ) {
      ATH_MSG_WARNING(trkColName + ": Could not get cluster");
      continue;
    }

    // focus on Barrel
    if( a_bec  ( *clus ) != 0 ) { continue; }

    std::string layStr     = std::to_string( a_layer( *clus ) );

    // seems to be hole, but sometimes only PullX is 0 - for baised pull, denominator is sqrt(clus err^2-trk err^2). If this term in side the sqrt is < 0, then the pull is set to zero
    // WTF ARE THESE? CHARGE IS NOT ZERO, TYPE IS 0 (measurement)...WTF
    /*
    if ( msos->biasedPullX() == 0 || msos->biasedPullY() == 0 ) {
      std::cout << "CHECK " << trkColName + "\t" + type << "\t" << layStr << std::endl;
      std::cout << "\t" << msos->type() << "\t" <<  a_charge( *clus ) << std::endl;
      std::cout << "\t" << msos->biasedPullX() << "\t" <<  msos->biasedPullY() << std::endl;
      continue;
    }
    */

    // should connect loop to create histos for size connect to what is done here...TODO
    // cluster size
    int sizeZ = a_sizeZ( *clus );
    std::string sizeZStr   = std::to_string( sizeZ );
    if (sizeZ>4) { sizeZStr = "4"; }
    //
    int sizePhi = a_sizePhi( *clus );
    std::string sizePhiStr = std::to_string( sizePhi );
    if (sizePhi>4) { sizePhiStr = "4"; }

    // use limit+1 for split
    if( a_isSplit( *clus ) ) { 
      sizeZStr   = "5";
      sizePhiStr = "5";
    }

    std::vector<int>   si_barcode   = a_sihit_barcode( *clus );
    std::vector<int>   uniqueBarcode( si_barcode ); // copy
    std::sort(uniqueBarcode.begin(), uniqueBarcode.end()); // sort so identical barcodes are consecutive
    // std::unique acts on consecutive duplicates and does not change vector size
    auto last = std::unique(uniqueBarcode.begin(), uniqueBarcode.end());
    uniqueBarcode.erase(last, uniqueBarcode.end());
    int nParticle = uniqueBarcode.size();
    // see above - could clean this up
    // add sdo_barcodes to get some missing barcodes (not all)
    // can also separate reconstructable barcodes from all 
    std::string nparStr   = std::to_string( nParticle );
    if ( nParticle > 4 ) { nparStr = "4"; }
    // it has got to be at least 1...right? TODO understand this better
    if ( nParticle == 0 ) { nparStr = "1"; }

    /*
       std::cout << layStr << "\t"
       << msos->unbiasedResidualX() << "\t" << msos->unbiasedResidualY() << "\t"
       << msos->unbiasedPullX() << "\t" << msos->unbiasedPullY() << "\t"
       << std::endl;
       */


    // get stuff from Tracking EDM - this is cluster position - track position
/*
    std::string id = baseHistName;
    // unbiased
    id = baseHistName + layStr + "_unbiasedResidualX";
    hist(id.c_str())->Fill( msos->unbiasedResidualX() );
    id = baseHistName + layStr + "_Size" + sizePhiStr + "_unbiasedResidualX";
    hist(id.c_str())->Fill( msos->unbiasedResidualX() );
    id = baseHistName + layStr + "_Part" + nparStr + "_unbiasedResidualX";
    hist(id.c_str())->Fill( msos->unbiasedResidualX() );
    //
    id = baseHistName + layStr + "_unbiasedResidualY";
    hist(id.c_str())->Fill( msos->unbiasedResidualY() );
    id = baseHistName + layStr + "_Size" + sizeZStr + "_unbiasedResidualY";
    hist(id.c_str())->Fill( msos->unbiasedResidualY() );
    id = baseHistName + layStr + "_Part" + nparStr + "_unbiasedResidualY";
    hist(id.c_str())->Fill( msos->unbiasedResidualY() );
    //
    id = baseHistName + layStr + "_unbiasedPullX";
    hist(id.c_str())->Fill( msos->unbiasedPullX() );
    id = baseHistName + layStr + "_Size" + sizePhiStr + "_unbiasedPullX";
    hist(id.c_str())->Fill( msos->unbiasedPullX() );
    id = baseHistName + layStr + "_Part" + nparStr + "_unbiasedPullX";
    hist(id.c_str())->Fill( msos->unbiasedPullX() );
    //
    id = baseHistName + layStr + "_unbiasedPullY";
    hist(id.c_str())->Fill( msos->unbiasedPullY() );
    id = baseHistName + layStr + "_Size" + sizeZStr + "_unbiasedPullY";
    hist(id.c_str())->Fill( msos->unbiasedPullY() );
    id = baseHistName + layStr + "_Part" + nparStr + "_unbiasedPullY";
    hist(id.c_str())->Fill( msos->unbiasedPullY() );

    // biased
    id = baseHistName + layStr + "_biasedResidualX";
    hist(id.c_str())->Fill( msos->biasedResidualX() );
    id = baseHistName + layStr + "_Size" + sizePhiStr + "_biasedResidualX";
    hist(id.c_str())->Fill( msos->biasedResidualX() );
    id = baseHistName + layStr + "_Part" + nparStr + "_biasedResidualX";
    hist(id.c_str())->Fill( msos->biasedResidualX() );
    //
    id = baseHistName + layStr + "_biasedResidualY";
    hist(id.c_str())->Fill( msos->biasedResidualY() );
    id = baseHistName + layStr + "_Size" + sizeZStr + "_biasedResidualY";
    hist(id.c_str())->Fill( msos->biasedResidualY() );
    id = baseHistName + layStr + "_Part" + nparStr + "_biasedResidualY";
    hist(id.c_str())->Fill( msos->biasedResidualY() );
    //
    id = baseHistName + layStr + "_biasedPullX";
    hist(id.c_str())->Fill( msos->biasedPullX() );
    id = baseHistName + layStr + "_Size" + sizePhiStr + "_biasedPullX";
    hist(id.c_str())->Fill( msos->biasedPullX() );
    id = baseHistName + layStr + "_Part" + nparStr + "_biasedPullX";
    hist(id.c_str())->Fill( msos->biasedPullX() );
    //
    id = baseHistName + layStr + "_biasedPullY";
    hist(id.c_str())->Fill( msos->biasedPullY() );
    id = baseHistName + layStr + "_Size" + sizeZStr + "_biasedPullY";
    hist(id.c_str())->Fill( msos->biasedPullY() );
    id = baseHistName + layStr + "_Part" + nparStr + "_biasedPullY";
    hist(id.c_str())->Fill( msos->biasedPullY() );
    */


    // reco position - should be geometric center of cluster
    float NN_localEtaPixelIndexWeightedPosition = a_localEtaPixelIndexWeightedPosition( *clus );
    float NN_localPhiPixelIndexWeightedPosition = a_localPhiPixelIndexWeightedPosition( *clus );

    // get positions from the residuals
    //
    // cluster position used in track fit
    float clusLocalX = msos->biasedResidualX() + msos->localX();
    float clusLocalY = msos->biasedResidualY() + msos->localY();

    // unbiased track position
    float trkUnbiasedLocX = clusLocalX - msos->unbiasedResidualX();
    float trkUnbiasedLocY = clusLocalY - msos->unbiasedResidualY();

    // all the errors we will solve for using a bit more math
    float trkBiasedCovXY(-999);
    float trkBiasedCovXX(-999);
    float trkBiasedCovYY(-999);
    float clusCovXX(-999);
    float clusCovYY(-999);
    float trkUnbiasedCovXX(-999);
    float trkUnbiasedCovYY(-999);


    bool isGood = SolveForErrorsAndLocations(
        // inputs
        msos->biasedResidualX(),  msos->biasedResidualY(), 
        msos->biasedPullX(),      msos->biasedPullY(), 
        msos->unbiasedResidualX(),msos->unbiasedResidualY(),
        msos->unbiasedPullX(),    msos->unbiasedPullY(),
        msos->localX(),           msos->localY(),
        trkUnbiasedLocX,          trkUnbiasedLocY,
        true, // first time calling the function
        // outputs
        trkBiasedCovXY,trkBiasedCovXX,trkBiasedCovYY,
        clusCovXX,clusCovYY,
        trkUnbiasedCovXX,trkUnbiasedCovYY);

    // if solution is not trustworthy, then let's not mess things up
    if(!isGood) { continue; }

    // 
    // ONLY TRUTH BASED THINGS BELOW THIS POINT
    //
    auto truth = getTruth( track );
    if( !truth ) {
      ATH_MSG_WARNING(trkColName + ": Could not get truth particle");
      continue;
    }


    // from the sihits, can build position
    // use the maximum energy sihit TODO improve by weighting position of all sihits
    // This is what is done to make the NN location above ... need to understand this diode function in ATENA
    // local sensor frame
    // x is phi
    // y is eta
    std::vector<float> si_eDeposit  = a_sihit_energyDeposit( *clus );
    std::vector<float> si_sPosX     = a_sihit_startPosX( *clus );
    std::vector<float> si_sPosY     = a_sihit_startPosY( *clus );
    std::vector<float> si_sPosZ     = a_sihit_startPosZ( *clus );
    std::vector<float> si_ePosX     = a_sihit_endPosX( *clus );
    std::vector<float> si_ePosY     = a_sihit_endPosY( *clus );
    std::vector<float> si_ePosZ     = a_sihit_endPosZ( *clus );

    int nSiHit(0);
    float maxE(-999);
    float maxELoc(-999);
    float sihitX(0); // phi
    float sihitY(0); // eta
    float sihitZ(0);

    // double loop - for each matching SiHit, consider all the SiHits _next_ in the collection
    // to see if they overlap. 
    // if overlapping, combine and only conshnameer new merged hits
    // this is done in ideal track code to find initial position
    int siHitPos(0);
    int siHitPos2(0);
    std::vector<int> usedHits;
    for( ; siHitPos<(int)si_barcode.size(); siHitPos++) {
      // make sure hit was not used already
      auto res = std::find(std::begin(usedHits), std::end(usedHits), siHitPos);
      if( res != std::end(usedHits) ) { continue; }
      if( si_barcode.at(siHitPos) != truth->barcode() ) { continue; }
      nSiHit++;
      int lowestXPos  = siHitPos;
      int highestXPos = siHitPos;

      // We will merge these hits
      std::vector<int> ajoiningHits;
      ajoiningHits.push_back( siHitPos );

      siHitPos2 = siHitPos+1;   
      while ( siHitPos2 != (int)si_barcode.size() ) {
        // make sure hit was not used already
        res = std::find(std::begin(usedHits), std::end(usedHits), siHitPos2);
        if( res != std::end(usedHits) ) { continue; }
        // Need to come from the same truth particle
        if( si_barcode.at(siHitPos2) != truth->barcode() ) { 
          ++siHitPos2;
          continue; 
        }

        // Check to see if the SiHits are compatible with each other.
        if (fabs((si_ePosX.at(highestXPos)-si_sPosX.at(siHitPos2)))<0.00005 &&
            fabs((si_ePosY.at(highestXPos)-si_sPosY.at(siHitPos2)))<0.00005 &&
            fabs((si_ePosZ.at(highestXPos)-si_sPosZ.at(siHitPos2)))<0.00005 )
        {
          highestXPos = siHitPos2;
          ajoiningHits.push_back( siHitPos2 );
          // Dont use hit  more than once
          usedHits.push_back( siHitPos2 );
          ++siHitPos2;
        } else if (fabs((si_sPosX.at(lowestXPos)-si_ePosX.at(siHitPos2)))<0.00005 &&
                   fabs((si_sPosY.at(lowestXPos)-si_ePosY.at(siHitPos2)))<0.00005 &&
                   fabs((si_sPosZ.at(lowestXPos)-si_ePosZ.at(siHitPos2)))<0.00005 )
        {
          lowestXPos = siHitPos2;
          ajoiningHits.push_back( siHitPos2 );
          // Dont use hit  more than once
          usedHits.push_back( siHitPos2 );
          ++siHitPos2;
        } else {
          ++siHitPos2;
        }
      } // loop over rest of SiHits to see if any overlap 

      if( ajoiningHits.size() == 0){
        ATH_MSG_WARNING("This should really never happen");
        continue;
      }else if(ajoiningHits.size() == 1){ // could be maxE hit?
        
        if( si_eDeposit.at( ajoiningHits.at(0) ) > maxE ) {
          maxE = si_eDeposit.at( ajoiningHits.at(0) );
          maxELoc = ajoiningHits.at(0);
          sihitX = 0.5*(si_sPosX.at(maxELoc) + si_ePosX.at(maxELoc)); // phi
          sihitY = 0.5*(si_sPosY.at(maxELoc) + si_ePosY.at(maxELoc)); // eta
          sihitZ = 0.5*(si_sPosZ.at(maxELoc) + si_ePosZ.at(maxELoc));
        }
        continue;
      } else {

        //  Build new SiHit and merge information together. 
        //  first check if energy is more than max energy
        ATH_MSG_DEBUG("Merging " << ajoiningHits.size() << " SiHits together." );
        float energyDep(0);
        for( auto i :  ajoiningHits){
          energyDep += si_eDeposit.at( i );
        }
        if( energyDep > maxE ) {
          maxE = energyDep;
          maxELoc = -1; // means it is not a single hit
          sihitX = 0.5*(si_sPosX.at(lowestXPos) + si_ePosX.at(highestXPos)); // phi
          sihitY = 0.5*(si_sPosY.at(lowestXPos) + si_ePosY.at(highestXPos)); // eta
          sihitZ = 0.5*(si_sPosZ.at(lowestXPos) + si_ePosZ.at(highestXPos));
        }

      } // depending on number of ajoined hits, filling matchingHits vector
    } // loop over all matching SiHits

    if( maxE < 0 ) { 
      //ATH_MSG_WARNING(trkColName + ": no matching SiHits in cluster");
//      std::cout << "\t" << truth->barcode() << std::endl;
//      for( auto bc : si_barcode ) { std::cout << "\t\t" << bc << std::endl; }
      continue; 
    }


/*
    // position/error debugging central
    static SG::AuxElement::Accessor< float >  a_clusX("clusX");
    if( a_layer( *clus ) == 0 && type == "" && a_clusX.isAvailable(*msos)) {
      std::cout << trkColName << "\t" << type << "\t Cluster, SiHit " << std::endl;
      //std::cout << "\t ID    " << msos->identifier() << std::endl;
      std::cout << "\t positions" << std::endl;
      std::cout << "\t msos    " << msos->localX() << "\t" << msos->localY() << std::endl;   // track at surface
      std::cout << "\t clus    " << clus->localX() << "\t" << clus->localY() << std::endl;   // analogue cluster
      std::cout << "\t sihit   " << sihitX << "\t" << sihitY << "\t" << sihitZ << std::endl; // truth location 
      //std::cout << "\t NN      " << clusLocalX << "\t" << clusLocalY << std::endl;
      //std::cout << "\t NN A    " << msos->auxdata<float>("clusX")  // check with athena hack
      //                 << "\t  " << msos->auxdata<float>("clusY") 
      //                 << std::endl;
      //std::cout << "\t Trk L   " << trkUnbiasedLocX << "\t" << trkUnbiasedLocY << std::endl;
      //std::cout << "\t Trk L A " << msos->auxdata<float>("trkUnbiasedX")  // check with athena hack
      //                 << "\t  " << msos->auxdata<float>("trkUnbiasedY") 
      //                 << std::endl;

      //std::cout << "\t      XY " << "\t" << trkBiasedCovXY
      //  << msos->auxdata<float>("trkBiasedErrorXY") << std::endl; // check with athena hack

      //std::cout << "\t trkEr B " << trkBiasedCovXX << "\t" << trkBiasedCovYY << std::endl;
      //std::cout << "\t         " << msos->auxdata<float>("trkBiasedErrorX") 
      //  << "\t" << msos->auxdata<float>("trkBiasedErrorY") << std::endl; // check with athena hack

      //std::cout << "\t clus Er " << clusCovXX << "\t" << clusCovYY << std::endl;
      //std::cout << "\t         " << msos->auxdata<float>("clusErrorX") 
      //  << "\t" << msos->auxdata<float>("clusErrorY") << std::endl; // check with athena hack

      //std::cout << "\t trkEr U " << trkUnbiasedCovXX << "\t" << trkUnbiasedCovYY << std::endl;
      //std::cout << "\t         " << msos->auxdata<float>("trkUnbiasedErrorX") 
      //  << "\t" << msos->auxdata<float>("trkUnbiasedErrorY") << std::endl; // check with athena hack


//      std::cout << "\t trk E " << a_trkErrBiased( *msos ) << "\t" << a_trkErrUnbiased( *msos ) << std::endl;
//      std::cout << "\t extrp " << a_trkIBLX( *track ) 
//        << "\t" << a_trkIBLY( *track ) 
//        << "\t" << a_trkIBLZ( *track ) << std::endl;
      // analoguy cluster residual to truth and error
      //std::cout << "\t" << clus->localX() - sihitX << "\t" << sqrt(clus->localXError())
      //  << "\t" << clus->localY() - sihitY << "\t" << sqrt(clus->localYError())
      //  << std::endl;
    }
*/


    /*
    // truth compared to cluster position
    id = baseHistName + layStr + "_clusterResidualX";
    hist(id.c_str())->Fill( clusLocalX - sihitX );
    id = baseHistName + layStr + "_Size" + sizePhiStr + "_clusterResidualX";
    hist(id.c_str())->Fill( clusLocalX - sihitX );
    id = baseHistName + layStr + "_Part" + nparStr + "_clusterResidualX";
    hist(id.c_str())->Fill( clusLocalX - sihitX );
    id = baseHistName + layStr + "_clusterResidualY";
    hist(id.c_str())->Fill( clusLocalY - sihitY );
    id = baseHistName + layStr + "_Size" + sizeZStr + "_clusterResidualY";
    hist(id.c_str())->Fill( clusLocalY - sihitY );
    id = baseHistName + layStr + "_Part" + nparStr + "_clusterResidualY";
    hist(id.c_str())->Fill( clusLocalY - sihitY );
    //
    id = baseHistName + layStr + "_clusterPullX";
    hist(id.c_str())->Fill( (clusLocalX - sihitX)/sqrt(clusCovXX) );
    id = baseHistName + layStr + "_Size" + sizePhiStr + "_clusterPullX";
    hist(id.c_str())->Fill( (clusLocalX - sihitX)/sqrt(clusCovXX) );
    id = baseHistName + layStr + "_Part" + nparStr + "_clusterPullX";
    hist(id.c_str())->Fill( (clusLocalX - sihitX)/sqrt(clusCovXX) );
    id = baseHistName + layStr + "_clusterPullY";
    hist(id.c_str())->Fill( (clusLocalY - sihitY)/sqrt(clusCovYY) );
    id = baseHistName + layStr + "_Size" + sizeZStr + "_clusterPullY";
    hist(id.c_str())->Fill( (clusLocalY - sihitY)/sqrt(clusCovYY) );
    id = baseHistName + layStr + "_Part" + nparStr + "_clusterPullY";
    hist(id.c_str())->Fill( (clusLocalY - sihitY)/sqrt(clusCovYY) );
    //
    id = baseHistName + layStr + "_clusterErrorX";
    hist(id.c_str())->Fill( sqrt(clusCovXX) );
    id = baseHistName + layStr + "_clusterErrorY";
    hist(id.c_str())->Fill( sqrt(clusCovYY) );


    // truth compared to track position
    // biased
    id = baseHistName + layStr + "_trackBiasedResidualX";
    hist(id.c_str())->Fill( msos->localX() - sihitX );
    id = baseHistName + layStr + "_Size" + sizePhiStr + "_trackBiasedResidualX";
    hist(id.c_str())->Fill( msos->localX() - sihitX );
    id = baseHistName + layStr + "_Part" + nparStr + "_trackBiasedResidualX";
    hist(id.c_str())->Fill( msos->localX() - sihitX );
    id = baseHistName + layStr + "_trackBiasedResidualY";
    hist(id.c_str())->Fill( msos->localY() - sihitY );
    id = baseHistName + layStr + "_Size" + sizeZStr + "_trackBiasedResidualY";
    hist(id.c_str())->Fill( msos->localY() - sihitY );
    id = baseHistName + layStr + "_Part" + nparStr + "_trackBiasedResidualY";
    hist(id.c_str())->Fill( msos->localY() - sihitY );
    // unbiased
    id = baseHistName + layStr + "_trackUnbiasedResidualX";
    hist(id.c_str())->Fill( trkUnbiasedLocX - sihitX );
    id = baseHistName + layStr + "_Size" + sizePhiStr + "_trackUnbiasedResidualX";
    hist(id.c_str())->Fill( trkUnbiasedLocX - sihitX );
    id = baseHistName + layStr + "_Part" + nparStr + "_trackUnbiasedResidualX";
    hist(id.c_str())->Fill( trkUnbiasedLocX - sihitX );
    id = baseHistName + layStr + "_trackUnbiasedResidualY";
    hist(id.c_str())->Fill( trkUnbiasedLocY - sihitY );
    id = baseHistName + layStr + "_Size" + sizeZStr + "_trackUnbiasedResidualY";
    hist(id.c_str())->Fill( trkUnbiasedLocY - sihitY );
    id = baseHistName + layStr + "_Part" + nparStr + "_trackUnbiasedResidualY";
    hist(id.c_str())->Fill( trkUnbiasedLocY - sihitY );
    // use biased error
    id = baseHistName + layStr + "_trackBiasedPullX";
    hist(id.c_str())->Fill( (msos->localX() - sihitX)/sqrt(trkBiasedCovXX) );
    id = baseHistName + layStr + "_Size" + sizePhiStr + "_trackBiasedPullX";
    hist(id.c_str())->Fill( (msos->localX() - sihitX)/sqrt(trkBiasedCovXX) );
    id = baseHistName + layStr + "_Part" + nparStr + "_trackBiasedPullX";
    hist(id.c_str())->Fill( (msos->localX() - sihitX)/sqrt(trkBiasedCovXX) );
    id = baseHistName + layStr + "_trackBiasedPullY";
    hist(id.c_str())->Fill( (msos->localY() - sihitY)/sqrt(trkBiasedCovYY) );
    id = baseHistName + layStr + "_Size" + sizeZStr + "_trackBiasedPullY";
    hist(id.c_str())->Fill( (msos->localY() - sihitY)/sqrt(trkBiasedCovYY) );
    id = baseHistName + layStr + "_Part" + nparStr + "_trackBiasedPullY";
    hist(id.c_str())->Fill( (msos->localY() - sihitY)/sqrt(trkBiasedCovYY) );
    // use biased error
    id = baseHistName + layStr + "_trackUnbiasedPullX";
    hist(id.c_str())->Fill( (trkUnbiasedLocX - sihitX)/sqrt(trkUnbiasedCovXX) );
    id = baseHistName + layStr + "_Size" + sizePhiStr + "_trackUnbiasedPullX";
    hist(id.c_str())->Fill( (trkUnbiasedLocX - sihitX)/sqrt(trkUnbiasedCovXX) );
    id = baseHistName + layStr + "_Part" + nparStr + "_trackUnbiasedPullX";
    hist(id.c_str())->Fill( (trkUnbiasedLocX - sihitX)/sqrt(trkUnbiasedCovXX) );
    id = baseHistName + layStr + "_trackUnbiasedPullY";
    hist(id.c_str())->Fill( (trkUnbiasedLocY - sihitY)/sqrt(trkUnbiasedCovYY) );
    id = baseHistName + layStr + "_Size" + sizeZStr + "_trackUnbiasedPullY";
    hist(id.c_str())->Fill( (trkUnbiasedLocY - sihitY)/sqrt(trkUnbiasedCovYY) );
    id = baseHistName + layStr + "_Part" + nparStr + "_trackUnbiasedPullY";
    hist(id.c_str())->Fill( (trkUnbiasedLocY - sihitY)/sqrt(trkUnbiasedCovYY) );
    */

    /*
       std::cout << "\t" << clus->identifier() 
       << "\t" << NN_localEtaPixelIndexWeightedPosition 
       << "\t" << NN_localPhiPixelIndexWeightedPosition
       << "\t" << nSiHit
       << "\t" << sihitY // eta
       << "\t" << sihitX // phi
       << "\t" << msos->localY()
       << "\t" << msos->localX()
    //<< "\t" << sihitZ // depth
    //<< "\t" << a_trkIBLX( *track ) 
    //<< "\t" << a_trkIBLY( *track ) 
    //<< "\t" << a_trkIBLZ( *track ) 
    << std::endl;
    */


  } // loop over track msos

  return StatusCode::SUCCESS;
}


StatusCode IdealTrackStudyAlg::fillHitPatterns( const xAOD::TrackParticleContainer *tracks, 
    const std::string trkColName) {

  for ( const auto itrk : *tracks ) {
    // all tracks
    CHECK( fillHitPatterns( itrk, trkColName, "" ) );
    // break up into categories
    std::string type = getOriginLabel( m_trackOriginTool->getTrackOrigin( itrk ) );
    CHECK( fillHitPatterns( itrk, trkColName, type ) );
  }

  return StatusCode::SUCCESS;
}

StatusCode IdealTrackStudyAlg::fillHitPatterns( const xAOD::TrackParticle *track, 
    const std::string trkColName, const std::string type ) {

  if ( track->pt() < m_trackPtMin ) { return StatusCode::SUCCESS; }

  int nHit(0);
  TH1* hist = nullptr;
  // move to helper function?
  std::string base = "/"+m_streamName+"/HitPattern/" + trkColName + "/" + type + "/h_" + trkColName + type + "_";
  auto id = base;

  // TrackInfo
  // https://acode-browser.usatlas.bnl.gov/lxr/source/athena/Event/xAOD/xAODTracking/xAODTracking/TrackingPrimitives.h?v=21.2#0079
  id = base + "patternRecoInfo";
  const std::bitset<xAOD::NumberOfTrackRecoInfo> patternReco = track->patternRecoInfo();
  ATH_CHECK( m_thistSvc->getHist( id.c_str(), hist ) );
  for( int i = 0; i< xAOD::TrackPatternRecoInfo::NumberOfTrackRecoInfo; i++ ) {
    if( patternReco.test(i) ) {
      hist->Fill( i );
    }
  }
  //<< "\t" << patternReco.test(49) << "\t" << patternReco.test(xAOD::TrackPatternRecoInfo::SiSpacePointsSeedMaker_LargeD0) 
  //<< "\t" << patternReco.test(19) << "\t" << patternReco.test(xAOD::TrackPatternRecoInfo::InDetAmbiTrackSelectionTool) 

  
  // Pixel
  nHit = a_pixelHits( *track );
  id = base + "numberOfPixelHits";
  ATH_CHECK( m_thistSvc->getHist( id.c_str(), hist ) );
  hist->Fill( nHit );

  nHit = a_pixelHoles( *track );
  id = base + "numberOfPixelHoles";
  ATH_CHECK( m_thistSvc->getHist( id.c_str(), hist ) );
  hist->Fill( nHit );

  nHit = a_pixelSharedHits( *track );
  id = base + "numberOfPixelSharedHits";
  ATH_CHECK( m_thistSvc->getHist( id.c_str(), hist ) );
  hist->Fill( nHit );

  // if have shared hits, tell me more
  if(nHit>0) {
    id = base + "numberOfPixelWrongShareHits";
    ATH_CHECK( m_thistSvc->getHist( id.c_str(), hist ) );
    hist->Fill( a_nWrongSharePixel(*track) );
    id = base + "numberOfPixelUnderShareHits";
    ATH_CHECK( m_thistSvc->getHist( id.c_str(), hist ) );
    hist->Fill( a_nUnSharePixel(*track) );
  } else {
    id = base + "numberOfPixelUnShareHits";
    ATH_CHECK( m_thistSvc->getHist( id.c_str(), hist ) );
    hist->Fill( a_nUnSharePixel(*track) );
  }

  nHit = a_pixelSplitHits( *track );
  id = base + "numberOfPixelSplitHits";
  ATH_CHECK( m_thistSvc->getHist( id.c_str(), hist ) );
  hist->Fill( nHit );

  // if have split hits, tell me more
  if( nHit > 0 ) {
    id = base + "numberOfPixelWrongSplitHits";
    ATH_CHECK( m_thistSvc->getHist( id.c_str(), hist ) );
    hist->Fill( a_nWrongSplit(*track) );
    id = base + "numberOfPixelWrongSplitHitsTight";
    ATH_CHECK( m_thistSvc->getHist( id.c_str(), hist ) );
    hist->Fill( a_nWrongSplitTight(*track) );
    id = base + "numberOfPixelSplitShareHits";
    ATH_CHECK( m_thistSvc->getHist( id.c_str(), hist ) );
    hist->Fill( a_nSplitShare(*track) );
  }

  id = base + "numberOfPixelUnSplitHits";
  ATH_CHECK( m_thistSvc->getHist( id.c_str(), hist ) );
  hist->Fill( a_nUnSplit(*track) );
  id = base + "numberOfPixelUnSplitHitsTight";
  ATH_CHECK( m_thistSvc->getHist( id.c_str(), hist ) );
  hist->Fill( a_nUnSplitTight(*track) );


  // IBL
  nHit = track->auxdata<uint8_t>("numberOfInnermostPixelLayerHits");
  id = base + "numberOfInnermostPixelLayerHits";
  ATH_CHECK( m_thistSvc->getHist( id.c_str(), hist ) );
  hist->Fill( nHit );

  nHit = track->auxdata<uint8_t>("numberOfInnermostPixelLayerSharedHits");
  id = base + "numberOfInnermostPixelLayerSharedHits";
  ATH_CHECK( m_thistSvc->getHist( id.c_str(), hist ) );
  hist->Fill( nHit );

  nHit = track->auxdata<uint8_t>("numberOfInnermostPixelLayerSplitHits");
  id = base + "numberOfInnermostPixelLayerSplitHits";
  ATH_CHECK( m_thistSvc->getHist( id.c_str(), hist ) );
  hist->Fill( nHit );

  nHit = track->auxdata<uint8_t>("expectInnermostPixelLayerHit");
  id = base + "expectInnermostPixelLayerHit";
  ATH_CHECK( m_thistSvc->getHist( id.c_str(), hist ) );
  hist->Fill( nHit );


  // SCT
  nHit = a_sctHits( *track );
  id = base + "numberOfSCTHits";
  ATH_CHECK( m_thistSvc->getHist( id.c_str(), hist ) );
  hist->Fill( nHit );

  nHit = a_sctHoles( *track );
  id = base + "numberOfSCTHoles";
  ATH_CHECK( m_thistSvc->getHist( id.c_str(), hist ) );
  hist->Fill( nHit );

  nHit = a_sctSharedHits( *track );
  id = base + "numberOfSCTSharedHits";
  ATH_CHECK( m_thistSvc->getHist( id.c_str(), hist ) );
  hist->Fill( nHit );

  if(nHit>0) {
    id = base + "numberOfSCTWrongShareHits";
    ATH_CHECK( m_thistSvc->getHist( id.c_str(), hist ) );
    hist->Fill( a_nWrongShareSCT(*track) );
    id = base + "numberOfSCTUnderShareHits";
    ATH_CHECK( m_thistSvc->getHist( id.c_str(), hist ) );
    hist->Fill( a_nUnShareSCT(*track) );
  } else {
    id = base + "numberOfSCTUnShareHits";
    ATH_CHECK( m_thistSvc->getHist( id.c_str(), hist ) );
    hist->Fill( a_nUnShareSCT(*track) );
  }

  nHit = a_pixelHits( *track ) + track->auxdata<uint8_t>("numberOfSCTHits");
  id = base + "numberOfSiHits";
  ATH_CHECK( m_thistSvc->getHist( id.c_str(), hist ) );
  hist->Fill( nHit );

  float nShMod = a_pixelSharedHits( *track ) + a_sctSharedHits( *track )/2.;
  id = base + "numberOfSharedModules";
  ATH_CHECK( m_thistSvc->getHist( id.c_str(), hist ) );
  hist->Fill( nShMod );

  nHit = a_pixelHoles( *track ) + a_sctHoles( *track );
  id = base + "numberOfSiHoles";
  ATH_CHECK( m_thistSvc->getHist( id.c_str(), hist ) );
  hist->Fill( nHit );

  // truth
  if( a_truthProb.isAvailable( *track ) ) {
    id = base + "truthMatchProbability";
    ATH_CHECK( m_thistSvc->getHist( id.c_str(), hist ) );
    hist->Fill( a_truthProb( *track ) );
  }

  // SCT Width
  /*
  int nParticle(0);
  int nParticleTight(0);
  int nUsed(0);
  static SG::AuxElement::ConstAccessor< MeasurementsOnTrack > acc_MeasurementsOnTrack( measurementNames  );
  const MeasurementsOnTrack& measurementsOnTrack = acc_MeasurementsOnTrack( *track );
  for ( auto msos_iter=measurementsOnTrack.begin(); 
      msos_iter!=measurementsOnTrack.end(); ++msos_iter ) {
    // TrackStateValidation is the track at the surface 
    const xAOD::TrackStateValidation* msos = getMSOS( msos_iter ); 
    if ( msos == nullptr) { continue; }
    if ( msos->detType() != detSCT ) { continue; }
    const xAOD::TrackMeasurementValidation* clus =  *(msos->trackMeasurementValidationLink());
    if( !clus ) { continue; }

    // focus on barrel for now
    if( a_bec  ( *clus ) != 0 ) { continue; }

    nParticle      = a_nPart( *msos );
    nParticleTight = a_nPartTight( *msos );
    nUsed          = a_nUsed( *msos );

    if( nUsed > 1 and (nUsed > nParticleTight) ) {
      id = base + "nPartWrongShare";
      ATH_CHECK( m_thistSvc->getHist( id.c_str(), hist ) );
      hist->Fill( nParticleTight );
      id = base + "nPartDiffWrongShare";
      ATH_CHECK( m_thistSvc->getHist( id.c_str(), hist ) );
      hist->Fill( nUsed-nParticleTight );
    }

    // https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/CONFNOTES/ATLAS-CONF-2013-005/
    float diff = (285.*abs( tan(msos->localPhi())-tan(-0.07)) ) -  80*a_siWidth( *clus );
    //std::cout << " dff " << diff << std::endl;
    if( nParticle == 1 and nUsed == 1 ) { 
      id = base + "widthDiffNP1NU1";
      ATH_CHECK( m_thistSvc->getHist( id.c_str(), hist ) );
      hist->Fill( diff );
      //std::cout << "layer " << a_layer(*clus) << "\t" << diff << std::endl;
    }
    if( nParticle == 1 and nUsed >  1 ) { 
      id = base + "widthDiffNP1NU2";
      ATH_CHECK( m_thistSvc->getHist( id.c_str(), hist ) );
      hist->Fill( diff );
      //std::cout << "layer " << a_layer(*clus) << "\t" << diff << std::endl;
    }
    if( nParticle >  1 and nUsed >  1 ) { 
      id = base + "widthDiffNP2NU2";
      ATH_CHECK( m_thistSvc->getHist( id.c_str(), hist ) );
      hist->Fill( diff );
      //std::cout << "layer " << a_layer(*clus) << "\t" << diff << std::endl;
    }
    if( nParticle >  1 and nUsed == 1 ) { 
      id = base + "widthDiffNP2NU1";
      ATH_CHECK( m_thistSvc->getHist( id.c_str(), hist ) );
      hist->Fill( diff );
      //std::cout << "layer " << a_layer(*clus) << "\t" << diff << std::endl;
    }
  }
  */

  return StatusCode::SUCCESS;
}

StatusCode IdealTrackStudyAlg::fillTrkPResiduals( const xAOD::TrackParticleContainer *tracks, 
    const std::string trkColName) {

  // NEED MORE INFO TO RUN
  return StatusCode::SUCCESS;

  for ( const auto itrk : *tracks ) {
    // all tracks
    CHECK( fillTrkPResiduals( itrk, trkColName, "" ) );
    // break up into categories
    std::string type = getOriginLabel( m_trackOriginTool->getTrackOrigin( itrk ) );
    CHECK( fillTrkPResiduals( itrk, trkColName, type ) );
  }

  return StatusCode::SUCCESS;
}

StatusCode IdealTrackStudyAlg::fillTrkPResiduals( const xAOD::TrackParticle *track, 
    const std::string trkColName, const std::string type ) {

  // NEED MORE INFO TO RUN
  return StatusCode::SUCCESS;

  if ( track->pt() < m_trackPtMin ) { return StatusCode::SUCCESS; }

  // no space between type and layer since type can be ""
  // move to helper function?
  std::string base = "TrkPResiduals/" + trkColName + "/" + type + "/h_" + trkColName + type + "_";
  auto id = base;

  // all below are truth based
  auto truth = getTruth(track);
  if ( !truth ) { return StatusCode::SUCCESS; }

//  std::cout << track->d0() << "\t" << track->z0() 
//    << "\t" << track->phi() << "\t" << track->theta() 
//    << "\t" << track->qOverP() << std::endl;


  //float etaResidual      = track->eta()    - truth->eta();
  float d0Residual       = track->d0()     - a_d0(*truth);
  float z0Residual       = track->z0()     - a_z0(*truth);
  float phiResidual      = track->phi()    - truth->phi();
  // keep between -pi and pi
  double pi = 2 * std::acos(0.0);
  if ( phiResidual < -pi ) { phiResidual = phiResidual + 2*pi; }
  if ( phiResidual >  pi ) { phiResidual = phiResidual - 2*pi; }

  float thetaResidual    = track->theta()  - a_theta(*truth);
  float qOverPResidual   = track->qOverP() - (truth->charge() / (truth->pt() * std::cosh(truth->eta())));

  
  float d0Pull       = d0Residual     / TMath::Sqrt(track->definingParametersCovMatrix()(0, 0));
  float z0Pull       = z0Residual     / TMath::Sqrt(track->definingParametersCovMatrix()(1, 1));
  float phiPull      = phiResidual    / TMath::Sqrt(track->definingParametersCovMatrix()(2, 2));
  float thetaPull    = thetaResidual  / TMath::Sqrt(track->definingParametersCovMatrix()(3, 3));
  float qOverPPull   = qOverPResidual / TMath::Sqrt(track->definingParametersCovMatrix()(4, 4));

  /*
  id = base + "D0";
  hist(id.c_str())->Fill( track->d0() );
  id = base + "Z0";
  // https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/InDetTrackingDC14
  hist(id.c_str())->Fill( track->z0() + track->vz() ); // track->vz() represents the point of reference for the z0 calculation (in this case, the beamspot position along the z axis).
  id = base + "Phi";
  hist(id.c_str())->Fill( track->phi() );
  id = base + "Theta";
  hist(id.c_str())->Fill( track->theta() );
  id = base + "qOverP";
  hist(id.c_str())->Fill( track->qOverP() );
  id = base + "pT";
  hist(id.c_str())->Fill( track->pt() );

  id = base + "D0Truth";
  hist(id.c_str())->Fill( a_d0(*truth) );
  id = base + "Z0Truth";
  hist(id.c_str())->Fill( a_z0(*truth) + track->vz() );
  id = base + "PhiTruth";
  hist(id.c_str())->Fill( truth->phi() );
  id = base + "ThetaTruth";
  hist(id.c_str())->Fill( a_theta(*truth) );
  id = base + "qOverPTruth";
  hist(id.c_str())->Fill( (truth->charge() / (truth->pt() * std::cosh(truth->eta()))) );
  id = base + "pTTruth";
  hist(id.c_str())->Fill( truth->pt() );
                                       
  id = base + "D0Residual";
  hist(id.c_str())->Fill( d0Residual );
  id = base + "D0Pull";
  hist(id.c_str())->Fill( d0Pull );
                                       
  id = base + "Z0Residual";
  hist(id.c_str())->Fill( z0Residual );
  id = base + "Z0Pull";
  hist(id.c_str())->Fill( z0Pull );

  id = base + "PhiResidual";
  hist(id.c_str())->Fill( phiResidual );
  id = base + "PhiPull";
  hist(id.c_str())->Fill( phiPull );
                                       
  id = base + "ThetaResidual";
  hist(id.c_str())->Fill( thetaResidual );
  id = base + "ThetaPull";
  hist(id.c_str())->Fill( thetaPull );
                                       
  id = base + "qOverPResidual";
  hist(id.c_str())->Fill( qOverPResidual*1e3 );
  id = base + "qOverPPull";
  hist(id.c_str())->Fill( qOverPPull );

  // 2D
  id = base + "D0TruthVsD0Residual";
  hist(id.c_str())->Fill( a_d0(*truth), d0Residual );

  id = base + "D0TruthVsD0Pull";
  hist(id.c_str())->Fill( a_d0(*truth), d0Pull );

  id = base + "Z0TruthVsZ0Residual";
  hist(id.c_str())->Fill( a_d0(*truth), d0Residual );

  id = base + "Z0TruthVsZ0Pull";
  hist(id.c_str())->Fill( a_d0(*truth), d0Pull );

  id = base + "EtaTruthVsD0Residual";
  hist(id.c_str())->Fill( truth->eta(), d0Residual );

  id = base + "EtaTruthVsZ0Residual";
  hist(id.c_str())->Fill( truth->eta(), z0Residual );

  id = base + "EtaTruthVsD0Pull";
  hist(id.c_str())->Fill( truth->eta(), d0Pull );

  id = base + "EtaTruthVsZ0Pull";
  hist(id.c_str())->Fill( truth->eta(), z0Pull );

  id = base + "PhiTruthVsPhiResidual";
  hist(id.c_str())->Fill( truth->phi(), phiResidual );

  id = base + "PhiTruthVsPhiPull";
  hist(id.c_str())->Fill( truth->phi(), phiPull );

  id = base + "PtTruthVsD0Residual";
  hist(id.c_str())->Fill( truth->pt(), d0Residual );

  id = base + "PtTruthVsZ0Residual";
  hist(id.c_str())->Fill( truth->pt(), z0Residual );

  id = base + "PtTruthVsD0Pull";
  hist(id.c_str())->Fill( truth->pt(), d0Pull );

  id = base + "PtTruthVsZ0Pull";
  hist(id.c_str())->Fill( truth->pt(), z0Pull );
  */

  return StatusCode::SUCCESS;
}

StatusCode IdealTrackStudyAlg::fillZ0Bias( const xAOD::TrackParticle *track, 
    const std::string trkColName ) {
/*
  auto id = "z0Bias/h_" + trkColName + "_eta_z0";
  hist(id.c_str())->Fill( track->eta(), track->z0() - m_pvZ0 );

  id = id + "_truth";
  auto truth = getTruth(track);
  if( truth ) {
    hist(id.c_str())->Fill( truth->eta(), truth->auxdata<float>("z0") - m_pvZ0 );
  }
*/
  return StatusCode::SUCCESS;
}

StatusCode IdealTrackStudyAlg::fillRateHists( const xAOD::TrackParticleContainer *tracks, 
    const std::string trkColName) {

  bool fillReco(false);
  if (trkColName == "Reco") { fillReco = true; } // will fill set with reco quantities

  int origin(-999);
  bool isFake(false);
  for ( const auto itrk : *tracks ) {

    origin = m_trackOriginTool->getTrackOrigin( itrk );
    if( fillReco ) { 
      isFake = InDet::TrkOrigin::isFake(origin); 

      // apply selection if needed but only apply to reco tracks
      if( m_applyTrackSelection ) {
        if( ! m_trackSelTool->accept( *itrk , m_primaryVertex ) ) continue;
      }

    }

    // fill all
    CHECK( fillRateHists( itrk, trkColName, "", fillReco, isFake ) );
    
    // if not in a jet, do not continue
    if( a_minDR( *itrk ) < 0 ) { continue; }

    if ( a_minDRFlavor( *itrk ) == 5 ) {
      CHECK( fillRateHists( itrk, trkColName, "BJet", fillReco, isFake ) );
      if( InDet::TrkOrigin::isFromB(origin) ) { // includes B->D
        CHECK( fillRateHists( itrk, trkColName, "BHadron", fillReco, isFake ) );
      }

    } else if ( a_minDRFlavor( *itrk ) == 4 ) {
      CHECK( fillRateHists( itrk, trkColName, "CJet", fillReco, isFake ) );
      if( InDet::TrkOrigin::isFromD(origin) ) {
        CHECK( fillRateHists( itrk, trkColName, "CHadron", fillReco, isFake ) );
      }

    } else {
      CHECK( fillRateHists( itrk, trkColName, "LightJet", fillReco, isFake ) );
    }
  }

  return StatusCode::SUCCESS;
}

StatusCode IdealTrackStudyAlg::fillRateHists( const xAOD::TrackParticle *track, 
    const std::string trkColName, const std::string type,
    bool fillReco, bool isFake) { 
  CHECK( fillRateHists( track, trkColName, type, true) ); // use truth information
  if(fillReco) {
    CHECK( fillRateHists( track, "RecoReco", type, false) ); // use reco information
    if(isFake) {
      CHECK( fillRateHists( track, "RecoFake", type, false) ); // use reco information
    }
  }
  return StatusCode::SUCCESS;
}

StatusCode IdealTrackStudyAlg::fillRateHists( const xAOD::TrackParticle *track, 
    const std::string trkColName, const std::string type,
    bool useTruth) { 

  std::string base = "/"+m_streamName+"/Rate/" + trkColName + "/" + type + "/h_Rate_" + trkColName + type + "_";
  auto id = base;

  auto truth = getTruth( track );

  TH1* hist = nullptr;

  // derivation thing missing in master to decorrate truth with track parameters
  useTruth = false;


  if(useTruth) {
    if( !truth ) { return StatusCode::SUCCESS; } // okay? TODO FIXME...think about implications
    id = base + "pT";
    ATH_CHECK( m_thistSvc->getHist( id.c_str(), hist ) );
    hist->Fill( truth->pt() );
    id = base + "Eta";
    ATH_CHECK( m_thistSvc->getHist( id.c_str(), hist ) );
    hist->Fill( truth->eta() );
    id = base + "D0";
    ATH_CHECK( m_thistSvc->getHist( id.c_str(), hist ) );
    hist->Fill( a_d0(*truth) );
    id = base + "Z0";
    ATH_CHECK( m_thistSvc->getHist( id.c_str(), hist ) );
    hist->Fill( a_z0(*truth) + track->vz() );

    if(m_hasJets && a_minDR(*track)>0) { 
      id = base + "JetPt";
      ATH_CHECK( m_thistSvc->getHist( id.c_str(), hist ) );
      hist->Fill( a_minDRPt( *track )/1e3 );  // GeV

      id = base + "JetDR";
      ATH_CHECK( m_thistSvc->getHist( id.c_str(), hist ) );
      // first check if truth particle is already decorrated
      if( a_minDR.isAvailable( *truth ) && a_minDR( *truth ) > 0 ) {
        hist->Fill( a_minDR( *truth ) );
      } else {
        // if not, use truth dR decorrated
        hist->Fill( a_minDRTruth( *track ) );
      }
    }

    id = base + "CreationVtxLG";
    ATH_CHECK( m_thistSvc->getHist( id.c_str(), hist ) );
    hist->Fill( truth->prodVtx()->perp() );
    id = base + "CreationVtx";
    ATH_CHECK( m_thistSvc->getHist( id.c_str(), hist ) );
    hist->Fill( truth->prodVtx()->perp() );

  } else {
    id = base + "pT";
    ATH_CHECK( m_thistSvc->getHist( id.c_str(), hist ) );
    hist->Fill( track->pt() );
    id = base + "Eta";
    ATH_CHECK( m_thistSvc->getHist( id.c_str(), hist ) );
    hist->Fill( track->eta() );
    id = base + "D0";
    ATH_CHECK( m_thistSvc->getHist( id.c_str(), hist ) );
    hist->Fill( track->d0() );
    id = base + "Z0";
    ATH_CHECK( m_thistSvc->getHist( id.c_str(), hist ) );
    hist->Fill( track->z0() + track->vz() );

    if(m_hasJets && a_minDR(*track)>0) { 
      id = base + "JetDR";
      ATH_CHECK( m_thistSvc->getHist( id.c_str(), hist ) );
      hist->Fill( a_minDR( *track ) );
      id = base + "JetPt";
      ATH_CHECK( m_thistSvc->getHist( id.c_str(), hist ) );
      hist->Fill( a_minDRPt( *track )/1e3  ); // GeV
    }

    if( truth ) { // if have it, just plot it
      id = base + "CreationVtxLG";
      ATH_CHECK( m_thistSvc->getHist( id.c_str(), hist ) );
      hist->Fill( truth->prodVtx()->perp() );
      id = base + "CreationVtx";
      ATH_CHECK( m_thistSvc->getHist( id.c_str(), hist ) );
      hist->Fill( truth->prodVtx()->perp() );
    }
  } // truth or reco information
  
  return StatusCode::SUCCESS;
}

StatusCode IdealTrackStudyAlg::fillRateHists( const xAOD::TruthParticleContainer *particles, 
    const std::string truColName) {
  for ( const auto ipart : *particles) {
    CHECK( fillRateHists( ipart, truColName, "" ) );

    // if not in a jet, do not continue
    if( a_minDR( *ipart ) < 0 ) { continue; }

    if ( a_minDRFlavor( *ipart ) == 5 ) {
      CHECK( fillRateHists( ipart, truColName, "BJet" ) );
      if( this->isFrom(ipart,5) ) { // includes B->D
        CHECK( fillRateHists( ipart, truColName, "BHadron") );
      }
    } else if ( a_minDRFlavor( *ipart ) == 4 ) {
      CHECK( fillRateHists( ipart, truColName, "CJet" ) );
      if( this->isFrom(ipart,4) ) {
        CHECK( fillRateHists( ipart, truColName, "CHadron") );
      }
    } else {
      CHECK( fillRateHists( ipart, truColName, "LightJet" ) );
    }
  }
  return StatusCode::SUCCESS;
}

StatusCode IdealTrackStudyAlg::fillRateHists( const xAOD::TruthParticle *truth, 
    const std::string truColName, const std::string type ) {

  std::string base = "Rate/" + truColName + "/" + type + "/h_Rate_" + truColName + type + "_";
  auto id = base;

  /*
  id = base + "pT";
  hist(id.c_str())->Fill( truth->pt() );
  id = base + "Eta";
  hist(id.c_str())->Fill( truth->eta() );
  id = base + "D0";
  hist(id.c_str())->Fill( a_d0(*truth) );
  id = base + "Z0";
  hist(id.c_str())->Fill( truth->prodVtx()->z() ); // correct?

  if(m_hasJets && a_minDR(*truth) > 0) { 
    id = base + "JetPt";
    hist(id.c_str())->Fill( a_minDRPt( *truth )/1e3 ); // GeV
    id = base + "JetDR";
    hist(id.c_str())->Fill( a_minDR( *truth ) );
  }

  id = base + "CreationVtxLG";
  hist(id.c_str())->Fill( truth->prodVtx()->perp() );
  id = base + "CreationVtx";
  hist(id.c_str())->Fill( truth->prodVtx()->perp() );
  */

  return StatusCode::SUCCESS;
}



//--------------------------------------------------------------------------//
//--------------------------------------------------------------------------//
// BOOK HISTOGRAMS 
//--------------------------------------------------------------------------//
//--------------------------------------------------------------------------//

StatusCode IdealTrackStudyAlg::bookZ0BiasHistograms() {

  std::set<std::string> z0trkCols = {"Reco", "Ideal", "RecoMatched"};   // not the best place for this
  for (const std::string col : z0trkCols) {
    auto id = "z0Bias/h_" + col + "_eta_z0";
//    hist = new TH2F(hname.c_str(), (col+"_eta_z0").c_str(), 50, -2.5, 2.5, 240, -120, 120);
//    hist->GetXaxis()->SetTitle("Track #eta"); 
//    hist->GetYaxis()->SetTitle("Track z_{0} [mm]");

    id = id + "_truth";
//    hist = new TH2F(hname.c_str(), (col+"_eta_z0_truth").c_str(), 50, -2.5, 2.5, 240, -120, 120);
//    hist->GetXaxis()->SetTitle("Truth #eta"); 
//    hist->GetYaxis()->SetTitle("Truth z_{0} [mm]");

  }

  return StatusCode::SUCCESS;
} // bookZ0BiasHistograms

StatusCode IdealTrackStudyAlg::bookEventHistograms() {
  std::set<std::string> m_trkCols = {"Reco", "Pseudo", "Ideal"};  // move to initialization

  /*
  // event level histograms - jets
  std::string id = "EventInfo/h_nJets";
  hist = new TH1F(hname.c_str(), "Number of Jets", 21, -0.5, 20.5 );
  hist->GetXaxis()->SetTitle("Number of Jets"); hist->GetYaxis()->SetTitle("Jets");

  id = "EventInfo/h_jet0Pt";
  hist = new TH1F(hname.c_str(), "Jet0 pT", 400, 0, 2000 );
  hist->GetXaxis()->SetTitle("Jet_{0} p_{T} [GeV]"); hist->GetYaxis()->SetTitle("Jets");

  id = "EventInfo/h_jet1Pt";
  hist = new TH1F(hname.c_str(), "Jet1 pT", 400, 0, 2000 );
  hist->GetXaxis()->SetTitle("Jet_{1} p_{T} [GeV]"); hist->GetYaxis()->SetTitle("Jets");

  id = "EventInfo/h_jetPt";
  hist = new TH1F(hname.c_str(), "Jet pT", 400, 0, 2000 );
  hist->GetXaxis()->SetTitle("Jet p_{T} [GeV]"); hist->GetYaxis()->SetTitle("Jets");

  //double pi = 2 * std::acos(0.0);
  id = "EventInfo/h_jetDPhi";
  hist = new TH1F(hname.c_str(), "Jet DeltaPhi", 64, -3.2, 3.2 );
  hist->GetXaxis()->SetTitle("#Delta#phi(jet_{0},jet_{1})"); hist->GetYaxis()->SetTitle("Jets");

  // event level histograms - tracks
  for (const std::string col : m_trkCols) {
    id = "EventInfo/h_n" + col + "Tracks";
    hist = new TH1F(hname.c_str(), ("Number of " + col + " Tracks").c_str(), 40, 0.0, 200.0 );
    hist->GetXaxis()->SetTitle(("Number of " + col +" Tracks").c_str()); hist->GetYaxis()->SetTitle("Arbitrary Units");
  }

  */
  return StatusCode::SUCCESS;
}

StatusCode IdealTrackStudyAlg::bookPullHistograms() {


  std::set<std::string> m_trkCols = {"Reco", "Pseudo", "Ideal"};  // move to initialization
  std::set<std::string> m_resdiualTypes = {"", "FromB", "FromC", "Secondary", "Fragmentation", "Rest"};  // move to initialization
  //std::set<std::string> m_resdiualTypes = {"", "FromHF", "Secondary", "Fragmentation", "Rest"};  // move to initialization

  for (const std::string col : m_trkCols) {

    for (const std::string type : m_resdiualTypes) {
      CHECK( bookResidualHists( col, type ) );
      CHECK( bookHitPatternHists( col, type ) );
      CHECK( bookTrkPResidualHists( col, type ) );
    }
  }

  CHECK( bookResidualHists(     "Reco", "Fake" ) ); // play nicely with configurations TODO
  CHECK( bookHitPatternHists(   "Reco", "Fake" ) ); // play nicely with configurations TODO
  CHECK( bookTrkPResidualHists( "Reco", "Fake" ) ); // play nicely with configurations TODO

  if(m_studyMatchedTracks) {
    CHECK( bookResidualHists(     "Pseudo", "Lost" ) );
    CHECK( bookHitPatternHists(   "Pseudo", "Lost" ) );
    CHECK( bookTrkPResidualHists( "Pseudo", "Lost" ) );

    CHECK( bookResidualHists(     "Ideal", "Lost" ) );
    CHECK( bookHitPatternHists(   "Ideal", "Lost" ) );
    CHECK( bookTrkPResidualHists( "Ideal", "Lost" ) );

    CHECK( bookResidualHists(     "Pseudo", "Fake" ) );
    CHECK( bookHitPatternHists(   "Pseudo", "Fake" ) );
    CHECK( bookTrkPResidualHists( "Pseudo", "Fake" ) );

    CHECK( bookResidualHists(     "Ideal", "Fake" ) );
    CHECK( bookHitPatternHists(   "Ideal", "Fake" ) );
    CHECK( bookTrkPResidualHists( "Ideal", "Fake" ) );

    CHECK( bookResidualHists(     "Reco", "NoMatch" ) );
    CHECK( bookHitPatternHists(   "Reco", "NoMatch" ) );
    CHECK( bookTrkPResidualHists( "Reco", "NoMatch" ) );

    CHECK( bookResidualHists(     "Reco", "NoMatchFake" ) );
    CHECK( bookHitPatternHists(   "Reco", "NoMatchFake" ) );
    CHECK( bookTrkPResidualHists( "Reco", "NoMatchFake" ) );
  }


  return StatusCode::SUCCESS;
}

StatusCode IdealTrackStudyAlg::bookResidualHists( const std::string trkColName, const std::string type ) {

  std::set<std::string> m_layers = {"0", "1", "2", "3" };
  // split cluster plots for size= 1-4, where 4 is inclusive and 5 is split hits
  std::set<std::string> m_size   = {"1", "2", "3", "4", "5" };
  std::set<std::string> m_npars  = {"1", "2", "3", "4" };

  /*
  for (const std::string lay : m_layers) {

    // no space between type and layer since type can be ""
    // move to helper function?
    auto base = "Residuals/" + trkColName + "/" + type + "/h_" + trkColName + type + "_Layer" + lay + "_";
    auto id = base;

    // unbiased
    id = base + "unbiasedResidualX";
    hist = new TH1F(hname.c_str(), (trkColName + "Track Unbiased Residual x (phi)").c_str(), 80, -0.4, 0.4 );
    hist->GetXaxis()->SetTitle("Unbiased Residual x (phi) [mm]"); hist->GetYaxis()->SetTitle("Arbitrary Units");

    id = base + "unbiasedResidualY";
    hist = new TH1F(hname.c_str(), (trkColName + "Track Unbiased Residual y (eta)").c_str(), 100, -1.0, 1.0 );
    hist->GetXaxis()->SetTitle("Unbiased Residual y (eta) [mm]"); hist->GetYaxis()->SetTitle("Arbitrary Units");

    id = base + "unbiasedPullX";
    hist = new TH1F(hname.c_str(), (trkColName + "Track Unbiased Pull x (phi)").c_str(), 100, -5.0, 5.0 );
    hist->GetXaxis()->SetTitle("Unbiased Pull x (phi)"); hist->GetYaxis()->SetTitle("Arbitrary Units");

    id = base + "unbiasedPullY";
    hist = new TH1F(hname.c_str(), (trkColName + "Track Unbiased Pull y (eta)").c_str(), 100, -5.0, 5.0 );
    hist->GetXaxis()->SetTitle("Unbiased Pull y (eta)"); hist->GetYaxis()->SetTitle("Arbitrary Units");


    // biased
    id = base + "biasedResidualX";
    hist = new TH1F(hname.c_str(), (trkColName + "Track Biased Residual x (phi)").c_str(), 80, -0.2, 0.2 );
    hist->GetXaxis()->SetTitle("Biased Residual x (phi) [mm]"); hist->GetYaxis()->SetTitle("Arbitrary Units");

    id = base + "biasedResidualY";
    hist = new TH1F(hname.c_str(), (trkColName + "Track Biased Residual y (eta)").c_str(), 100, -0.5, 0.5 );
    hist->GetXaxis()->SetTitle("Biased Residual y (eta) [mm]"); hist->GetYaxis()->SetTitle("Arbitrary Units");

    id = base + "biasedPullX";
    hist = new TH1F(hname.c_str(), (trkColName + "Track Biased Pull x (phi)").c_str(), 100, -5.0, 5.0 );
    hist->GetXaxis()->SetTitle("Biased Pull x (phi)"); hist->GetYaxis()->SetTitle("Arbitrary Units");

    id = base + "biasedPullY";
    hist = new TH1F(hname.c_str(), (trkColName + "Track Biased Pull y (eta)").c_str(), 100, -5.0, 5.0 );
    hist->GetXaxis()->SetTitle("Biased Pull y (eta)"); hist->GetYaxis()->SetTitle("Arbitrary Units");


    // cluster
    id = base + "clusterResidualX";
    hist = new TH1F(hname.c_str(), (trkColName + "Cluster Truth Residual x (phi)").c_str(), 80, -0.1, 0.1 );
    hist->GetXaxis()->SetTitle("Cluster Truth Residual x (phi) [mm]"); hist->GetYaxis()->SetTitle("Arbitrary Units");

    id = base + "clusterResidualY";
    hist = new TH1F(hname.c_str(), (trkColName + "Cluster Truth Residual y (eta)").c_str(), 120, -0.6, 0.6 );
    hist->GetXaxis()->SetTitle("Cluster Truth Residual y (eta) [mm]"); hist->GetYaxis()->SetTitle("Arbitrary Units");

    id = base + "clusterPullX";
    hist = new TH1F(hname.c_str(), (trkColName + "Cluster Truth Pull x (phi)").c_str(), 100, -5.0, 5.0 );
    hist->GetXaxis()->SetTitle("Cluster Truth Pull x (phi)"); hist->GetYaxis()->SetTitle("Arbitrary Units");

    id = base + "clusterPullY";
    hist = new TH1F(hname.c_str(), (trkColName + "Cluster Truth Pull y (eta)").c_str(), 100, -5.0, 5.0 );
    hist->GetXaxis()->SetTitle("Cluster Truth Pull y (eta)"); hist->GetYaxis()->SetTitle("Arbitrary Units");

    id = base + "clusterErrorX";
    hist = new TH1F(hname.c_str(), (trkColName + "Cluster Error x (phi)").c_str(), 100, -1.0, 1.0 );
    hist->GetXaxis()->SetTitle("Cluster Error x (phi)"); hist->GetYaxis()->SetTitle("Arbitrary Units");

    id = base + "clusterErrorY";
    hist = new TH1F(hname.c_str(), (trkColName + "Cluster Error y (eta)").c_str(), 100, -1.0, 1.0 );
    hist->GetXaxis()->SetTitle("Cluster Error y (eta)"); hist->GetYaxis()->SetTitle("Arbitrary Units");

    // track
    id = base + "trackBiasedResidualX";
    hist = new TH1F(hname.c_str(), (trkColName + "Track Truth Biased Residual x (phi)").c_str(), 80, -0.2, 0.2 );
    hist->GetXaxis()->SetTitle("Track Truth Biased Residual x (phi) [mm]"); hist->GetYaxis()->SetTitle("Arbitrary Units");

    id = base + "trackBiasedResidualY";
    hist = new TH1F(hname.c_str(), (trkColName + "Track Truth Biased Residual y (eta)").c_str(), 100, -0.5, 0.5 );
    hist->GetXaxis()->SetTitle("Track Truth Biased Residual y (eta) [mm]"); hist->GetYaxis()->SetTitle("Arbitrary Units");

    id = base + "trackUnbiasedResidualX";
    hist = new TH1F(hname.c_str(), (trkColName + "Track Truth Unbiased Residual x (phi)").c_str(), 80, -0.2, 0.2 );
    hist->GetXaxis()->SetTitle("Track Truth Unbiased Residual x (phi) [mm]"); hist->GetYaxis()->SetTitle("Arbitrary Units");

    id = base + "trackUnbiasedResidualY";
    hist = new TH1F(hname.c_str(), (trkColName + "Track Truth Unbiased Residual y (eta)").c_str(), 100, -0.5, 0.5 );
    hist->GetXaxis()->SetTitle("Track Truth Unbiased Residual y (eta) [mm]"); hist->GetYaxis()->SetTitle("Arbitrary Units");

    id = base + "trackBiasedPullX";
    hist = new TH1F(hname.c_str(), (trkColName + "Track Truth Biased Pull x (phi)").c_str(), 100, -5.0, 5.0 );
    hist->GetXaxis()->SetTitle("Track Truth Biased Pull x (phi)"); hist->GetYaxis()->SetTitle("Arbitrary Units");

    id = base + "trackBiasedPullY";
    hist = new TH1F(hname.c_str(), (trkColName + "Track Truth Biased Pull y (eta)").c_str(), 100, -5.0, 5.0 );
    hist->GetXaxis()->SetTitle("Track Truth Biased Pull y (eta)"); hist->GetYaxis()->SetTitle("Arbitrary Units");

    id = base + "trackUnbiasedPullX";
    hist = new TH1F(hname.c_str(), (trkColName + "Track Truth Unbiased Pull x (phi)").c_str(), 100, -5.0, 5.0 );
    hist->GetXaxis()->SetTitle("Track Truth Unbiased Pull x (phi)"); hist->GetYaxis()->SetTitle("Arbitrary Units");

    id = base + "trackUnbiasedPullY";
    hist = new TH1F(hname.c_str(), (trkColName + "Track Truth Unbiased Pull y (eta)").c_str(), 100, -5.0, 5.0 );
    hist->GetXaxis()->SetTitle("Track Truth Unbiased Pull y (eta)"); hist->GetYaxis()->SetTitle("Arbitrary Units");


    for (const std::string size : m_size) {
      // unbiased
      id = base + "Size" + size + "_unbiasedResidualX"; // need to add _ to last str...ugly!
      hist = new TH1F(hname.c_str(), (trkColName + "Track Unbiased Residual x (phi) Size " + size).c_str(), 80, -0.4, 0.4 );
      hist->GetXaxis()->SetTitle("Unbiased Residual x (phi) [mm]"); hist->GetYaxis()->SetTitle("Arbitrary Units");

      id = base + "Size" + size + "_unbiasedResidualY";
      hist = new TH1F(hname.c_str(), (trkColName + "Track Unbiased Residual y (eta) Size " + size).c_str(), 100, -1.0, 1.0 );
      hist->GetXaxis()->SetTitle("Unbiased Residual y (eta) [mm]"); hist->GetYaxis()->SetTitle("Arbitrary Units");

      id = base + "Size" + size + "_unbiasedPullX";
      hist = new TH1F(hname.c_str(), (trkColName + "Track Unbiased Pull x (phi) Size " + size).c_str(), 100, -5.0, 5.0 );
      hist->GetXaxis()->SetTitle("Unbiased Pull x (phi)"); hist->GetYaxis()->SetTitle("Arbitrary Units");

      id = base + "Size" + size + "_unbiasedPullY";
      hist = new TH1F(hname.c_str(), (trkColName + "Track Unbiased Pull y (eta) Size " + size).c_str(), 100, -5.0, 5.0 );
      hist->GetXaxis()->SetTitle("Unbiased Pull y (eta)"); hist->GetYaxis()->SetTitle("Arbitrary Units");


      // biased
      id = base + "Size" + size + "_biasedResidualX";
      hist = new TH1F(hname.c_str(), (trkColName + "Track Biased Residual x (phi) Size " + size).c_str(), 80, -0.4, 0.4 );
      hist->GetXaxis()->SetTitle("Biased Residual x (phi) [mm]"); hist->GetYaxis()->SetTitle("Arbitrary Units");

      id = base + "Size" + size + "_biasedResidualY";
      hist = new TH1F(hname.c_str(), (trkColName + "Track Biased Residual y (eta) Size " + size).c_str(), 100, -1.0, 1.0 );
      hist->GetXaxis()->SetTitle("Biased Residual y (eta) [mm]"); hist->GetYaxis()->SetTitle("Arbitrary Units");

      id = base + "Size" + size + "_biasedPullX";
      hist = new TH1F(hname.c_str(), (trkColName + "Track Biased Pull x (phi) Size " + size).c_str(), 100, -5.0, 5.0 );
      hist->GetXaxis()->SetTitle("Biased Pull x (phi)"); hist->GetYaxis()->SetTitle("Arbitrary Units");

      id = base + "Size" + size + "_biasedPullY";
      hist = new TH1F(hname.c_str(), (trkColName + "Track Biased Pull y (eta) Size " + size).c_str(), 100, -5.0, 5.0 );
      hist->GetXaxis()->SetTitle("Biased Pull y (eta)"); hist->GetYaxis()->SetTitle("Arbitrary Units");


      // cluster 
      id = base + "Size" + size + "_clusterResidualX";
      hist = new TH1F(hname.c_str(), (trkColName + "Cluster Truth Residual x (phi) Size " + size).c_str(), 80, -0.1, 0.1 );
      hist->GetXaxis()->SetTitle("Cluster Truth Residual x (phi) [mm]"); hist->GetYaxis()->SetTitle("Arbitrary Units");

      id = base + "Size" + size + "_clusterResidualY";
      hist = new TH1F(hname.c_str(), (trkColName + "Cluster Truth Residual y (eta) Size " + size).c_str(), 120, -0.6, 0.6 );
      hist->GetXaxis()->SetTitle("Cluster Truth Residual y (eta) [mm]"); hist->GetYaxis()->SetTitle("Arbitrary Units");

      id = base + "Size" + size + "_clusterPullX";
      hist = new TH1F(hname.c_str(), (trkColName + "Cluster Truth Pull x (phi) Size " + size).c_str(), 100, -5.0, 5.0 );
      hist->GetXaxis()->SetTitle("Cluster Truth Pull x (phi)"); hist->GetYaxis()->SetTitle("Arbitrary Units");

      id = base + "Size" + size + "_clusterPullY";
      hist = new TH1F(hname.c_str(), (trkColName + "Cluster Truth Pull y (eta) Size " + size).c_str(), 100, -5.0, 5.0 );
      hist->GetXaxis()->SetTitle("Cluster Truth Pull y (eta)"); hist->GetYaxis()->SetTitle("Arbitrary Units");

      // track biased
      id = base + "Size" + size + "_trackBiasedResidualX";
      hist = new TH1F(hname.c_str(), (trkColName + "Track Truth Biased Residual x (phi) Size " + size).c_str(), 80, -0.2, 0.2 );
      hist->GetXaxis()->SetTitle("Track Truth Biased Residual x (phi) [mm]"); hist->GetYaxis()->SetTitle("Arbitrary Units");

      id = base + "Size" + size + "_trackBiasedResidualY";
      hist = new TH1F(hname.c_str(), (trkColName + "Track Truth Biased Residual y (eta) Size " + size).c_str(), 100, -0.5, 0.5 );
      hist->GetXaxis()->SetTitle("Track Truth Biased Residual y (eta) [mm]"); hist->GetYaxis()->SetTitle("Arbitrary Units");

      id = base + "Size" + size + "_trackBiasedPullX";
      hist = new TH1F(hname.c_str(), (trkColName + "Track Truth Biased Pull x (phi) Size " + size).c_str(), 100, -5.0, 5.0 );
      hist->GetXaxis()->SetTitle("Track Truth Biased Pull x (phi)"); hist->GetYaxis()->SetTitle("Arbitrary Units");

      id = base + "Size" + size + "_trackBiasedPullY";
      hist = new TH1F(hname.c_str(), (trkColName + "Track Truth Biased Pull y (eta) Size " + size).c_str(), 100, -5.0, 5.0 );
      hist->GetXaxis()->SetTitle("Track Truth Biased Pull y (eta)"); hist->GetYaxis()->SetTitle("Arbitrary Units");

      // track unbiased
      id = base + "Size" + size + "_trackUnbiasedResidualX";
      hist = new TH1F(hname.c_str(), (trkColName + "Track Truth Unbiased Residual x (phi) Size " + size).c_str(), 80, -0.2, 0.2 );
      hist->GetXaxis()->SetTitle("Track Truth Unbiased Residual x (phi) [mm]"); hist->GetYaxis()->SetTitle("Arbitrary Units");

      id = base + "Size" + size + "_trackUnbiasedResidualY";
      hist = new TH1F(hname.c_str(), (trkColName + "Track Truth Unbiased Residual y (eta) Size " + size).c_str(), 100, -0.5, 0.5 );
      hist->GetXaxis()->SetTitle("Track Truth Unbiased Residual y (eta) [mm]"); hist->GetYaxis()->SetTitle("Arbitrary Units");

      id = base + "Size" + size + "_trackUnbiasedPullX";
      hist = new TH1F(hname.c_str(), (trkColName + "Track Truth Unbiased Pull x (phi) Size " + size).c_str(), 100, -5.0, 5.0 );
      hist->GetXaxis()->SetTitle("Track Truth Unbiased Pull x (phi)"); hist->GetYaxis()->SetTitle("Arbitrary Units");

      id = base + "Size" + size + "_trackUnbiasedPullY";
      hist = new TH1F(hname.c_str(), (trkColName + "Track Truth Unbiased Pull y (eta) Size " + size).c_str(), 100, -5.0, 5.0 );
      hist->GetXaxis()->SetTitle("Track Truth Unbiased Pull y (eta)"); hist->GetYaxis()->SetTitle("Arbitrary Units");

    } // loop over size

    for (const std::string npar : m_npars) {
      // unbiased
      id = base + "Part" + npar + "_unbiasedResidualX"; // need to add _ to last str...ugly!
      hist = new TH1F(hname.c_str(), (trkColName + "Track Unbiased Residual x (phi) nParticle " + npar).c_str(), 80, -0.4, 0.4 );
      hist->GetXaxis()->SetTitle("Unbiased Residual x (phi) [mm]"); hist->GetYaxis()->SetTitle("Arbitrary Units");

      id = base + "Part" + npar + "_unbiasedResidualY";
      hist = new TH1F(hname.c_str(), (trkColName + "Track Unbiased Residual y (eta) nParticle " + npar).c_str(), 100, -1.0, 1.0 );
      hist->GetXaxis()->SetTitle("Unbiased Residual y (eta) [mm]"); hist->GetYaxis()->SetTitle("Arbitrary Units");

      id = base + "Part" + npar + "_unbiasedPullX";
      hist = new TH1F(hname.c_str(), (trkColName + "Track Unbiased Pull x (phi) nParticle " + npar).c_str(), 100, -5.0, 5.0 );
      hist->GetXaxis()->SetTitle("Unbiased Pull x (phi)"); hist->GetYaxis()->SetTitle("Arbitrary Units");

      id = base + "Part" + npar + "_unbiasedPullY";
      hist = new TH1F(hname.c_str(), (trkColName + "Track Unbiased Pull y (eta) nParticle " + npar).c_str(), 100, -5.0, 5.0 );
      hist->GetXaxis()->SetTitle("Unbiased Pull y (eta)"); hist->GetYaxis()->SetTitle("Arbitrary Units");


      // biased
      id = base + "Part" + npar + "_biasedResidualX";
      hist = new TH1F(hname.c_str(), (trkColName + "Track Biased Residual x (phi) nParticle " + npar).c_str(), 80, -0.4, 0.4 );
      hist->GetXaxis()->SetTitle("Biased Residual x (phi) [mm]"); hist->GetYaxis()->SetTitle("Arbitrary Units");

      id = base + "Part" + npar + "_biasedResidualY";
      hist = new TH1F(hname.c_str(), (trkColName + "Track Biased Residual y (eta) nParticle " + npar).c_str(), 100, -1.0, 1.0 );
      hist->GetXaxis()->SetTitle("Biased Residual y (eta) [mm]"); hist->GetYaxis()->SetTitle("Arbitrary Units");

      id = base + "Part" + npar + "_biasedPullX";
      hist = new TH1F(hname.c_str(), (trkColName + "Track Biased Pull x (phi) nParticle " + npar).c_str(), 100, -5.0, 5.0 );
      hist->GetXaxis()->SetTitle("Biased Pull x (phi)"); hist->GetYaxis()->SetTitle("Arbitrary Units");

      id = base + "Part" + npar + "_biasedPullY";
      hist = new TH1F(hname.c_str(), (trkColName + "Track Biased Pull y (eta) nParticle " + npar).c_str(), 100, -5.0, 5.0 );
      hist->GetXaxis()->SetTitle("Biased Pull y (eta)"); hist->GetYaxis()->SetTitle("Arbitrary Units");


      // cluster 
      id = base + "Part" + npar + "_clusterResidualX";
      hist = new TH1F(hname.c_str(), (trkColName + "Cluster Truth Residual x (phi) nParticle " + npar).c_str(), 80, -0.1, 0.1 );
      hist->GetXaxis()->SetTitle("Cluster Truth Residual x (phi) [mm]"); hist->GetYaxis()->SetTitle("Arbitrary Units");

      id = base + "Part" + npar + "_clusterResidualY";
      hist = new TH1F(hname.c_str(), (trkColName + "Cluster Truth Residual y (eta) nParticle " + npar).c_str(), 120, -0.6, 0.6 );
      hist->GetXaxis()->SetTitle("Cluster Truth Residual y (eta) [mm]"); hist->GetYaxis()->SetTitle("Arbitrary Units");

      id = base + "Part" + npar + "_clusterPullX";
      hist = new TH1F(hname.c_str(), (trkColName + "Cluster Truth Pull x (phi) nParticle " + npar).c_str(), 100, -5.0, 5.0 );
      hist->GetXaxis()->SetTitle("Cluster Truth Pull x (phi)"); hist->GetYaxis()->SetTitle("Arbitrary Units");

      id = base + "Part" + npar + "_clusterPullY";
      hist = new TH1F(hname.c_str(), (trkColName + "Cluster Truth Pull y (eta) nParticle " + npar).c_str(), 100, -5.0, 5.0 );
      hist->GetXaxis()->SetTitle("Cluster Truth Pull y (eta)"); hist->GetYaxis()->SetTitle("Arbitrary Units");

      // track biased
      id = base + "Part" + npar + "_trackBiasedResidualX";
      hist = new TH1F(hname.c_str(), (trkColName + "Track Truth Biased Residual x (phi) nParticle " + npar).c_str(), 80, -0.2, 0.2 );
      hist->GetXaxis()->SetTitle("Track Truth Biased Residual x (phi) [mm]"); hist->GetYaxis()->SetTitle("Arbitrary Units");

      id = base + "Part" + npar + "_trackBiasedResidualY";
      hist = new TH1F(hname.c_str(), (trkColName + "Track Truth Biased Residual y (eta) nParticle " + npar).c_str(), 100, -0.5, 0.5 );
      hist->GetXaxis()->SetTitle("Track Truth Biased Residual y (eta) [mm]"); hist->GetYaxis()->SetTitle("Arbitrary Units");

      id = base + "Part" + npar + "_trackBiasedPullX";
      hist = new TH1F(hname.c_str(), (trkColName + "Track Truth Biased Pull x (phi) nParticle " + npar).c_str(), 100, -5.0, 5.0 );
      hist->GetXaxis()->SetTitle("Track Truth Biased Pull x (phi)"); hist->GetYaxis()->SetTitle("Arbitrary Units");

      id = base + "Part" + npar + "_trackBiasedPullY";
      hist = new TH1F(hname.c_str(), (trkColName + "Track Truth Biased Pull y (eta) nParticle " + npar).c_str(), 100, -5.0, 5.0 );
      hist->GetXaxis()->SetTitle("Track Truth Biased Pull y (eta)"); hist->GetYaxis()->SetTitle("Arbitrary Units");

      // track unbiased
      id = base + "Part" + npar + "_trackUnbiasedResidualX";
      hist = new TH1F(hname.c_str(), (trkColName + "Track Truth Unbiased Residual x (phi) nParticle " + npar).c_str(), 80, -0.2, 0.2 );
      hist->GetXaxis()->SetTitle("Track Truth Unbiased Residual x (phi) [mm]"); hist->GetYaxis()->SetTitle("Arbitrary Units");

      id = base + "Part" + npar + "_trackUnbiasedResidualY";
      hist = new TH1F(hname.c_str(), (trkColName + "Track Truth Unbiased Residual y (eta) nParticle " + npar).c_str(), 100, -0.5, 0.5 );
      hist->GetXaxis()->SetTitle("Track Truth Unbiased Residual y (eta) [mm]"); hist->GetYaxis()->SetTitle("Arbitrary Units");

      id = base + "Part" + npar + "_trackUnbiasedPullX";
      hist = new TH1F(hname.c_str(), (trkColName + "Track Truth Unbiased Pull x (phi) nParticle " + npar).c_str(), 100, -5.0, 5.0 );
      hist->GetXaxis()->SetTitle("Track Truth Unbiased Pull x (phi)"); hist->GetYaxis()->SetTitle("Arbitrary Units");

      id = base + "Part" + npar + "_trackUnbiasedPullY";
      hist = new TH1F(hname.c_str(), (trkColName + "Track Truth Unbiased Pull y (eta) nParticle " + npar).c_str(), 100, -5.0, 5.0 );
      hist->GetXaxis()->SetTitle("Track Truth Unbiased Pull y (eta)"); hist->GetYaxis()->SetTitle("Arbitrary Units");

    } // loop over n particles


  } // loop over layers
*/

  return StatusCode::SUCCESS;
}

StatusCode IdealTrackStudyAlg::bookHitPatternHists( const std::string trkColName, const std::string type ) {


  // move to helper function?
  std::string base = "/"+m_streamName+"/HitPattern/" + trkColName + "/" + type + "/";
  std::string name = "h_" + trkColName + type + "_";
  auto id = base + name;
  auto hname = name;

  TH1F* hist = nullptr;

  // TrackInfo
  hname = name + "patternRecoInfo";
  id = base + hname;
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " NumberOfTrackRecoInfo").c_str(), 52, -0.5, 51.5 );
  hist->GetXaxis()->SetTitle("Track Pattern Reco Info"); hist->GetYaxis()->SetTitle("Arbitrary Units");
  ATH_CHECK( m_thistSvc->regHist( id.c_str(), hist ) );

  // Pixel
  hname = name + "numberOfPixelHits";
  id = base + hname;
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " numberOfPixelHits").c_str(), 9, -0.5, 8.5 );
  hist->GetXaxis()->SetTitle("N Pix Hits"); hist->GetYaxis()->SetTitle("Arbitrary Units");
  ATH_CHECK( m_thistSvc->regHist( id.c_str(), hist ) );

  hname = name + "numberOfPixelHoles";
  id = base + hname;
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " numberOfPixelHoles").c_str(), 6, -0.5, 5.5 );
  hist->GetXaxis()->SetTitle("N Pix Holes"); hist->GetYaxis()->SetTitle("Arbitrary Units");
  ATH_CHECK( m_thistSvc->regHist( id.c_str(), hist ) );


  hname = name + "numberOfPixelSharedHits";
  id = base + hname;
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " numberOfPixelSharedHits").c_str(), 4, -0.5, 3.5 );
  hist->GetXaxis()->SetTitle("N Pix Shared Hits"); hist->GetYaxis()->SetTitle("Arbitrary Units");
  ATH_CHECK( m_thistSvc->regHist( id.c_str(), hist ) );

  hname = name + "numberOfPixelSplitHits";
  id = base + hname;
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " numberOfPixelSplitHits").c_str(), 4, -0.5, 3.5 );
  hist->GetXaxis()->SetTitle("N Pix Split Hits"); hist->GetYaxis()->SetTitle("Arbitrary Units");
  ATH_CHECK( m_thistSvc->regHist( id.c_str(), hist ) );

  hname = name + "numberOfPixelWrongSplitHits";
  id = base + hname;
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " numberOfPixelWrongSplitHits").c_str(), 4, -0.5, 3.5 );
  hist->GetXaxis()->SetTitle("N Pix Wrong-Split Hits"); hist->GetYaxis()->SetTitle("Arbitrary Units");
  ATH_CHECK( m_thistSvc->regHist( id.c_str(), hist ) );

  hname = name + "numberOfPixelWrongSplitHitsTight";
  id = base + hname;
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " numberOfPixelWrongSplitHitsTight").c_str(), 4, -0.5, 3.5 );
  hist->GetXaxis()->SetTitle("N Pix Wrong-Split Hits Tight"); hist->GetYaxis()->SetTitle("Arbitrary Units");
  ATH_CHECK( m_thistSvc->regHist( id.c_str(), hist ) );

  hname = name + "numberOfPixelUnSplitHits";
  id = base + hname;
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " numberOfPixelUnSplitHits").c_str(), 4, -0.5, 3.5 );
  hist->GetXaxis()->SetTitle("N Pix Un-Split Hits"); hist->GetYaxis()->SetTitle("Arbitrary Units");
  ATH_CHECK( m_thistSvc->regHist( id.c_str(), hist ) );

  hname = name + "numberOfPixelUnSplitHitsTight";
  id = base + hname;
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " numberOfPixelUnSplitHitsTight").c_str(), 4, -0.5, 3.5 );
  hist->GetXaxis()->SetTitle("N Pix Un-Split Hits Tight"); hist->GetYaxis()->SetTitle("Arbitrary Units");
  ATH_CHECK( m_thistSvc->regHist( id.c_str(), hist ) );

  hname = name + "numberOfPixelSplitShareHits";
  id = base + hname;
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " numberOfPixelSplitShareHits").c_str(), 4, -0.5, 3.5 );
  hist->GetXaxis()->SetTitle("N Pix Split-Share Hits"); hist->GetYaxis()->SetTitle("Arbitrary Units");
  ATH_CHECK( m_thistSvc->regHist( id.c_str(), hist ) );

  hname = name + "numberOfPixelWrongShareHits";
  id = base + hname;
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " numberOfPixelWrongShareHits").c_str(), 4, -0.5, 3.5 );
  hist->GetXaxis()->SetTitle("N Pix Wrong-Share Hits"); hist->GetYaxis()->SetTitle("Arbitrary Units");
  ATH_CHECK( m_thistSvc->regHist( id.c_str(), hist ) );

  hname = name + "numberOfPixelUnderShareHits";
  id = base + hname;
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " numberOfPixelUnderShareHits").c_str(), 4, -0.5, 3.5 );
  hist->GetXaxis()->SetTitle("N Pix Under-Share Hits"); hist->GetYaxis()->SetTitle("Arbitrary Units");
  ATH_CHECK( m_thistSvc->regHist( id.c_str(), hist ) );

  hname = name + "numberOfPixelUnShareHits";
  id = base + hname;
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " numberOfPixelUnShareHits").c_str(), 4, -0.5, 3.5 );
  hist->GetXaxis()->SetTitle("N Pix Un-Share Hits"); hist->GetYaxis()->SetTitle("Arbitrary Units");
  ATH_CHECK( m_thistSvc->regHist( id.c_str(), hist ) );

  // IBL
  hname = name + "numberOfInnermostPixelLayerHits";
  id = base + hname;
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " numberOfInnermostPixelLayerHits").c_str(), 4, -0.5, 3.5 );
  hist->GetXaxis()->SetTitle("N Innermost Pix Hits"); hist->GetYaxis()->SetTitle("Arbitrary Units");
  ATH_CHECK( m_thistSvc->regHist( id.c_str(), hist ) );

  hname = name + "numberOfInnermostPixelLayerSharedHits";
  id = base + hname;
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " numberOfInnermostPixelLayerSharedHits").c_str(), 4, -0.5, 3.5 );
  hist->GetXaxis()->SetTitle("N Innermost Pix Shared Hits"); hist->GetYaxis()->SetTitle("Arbitrary Units");
  ATH_CHECK( m_thistSvc->regHist( id.c_str(), hist ) );

  hname = name + "numberOfInnermostPixelLayerSplitHits";
  id = base + hname;
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " numberOfInnermostPixelLayerSplitHits").c_str(), 4, -0.5, 3.5 );
  hist->GetXaxis()->SetTitle("N Innermost Pix Split Hits"); hist->GetYaxis()->SetTitle("Arbitrary Units");
  ATH_CHECK( m_thistSvc->regHist( id.c_str(), hist ) );

  hname = name + "expectInnermostPixelLayerHit";
  id = base + hname;
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " expectInnermostPixelLayerHit").c_str(), 2, -0.5, 1.5 );
  hist->GetXaxis()->SetTitle("Expect Innermost Pix Hits"); hist->GetYaxis()->SetTitle("Arbitrary Units");
  ATH_CHECK( m_thistSvc->regHist( id.c_str(), hist ) );



  // SCT
  hname = name + "numberOfSCTHits";
  id = base + hname;
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " numberOfSCTHits").c_str(), 15, -0.5, 14.5 );
  hist->GetXaxis()->SetTitle("N SCT Hits"); hist->GetYaxis()->SetTitle("Arbitrary Units");
  ATH_CHECK( m_thistSvc->regHist( id.c_str(), hist ) );

  hname = name + "numberOfSCTHoles";
  id = base + hname;
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " numberOfSCTHoles").c_str(), 7, -0.5, 6.5 );
  hist->GetXaxis()->SetTitle("N SCT Holes"); hist->GetYaxis()->SetTitle("Arbitrary Units");
  ATH_CHECK( m_thistSvc->regHist( id.c_str(), hist ) );

  hname = name + "numberOfSCTSharedHits";
  id = base + hname;
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " numberOfSCTSharedHits").c_str(), 6, -0.5, 5.5 );
  hist->GetXaxis()->SetTitle("N SCT Shared Hits"); hist->GetYaxis()->SetTitle("Arbitrary Units");
  ATH_CHECK( m_thistSvc->regHist( id.c_str(), hist ) );

  hname = name + "numberOfSCTUnderShareHits";
  id = base + hname;
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " numberOfSCTUnderShareHits").c_str(), 6, -0.5, 5.5 );
  hist->GetXaxis()->SetTitle("N SCT Under-Share Hits"); hist->GetYaxis()->SetTitle("Arbitrary Units");
  ATH_CHECK( m_thistSvc->regHist( id.c_str(), hist ) );

  hname = name + "numberOfSCTUnShareHits";
  id = base + hname;
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " numberOfSCTUnShareHits").c_str(), 6, -0.5, 5.5 );
  hist->GetXaxis()->SetTitle("N SCT Un-Share Hits"); hist->GetYaxis()->SetTitle("Arbitrary Units");
  ATH_CHECK( m_thistSvc->regHist( id.c_str(), hist ) );

  hname = name + "numberOfSCTWrongShareHits";
  id = base + hname;
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " numberOfSCTWrongShareHits").c_str(), 6, -0.5, 5.5 );
  hist->GetXaxis()->SetTitle("N SCT Wrong-Share Hits"); hist->GetYaxis()->SetTitle("Arbitrary Units");
  ATH_CHECK( m_thistSvc->regHist( id.c_str(), hist ) );

  hname = name + "nPartWrongShare";
  id = base + hname;
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " nPartWrongShare").c_str(), 6, -0.5, 5.5 );
  hist->GetXaxis()->SetTitle("N Particles (Tight) in Wrong-Share SCT Hits"); hist->GetYaxis()->SetTitle("Arbitrary Units");
  ATH_CHECK( m_thistSvc->regHist( id.c_str(), hist ) );

  hname = name + "nPartDiffWrongShare";
  id = base + hname;
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " nPartDiffWrongShare").c_str(), 6, -0.5, 5.5 );
  hist->GetXaxis()->SetTitle("N Particles (Tight) vs nUsed in Wrong-Share SCT Hits"); hist->GetYaxis()->SetTitle("Arbitrary Units");
  ATH_CHECK( m_thistSvc->regHist( id.c_str(), hist ) );

  // thing cut on
  // number of Si Hits ( >= 7)
  hname = name + "numberOfSiHits";
  id = base + hname;
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " numberOfSiHits").c_str(), 20, -0.5, 19.5 );
  hist->GetXaxis()->SetTitle("N Si Hits"); hist->GetYaxis()->SetTitle("Arbitrary Units");
  ATH_CHECK( m_thistSvc->regHist( id.c_str(), hist ) );

  // number of Shared Modules ( <= 1)
  hname = name + "numberOfSharedModules";
  id = base + hname;
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " numberOfSharedModules").c_str(), 20, -0.25, 9.75 );
  hist->GetXaxis()->SetTitle("N Shared Modules"); hist->GetYaxis()->SetTitle("Arbitrary Units");
  ATH_CHECK( m_thistSvc->regHist( id.c_str(), hist ) );

  // number of Si Holes ( <= 2)
  hname = name + "numberOfSiHoles";
  id = base + hname;
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " numberOfSiHoles").c_str(), 5, -0.5, 4.5 );
  hist->GetXaxis()->SetTitle("N Si Holes"); hist->GetYaxis()->SetTitle("Arbitrary Units");
  ATH_CHECK( m_thistSvc->regHist( id.c_str(), hist ) );

  // number of Pixel Holes ( <= 1)
  // already implemented above

  // truth
  hname = name + "truthMatchProbability";
  id = base + hname;
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " truthMatchProbability").c_str(), 20, 0.0, 1.0 );
  hist->GetXaxis()->SetTitle("Truth Match Probability"); hist->GetYaxis()->SetTitle("Arbitrary Units");
  ATH_CHECK( m_thistSvc->regHist( id.c_str(), hist ) );

  // SCT Width
  hname = name + "widthDiffNP1NU1";
  id = base + hname;
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " widthDiffNP1NU1").c_str(), 60,  -200.0, 100.0 );
  hist->GetXaxis()->SetTitle("Width Difference [um]"); hist->GetYaxis()->SetTitle("Arbitrary Units");
  ATH_CHECK( m_thistSvc->regHist( id.c_str(), hist ) );

  hname = name + "widthDiffNP1NU2";
  id = base + hname;
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " widthDiffNP1NU2").c_str(), 60,  -200.0, 100.0 );
  hist->GetXaxis()->SetTitle("Width Difference [um]"); hist->GetYaxis()->SetTitle("Arbitrary Units");
  ATH_CHECK( m_thistSvc->regHist( id.c_str(), hist ) );

  hname = name + "widthDiffNP2NU2";
  id = base + hname;
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " widthDiffNP2NU2").c_str(), 60,  -200.0, 100.0 );
  hist->GetXaxis()->SetTitle("Width Difference [um]"); hist->GetYaxis()->SetTitle("Arbitrary Units");
  ATH_CHECK( m_thistSvc->regHist( id.c_str(), hist ) );

  hname = name + "widthDiffNP2NU1";
  id = base + hname;
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " widthDiffNP2NU1").c_str(), 60,  -200.0, 100.0 );
  hist->GetXaxis()->SetTitle("Width Difference [um]"); hist->GetYaxis()->SetTitle("Arbitrary Units");
  ATH_CHECK( m_thistSvc->regHist( id.c_str(), hist ) );

  return StatusCode::SUCCESS;
}

StatusCode IdealTrackStudyAlg::bookTrkPResidualHists( const std::string trkColName, const std::string type ) {

  // move to helper function?
  auto base = "TrkPResiduals/" + trkColName + "/" + type + "/h_" + trkColName + type + "_";
  auto id = base;

  // match Supriya 
  // https://indico.cern.ch/event/907600/contributions/3870765/attachments/2046160/3428194/CTIDE_talk4_27May.pdf
  int nBinsEta = 22;
  double binsEta[23] = {-2.5, -2.0, -1.5, -1.25, -1.0, -0.80, -0.60, -0.40, -0.30, -0.20, -0.10, 0., 0.10, 0.20, 0.30, 0.40, 0.60, 0.80, 1.0, 1.25, 1.5, 2.0, 2.5};

  int nBinsPt = 39;
  double binsPt[40] = { 0.25, 0.5, 0.75, 1., 1.25, 1.5, 2., 2.5, 3., 3.5, 4., 5., 6., 7., 8., 9., 10., 12., 14., 16., 18., 20, 24., 28., 32., 36., 40, 45., 50., 55., 60., 70., 80., 100., 150, 250, 400, 600, 800, 1000 }; // in GeV
  for ( int i=0; i<40; i++ ) { 
    binsPt[i] = binsPt[i]*1e3; 
  } // convert to MeV

  double pi = 2 * std::acos(0.0);

  /*

  // D0
  id = base + "D0";
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " D0").c_str(), 400, -100.0, 100.);
  hist->GetXaxis()->SetTitle("Track d_{0} [mm]");
  hist->GetYaxis()->SetTitle("Arbitrary Units"); 

  id = base + "D0Truth";
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " D0Truth").c_str(), 400, -100.0, 100.);
  hist->GetXaxis()->SetTitle("Truth d_{0} [mm]");
  hist->GetYaxis()->SetTitle("Arbitrary Units"); 

  id = base + "D0Residual";
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " D0Residual").c_str(), 200, -3.0, 3.0);
  hist->GetXaxis()->SetTitle("Track d_{0} Residual [mm]");
  hist->GetYaxis()->SetTitle("Arbitrary Units"); 

  id = base + "D0Pull";
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " D0Pull").c_str(), 100, -5, 5);
  hist->GetXaxis()->SetTitle("Track d_{0} Pull");
  hist->GetYaxis()->SetTitle("Arbitrary Units"); 
                                       
  // Z0
  id = base + "Z0";
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " Z0").c_str(), 440, -120.0, 120.);
  hist->GetXaxis()->SetTitle("Track z_{0} [mm]");
  hist->GetYaxis()->SetTitle("Arbitrary Units"); 

  id = base + "Z0Truth";
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " Z0Truth").c_str(), 440, -120.0, 120.);
  hist->GetXaxis()->SetTitle("Truth z_{0} [mm]");
  hist->GetYaxis()->SetTitle("Arbitrary Units"); 

  id = base + "Z0Residual";
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " Z0Residual").c_str(), 200, -4, 4);
  hist->GetXaxis()->SetTitle("Track z_{0} Residual [mm]");
  hist->GetYaxis()->SetTitle("Arbitrary Units"); 

  id = base + "Z0Pull";
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " Z0Pull").c_str(), 100, -5, 5);
  hist->GetXaxis()->SetTitle("Track z_{0} Pull");
  hist->GetYaxis()->SetTitle("Arbitrary Units"); 

  // PHI
  id = base + "Phi";
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " Phi").c_str(), 64, -pi, pi);
  hist->GetXaxis()->SetTitle("Track #phi [rad]");
  hist->GetYaxis()->SetTitle("Arbitrary Units"); 

  id = base + "PhiTruth";
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " PhiTruth").c_str(), 64, -pi, pi);
  hist->GetXaxis()->SetTitle("Truth #phi [rad]");
  hist->GetYaxis()->SetTitle("Arbitrary Units"); 

  id = base + "PhiResidual";
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " PhiResidual").c_str(), 200, -0.1, 0.1);
  hist->GetXaxis()->SetTitle("Track #phi Residual [rad]");
  hist->GetYaxis()->SetTitle("Arbitrary Units"); 

  id = base + "PhiPull";
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " PhiPull").c_str(), 200, -10, 10);
  hist->GetXaxis()->SetTitle("Track #phi Pull");
  hist->GetYaxis()->SetTitle("Arbitrary Units"); 
        
  // THETA
  id = base + "Theta";
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " Theta").c_str(), 32, 0.0, pi);
  hist->GetXaxis()->SetTitle("Track #theta [rad]");
  hist->GetYaxis()->SetTitle("Arbitrary Units"); 

  id = base + "ThetaTruth";
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " ThetaTruth").c_str(), 32, 0.0, pi);
  hist->GetXaxis()->SetTitle("Truth #theta [rad]");
  hist->GetYaxis()->SetTitle("Arbitrary Units"); 

  id = base + "ThetaResidual";
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " ThetaResidual").c_str(), 100, -0.01, 0.01);
  hist->GetXaxis()->SetTitle("Track #theta Residual [rad]");
  hist->GetYaxis()->SetTitle("Arbitrary Units"); 

  id = base + "ThetaPull";
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " ThetaPull").c_str(), 100, -5, 5);
  hist->GetXaxis()->SetTitle("Track #theta Pull");
  hist->GetYaxis()->SetTitle("Arbitrary Units"); 
          
  // Q OVER P
  id = base + "qOverP";
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " qOverP").c_str(), 300, -0.003, 0.003);
  hist->GetXaxis()->SetTitle("Track q/p [1/MeV]");
  hist->GetYaxis()->SetTitle("Arbitrary Units"); 

  id = base + "qOverPTruth";
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " qOverPTruth").c_str(), 300, -0.003, 0.003);
  hist->GetXaxis()->SetTitle("Truth q/p [1/MeV]");
  hist->GetYaxis()->SetTitle("Arbitrary Units"); 

  id = base + "qOverPResidual";
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " qOverPResidual").c_str(), 100, -1e-2, 1e-2);
  hist->GetXaxis()->SetTitle("Track q/p Residual [1e3/MeV]");
  hist->GetYaxis()->SetTitle("Arbitrary Units"); 

  id = base + "qOverPPull";
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " qOverPPull").c_str(), 200, -10, 10);
  hist->GetXaxis()->SetTitle("Track q/p Pull");
  hist->GetYaxis()->SetTitle("Arbitrary Units"); 

  id = base + "pT";
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " pT").c_str(), nBinsPt, binsPt);
  hist->GetXaxis()->SetTitle("Track p_{T} [MeV]");
  hist->GetYaxis()->SetTitle("Arbitrary Units"); 

  id = base + "pTTruth";
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " pTTruth").c_str(), nBinsPt, binsPt);
  hist->GetXaxis()->SetTitle("Truth p_{T} [MeV]");
  hist->GetYaxis()->SetTitle("Arbitrary Units"); 


  // 2D
  id = base + "D0TruthVsD0Residual";
  hist = new TH2F(hname.c_str(), (trkColName + " " + type + " D0Truth Vs D0Residual").c_str(), 60, -15, 15, 4000, -10, 10);
  hist->GetXaxis()->SetTitle("Truth d_{0} [mm]"); 
  hist->GetYaxis()->SetTitle("Track d_{0} Residual [mm]");

  id = base + "D0TruthVsD0Pull";
  hist = new TH2F(hname.c_str(), (trkColName + " " + type + " D0Truth Vs D0Pull").c_str(), 60, -15, 15, 2000, -10, 10);
  hist->GetXaxis()->SetTitle("Truth d_{0} [mm]"); 
  hist->GetYaxis()->SetTitle("Track d_{0} Pull");

  id = base + "Z0TruthVsZ0Residual";
  hist = new TH2F(hname.c_str(), (trkColName + " " + type + " Z0Truth Vs Z0Residual").c_str(), 240, -120, 120, 4000, -10, 10);
  hist->GetXaxis()->SetTitle("Truth z_{0} [mm]"); 
  hist->GetYaxis()->SetTitle("Track z_{0} Residual [mm]");

  id = base + "Z0TruthVsZ0Pull";
  hist = new TH2F(hname.c_str(), (trkColName + " " + type + " Z0Truth Vs Z0Pull").c_str(), 240, -120, 120, 2000, -10, 10);
  hist->GetXaxis()->SetTitle("Truth z_{0} [mm]"); 
  hist->GetYaxis()->SetTitle("Track z_{0} Pull");

  id = base + "EtaTruthVsD0Residual";
  hist = new TH2F(hname.c_str(), (trkColName + " " + type + " EtaTruth Vs D0Residual").c_str(), nBinsEta, binsEta, 4000, -10, 10);
  hist->GetXaxis()->SetTitle("Truth #eta"); 
  hist->GetYaxis()->SetTitle("Track d_{0} Residual [mm]");

  id = base + "EtaTruthVsZ0Residual";
  hist = new TH2F(hname.c_str(), (trkColName + " " + type + " EtaTruth Vs Z0Residual").c_str(), nBinsEta, binsEta, 4000, -10, 10);
  hist->GetXaxis()->SetTitle("Truth #eta"); 
  hist->GetYaxis()->SetTitle("Track z_{0} Residual [mm]");
  
  id = base + "EtaTruthVsD0Pull";
  hist = new TH2F(hname.c_str(), (trkColName + " " + type + " Eta Vs D0Pull").c_str(), nBinsEta, binsEta, 2000, -10, 10);
  hist->GetXaxis()->SetTitle("Truth #eta"); 
  hist->GetYaxis()->SetTitle("Track d_{0} Pull");

  id = base + "EtaTruthVsZ0Pull";
  hist = new TH2F(hname.c_str(), (trkColName + " " + type + " Eta Vs Z0Pull").c_str(), nBinsEta, binsEta, 2000, -10, 10);
  hist->GetXaxis()->SetTitle("Truth #eta"); 
  hist->GetYaxis()->SetTitle("Track z_{0} Pull");

  id = base + "PhiTruthVsPhiResidual";
  hist = new TH2F(hname.c_str(), (trkColName + " " + type + " PhiTruth Vs PhiResidual").c_str(), 64, -pi, pi, 4000, -0.5, 0.5);
  hist->GetXaxis()->SetTitle("Truth #phi [rad]"); 
  hist->GetYaxis()->SetTitle("Track #phi Residual [rad]");

  id = base + "PhiTruthVsPhiPull";
  hist = new TH2F(hname.c_str(), (trkColName + " " + type + " PhiTruth Vs PhiPull").c_str(), 64, -pi, pi, 2000, -20, 20);
  hist->GetXaxis()->SetTitle("Truth #phi [rad]"); 
  hist->GetYaxis()->SetTitle("Track #phi Pull");

  id = base + "PtTruthVsD0Residual";
  hist = new TH2F(hname.c_str(), (trkColName + " " + type + " PtTruth Vs D0Residual").c_str(), nBinsPt, binsPt, 4000, -10, 10);
  hist->GetXaxis()->SetTitle("Truth p_{T} [MeV]"); 
  hist->GetYaxis()->SetTitle("Track d_{0} Residual [mm]");

  id = base + "PtTruthVsZ0Residual";
  hist = new TH2F(hname.c_str(), (trkColName + " " + type + " PtTruth Vs Z0Residual").c_str(), nBinsPt, binsPt, 4000, -10, 10);
  hist->GetXaxis()->SetTitle("Truth p_{T} [MeV]"); 
  hist->GetYaxis()->SetTitle("Track z_{0} Residual [mm]");
  
  id = base + "PtTruthVsD0Pull";
  hist = new TH2F(hname.c_str(), (trkColName + " " + type + " Pt Vs D0Pull").c_str(), nBinsPt, binsPt, 2000, -10, 10);
  hist->GetXaxis()->SetTitle("Truth p_{T} [MeV]"); 
  hist->GetYaxis()->SetTitle("Track d_{0} Pull");

  id = base + "PtTruthVsZ0Pull";
  hist = new TH2F(hname.c_str(), (trkColName + " " + type + " Pt Vs Z0Pull").c_str(), nBinsPt, binsPt, 2000, -10, 10);
  hist->GetXaxis()->SetTitle("Truth p_{T} [MeV]"); 
  hist->GetYaxis()->SetTitle("Track z_{0} Pull");

  */

  return StatusCode::SUCCESS;
}

StatusCode IdealTrackStudyAlg::bookRateHistograms() {

  std::set<std::string> m_trkCols = {"Reco", "Pseudo"};  // move to initialization
  if ( m_hasIdeal ) { m_trkCols.insert( "Ideal" ); }
  // for fake tracks, fill the histograms with reco information - or equivalent
  // fillRateHists functions designed with these two collections in mind
  m_trkCols.insert( "RecoReco" );
  m_trkCols.insert( "RecoFake" );
  // truth particles
  m_trkCols.insert( "Truth" );

  for (const std::string col : m_trkCols) {
    bool useTruth(true);
    if ( col == "RecoReco" || col == "RecoFake" ) { useTruth = false; }
    CHECK( bookRateHists( col, "",         useTruth ) );
    CHECK( bookRateHists( col, "BHadron",  useTruth ) );
    CHECK( bookRateHists( col, "CHadron",  useTruth ) );
    CHECK( bookRateHists( col, "BJet",     useTruth ) );
    CHECK( bookRateHists( col, "CJet",     useTruth ) );
    CHECK( bookRateHists( col, "LightJet", useTruth ) );
  }

  return StatusCode::SUCCESS;
}

StatusCode IdealTrackStudyAlg::bookRateHists( const std::string trkColName, const std::string type,
    bool useTruth) {

  std::string base = "/"+m_streamName+"/Rate/" + trkColName + "/" + type + "/";
  std::string name = "h_Rate_" + trkColName + type + "_"; // add Rate to name since some vars are duplicated in other hist sets
  auto id = base + name;
  auto hname = name;

  std::string isTruth = "";
  if (useTruth) { isTruth = "Truth "; }

  // match Supriya 
  // https://indico.cern.ch/event/907600/contributions/3870765/attachments/2046160/3428194/CTIDE_talk4_27May.pdf
  int nBinsEta = 22;
  double binsEta[23] = {-2.5, -2.0, -1.5, -1.25, -1.0, -0.80, -0.60, -0.40, -0.30, -0.20, -0.10, 0., 0.10, 0.20, 0.30, 0.40, 0.60, 0.80, 1.0, 1.25, 1.5, 2.0, 2.5};

  int nBinsPt = 39;
  double binsPt[40] = { 0.25, 0.5, 0.75, 1., 1.25, 1.5, 2., 2.5, 3., 3.5, 4., 5., 6., 7., 8., 9., 10., 12., 14., 16., 18., 20, 24., 28., 32., 36., 40, 45., 50., 55., 60., 70., 80., 100., 150, 250, 400, 600, 800, 1000 }; // in GeV
  for ( int i=0; i<40; i++ ) { 
    binsPt[i] = binsPt[i]*1e3; 
  } // convert to MeV


  // add some of my own
  // D0
  // 0.1 from | 0.0-> 1.0|
  // 0.2 from | 1.0-> 3.0|
  // 0.5 from | 3.0->10.0|
  // 1.0 from |10.0->20.0|
  // 2.0 from |20.0->50.0|
  // 5.0 from |50.0->100.|
  int nBinsD0 = 138;
  double binsD0[139] = { -100, -95, -90, -85, -80, -75, -70, -65, -60, -55, -50, -48, -46, -44, -42, -40, -38, -36, -34, -32, -30, -28, -26, -24, -22, -20, -19.0, -18.0, -17.0, -16.0, -15.0, -14.0, -13.0, -12.0, -11.0, -10.0, -9.5, -9.0, -8.5, -8.0, -7.5, -7.0, -6.5, -6.0, -5.5, -5.0, -4.5, -4.0, -3.5, -3.0, -2.8, -2.6, -2.4, -2.2, -2.0, -1.8, -1.6, -1.4, -1.2, -1.0, -0.9, -0.8, -0.7, -0.6, -0.5, -0.4, -0.3, -0.2, -0.1, 0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.2, 1.4, 1.6, 1.8, 2.0, 2.2, 2.4, 2.6, 2.8, 3.0, 3.5, 4.0, 4.5, 5.0, 5.5, 6.0, 6.5, 7.0, 7.5, 8.0, 8.5, 9.0, 9.5, 10.0, 11.0, 12.0, 13.0, 14.0, 15.0, 16.0, 17.0, 18.0, 19.0, 20.0, 22.0, 24.0, 26.0, 28.0, 30.0, 32.0, 34.0, 36.0, 38.0, 40.0, 42.0, 44.0, 46.0, 48.0, 50.0, 55.0, 60.0, 65.0, 70.0, 75.0, 80.0, 85.0, 90.0, 95.0, 100.0 };

  // creation vertex distance 
  int nBinsCVtx = 20;
  double binsCVtx[21] = {0.010, 0.016, 0.025, 0.040, 0.063, 0.100, 0.160, 0.250, 0.400, 0.630, 1.0, 1.585, 2.511, 4.000, 6.310, 10.000, 15.850, 25.119, 39.811, 63.096, 100.000};


  TH1F* hist = nullptr;

  // track quantities: pT, eta, d0, z0 (but fill from truth!?)
  hname = name + "pT";
  id = base + hname;
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " pT").c_str(), nBinsPt, binsPt);
  hist->GetXaxis()->SetTitle((isTruth + "p_{T} [MeV]").c_str());
  hist->GetYaxis()->SetTitle("Tracks"); 
  ATH_CHECK( m_thistSvc->regHist( id.c_str(), hist ) );

  hname = name + "Eta";
  id = base + hname;
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " eta").c_str(), nBinsEta, binsEta);
  hist->GetXaxis()->SetTitle((isTruth + "#eta").c_str());
  hist->GetYaxis()->SetTitle("Tracks");
  ATH_CHECK( m_thistSvc->regHist( id.c_str(), hist ) );

  hname = name + "D0";
  id = base + hname;
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " d0").c_str(), nBinsD0, binsD0);
  hist->GetXaxis()->SetTitle((isTruth + "d_{0} [mm]").c_str());
  hist->GetYaxis()->SetTitle("Tracks");
  ATH_CHECK( m_thistSvc->regHist( id.c_str(), hist ) );

  hname = name + "Z0";
  id = base + hname;
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " z0").c_str(), 120, -120, 120);
  hist->GetXaxis()->SetTitle((isTruth + "z_{0} [mm]").c_str());
  hist->GetYaxis()->SetTitle("Tracks");
  ATH_CHECK( m_thistSvc->regHist( id.c_str(), hist ) );

  // jet quantities:   pT, dR
  hname = name + "JetPt";
  id = base + hname;
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " Jet pT").c_str(), 100, 0, 2000);
  hist->GetXaxis()->SetTitle("Jet p_{T} [GeV]"); 
  hist->GetYaxis()->SetTitle("Tracks");
  ATH_CHECK( m_thistSvc->regHist( id.c_str(), hist ) );

  hname = name + "JetDR";
  id = base + hname;
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " Jet dR").c_str(), 50, 0, 0.5);
  hist->GetXaxis()->SetTitle(("#Delta R(" + isTruth + "track,jet)").c_str());
  hist->GetYaxis()->SetTitle("Tracks");
  ATH_CHECK( m_thistSvc->regHist( id.c_str(), hist ) );

  // truth quantities: creation vertex perpendicular distance
  hname = name + "CreationVtxLG";
  id = base + hname;
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " Creation Vtx").c_str(), nBinsCVtx, binsCVtx);
  hist->GetXaxis()->SetTitle("Creation Vertex [mm]"); 
  hist->GetYaxis()->SetTitle("Tracks");
  ATH_CHECK( m_thistSvc->regHist( id.c_str(), hist ) );

  hname = name + "CreationVtx";
  id = base + hname;
  hist = new TH1F(hname.c_str(), (trkColName + " " + type + " Creation Vtx").c_str(), 20, 0, 100);
  hist->GetXaxis()->SetTitle("Creation Vertex [mm]"); 
  hist->GetYaxis()->SetTitle("Tracks");
  ATH_CHECK( m_thistSvc->regHist( id.c_str(), hist ) );

  return StatusCode::SUCCESS;
}

//--------------------------------------------------------------------------//
//--------------------------------------------------------------------------//
// REST
//--------------------------------------------------------------------------//
//--------------------------------------------------------------------------//

// had to copy private function from here
// https://acode-browser.usatlas.bnl.gov/lxr/source/athena/PhysicsAnalysis/TrackingID/InDetTrackSystematicsTools/Root/InDetTrackTruthOriginTool.cxx?v=21.2
//
bool IdealTrackStudyAlg::isFrom(const xAOD::TruthParticle* part, int flav) {

  if ( part == nullptr ) return false;

  if( flav != 5 && flav != 4 ) return false;

  if( ! part->isHadron() ) return false;

  if( flav == 5 && part->isBottomHadron() ) return true;

  if( flav == 4 && part->isCharmHadron() ) return true;

  for(unsigned int p=0; p<part->nParents(); p++) {
    const xAOD::TruthParticle* parent = part->parent(p);
    if( isFrom(parent, flav) ) return true;
  }

  return false;
}


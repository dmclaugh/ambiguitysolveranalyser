reco_tf="Reco_tf.py \
	--autoConfiguration everything \
    --inputRDOFile %IN \
    --outputAODFile %OUT.AOD.root \
    --preExec 'all:rec.Commissioning.set_Value_and_Lock(True);from AthenaCommon.BeamFlags import jobproperties;jobproperties.Beam.numberOfCollisions.set_Value_and_Lock(20.0);from LArROD.LArRODFlags import larRODFlags;larRODFlags.NumberOfCollisions.set_Value_and_Lock(20);larRODFlags.nSamples.set_Value_and_Lock(4);larRODFlags.doOFCPileupOptimization.set_Value_and_Lock(True);larRODFlags.firstSample.set_Value_and_Lock(0);larRODFlags.useHighestGainAutoCorr.set_Value_and_Lock(True); from LArDigitization.LArDigitizationFlags import jobproperties;jobproperties.LArDigitizationFlags.useEmecIwHighGain.set_Value_and_Lock(False); rec.doTrigger.set_Value_and_Lock(False);' 'ESDtoAOD:from TriggerJobOpts.TriggerFlags import TriggerFlags;TriggerFlags.AODEDMSet.set_Value_and_Lock(\"AODSLIM\");' 'RAWtoESD:InDetFlags.doPseudoTracking.set_Value_and_Lock(True); InDetFlags.doSLHC.set_Value_and_Lock(False); InDetFlags.doTIDE_AmbiTrackMonitoring.set_Value_and_Lock(True)' 'RAWtoESD:from RecExConfig.RecFlags import rec; rec.UserAlgs=[\"postInclude.Donal.py\"]; rec.doTrigger.set_Value_and_Lock(False);' 'ESDtoAOD:from RecExConfig.RecFlags import rec; rec.UserAlgs=[\"postInclude.addMoreInfo.Donal.py\"];' \
	--postExec 'all:CfgMgr.MessageSvc(OutputLevel=ERROR).setError+=[\"HepMcParticleLink\"]' 'ESDtoAOD:fixedAttrib=[s if \"CONTAINER_SPLITLEVEL = '99'\" not in s else \"\" for s in svcMgr.AthenaPoolCnvSvc.PoolAttributes];svcMgr.AthenaPoolCnvSvc.PoolAttributes=fixedAttrib' 'ESDtoAOD:xAODMaker__xAODTruthCnvAlg(\"GEN_AOD2xAOD\",WriteInTimePileUpTruth=True);' 'RAWtoESD:xAODMaker__xAODTruthCnvAlg(\"GEN_AOD2xAOD\",WriteInTimePileUpTruth=True);InDetPRD_TruthTrajectoryBuilder.PRD_TruthTrajectoryManipulators=[InDetTruthTrajectorySorter];InDetTruthTrackCreation.PRD_TruthTrajectorySelectors=[];' 'ESDtoDPD:xAODMaker__xAODTruthCnvAlg(\"GEN_AOD2xAOD\",WriteInTimePileUpTruth=True);TruthDecor.DecorationPrefix=\"IDTIDE_\"' 'HITtoRDO:streamRDO.ItemList+=\"SiHitCollection#*\",' 'HITtoRDO:from AthenaCommon.AppMgr import ToolSvc;ToolSvc.PixelDigitizationTool.ToTMinCut=[0,4,6,6,6,0];' \
	--preInclude 'preInclude.noSlim.py' 'preInclude.IDonly_reconstruction.py' \
	--postInclude 'ESDtoAOD:postInclude.ESDtoAODStream.py' 'RAWtoESD:postInclude.RAWtoESDStream.py'
  "; \
pathena \
    --trf "$reco_tf" \
	--inDS mc16_13TeV.427080.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime.recon.RDO.e5362_e5984_s3126_r11212/   \
	--fileList RDO.20759048._000001.pool.root.1,RDO.20759048._000002.pool.root.1,RDO.20759048._000003.pool.root.1 \
    --outDS user.dmclaugh.Zprime_TrkObserver_maxPRD3_minNotShared4_minUniqueSCT2_maxShared7__1000events_3files \
    --nFilesPerJob 1 \
    --expertOnly_skipScout \
    --osMatching \
	--nEventsPerFile=1000 \
#!/bin/bash
setupATLAS
lsetup asetup

mkdir build
mkdir run
mkdir run/plots
mkdir run/output
mkdir run/logs
mkdir run/dump

cd build
acmSetup AthAnalysis,21.2.100
acm new_skeleton AmbiguitySolverAnalyser
acm compile

cd ../run

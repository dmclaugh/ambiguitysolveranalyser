import os

#---- Minimal job options -----
VERSION = 'v2'
# OUTNAME = 'nominal_addinfo_11112020_parent'
# OUTNAME = 'ForUpgrade_nom_with_DL_all_matched'
# OUTNAME = 'ForUpgrade_427'
# OUTNAME = 'ForUpgrade_obstracks'
DATASET = 'Baseline'
dataset_base_path = '/unix/atlastracking/dmcl/ambicutflow/'
# dataset_base_path ='/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/'
# dataset_base_path  = 'Baseline2_100621'
OUTNAME = 'Cutflow_nominal_270721'
# OUTNAME = 'BackToBasics_150421_NoBig3'
theApp.EvtMax = int(5e3) # int(1e4) 10000 event

datasets = {
    'Baseline'   : 'user.dmclaugh.Zprime_nominal_Ambiguity_25files_150721_nominal_2_EXT0/',
    # 'Baseline'       : 'user.dmclaugh.Zprime_nominal_Ambiguity_200files_080321_EXT0/',
    'Candidates' : 'user.svanstro.qt.flatZprime.s3431.fullPUtruth.withcandidatesAndPseudotracks.full_EXT0/',
    'GX2F-q-1.5' : 'user.svanstro.qt.flatZprime.s3431.fullPUtruth.pseudo.IBLcutat1.5.IBLcut2at0.quad.chi2cutat4.v3.RealQuadrature_EXT0',
    'GX2F-q-1.0' : 'user.svanstro.qt.flatZprime.s3431.fullPUtruth.pseudo.IBLcutat1.25.IBLcut1at1.75cut2at0.chi2cutat4.RealQ_EXT0/',
}

# input_files = ['/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/user.dmclaugh.Zprime_TrkObserver_nominal_maxTracksPerPRD2_1000events_3files_EXT0/user.dmclaugh.22662453.EXT0._000001.AOD.root',
# "/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/user.dmclaugh.Zprime_TrkObserver_nominal_maxTracksPerPRD2_1000events_3files_EXT0/user.dmclaugh.22662453.EXT0._000002.AOD.root",
# "/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/user.dmclaugh.Zprime_TrkObserver_nominal_maxTracksPerPRD2_1000events_3files_EXT0/user.dmclaugh.22662453.EXT0._000003.AOD.root"]

# input_files = ['/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/user.dmclaugh.Zprime_TrkObserver_nominalTracks_AllOutput__1500events_3files_EXT0/user.dmclaugh.23006070.EXT0._000001.AOD.root',
# "/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/user.dmclaugh.Zprime_TrkObserver_nominalTracks_AllOutput__1500events_3files_EXT0/user.dmclaugh.23006070.EXT0._000002.AOD.root",
# "/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/user.dmclaugh.Zprime_TrkObserver_nominalTracks_AllOutput__1500events_3files_EXT0/user.dmclaugh.23006070.EXT0._000003.AOD.root"]

# input_files = ['/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/user.dmclaugh.Zprime_TrkObserver_nominalTracks_bugsFixed_simp111_rem107_AllOutput__1500events_3files_EXT0/user.dmclaugh.23006626.EXT0._000001.AOD.root',
# '/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/user.dmclaugh.Zprime_TrkObserver_nominalTracks_bugsFixed_simp111_rem107_AllOutput__1500events_3files_EXT0/user.dmclaugh.23006626.EXT0._000002.AOD.root',
# '/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/user.dmclaugh.Zprime_TrkObserver_nominalTracks_bugsFixed_simp111_rem107_AllOutput__1500events_3files_EXT0/user.dmclaugh.23006626.EXT0._000003.AOD.root']

# input_files = ["/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/donalReco/run/nominal_without_bugs_addedInformation.AOD.root"]
# input_files = ["/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/user.dmclaugh.Zprime_nominal_101120_1000events_3files_EXT0/user.dmclaugh.23149697.EXT0._000001.AOD.root",
# "/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/user.dmclaugh.Zprime_nominal_101120_1000events_3files_EXT0/user.dmclaugh.23149697.EXT0._000002.AOD.root",
# "/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/user.dmclaugh.Zprime_nominal_101120_1000events_3files_EXT0/user.dmclaugh.23149697.EXT0._000003.AOD.root"]

# input_files = ["/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/user.dmclaugh.Zprime_TrkObserver_nominalTracks_AllOutput__1500events_3files_EXT0/user.dmclaugh.23006070.EXT0._000001.AOD.root",
# "/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/user.dmclaugh.Zprime_TrkObserver_nominalTracks_AllOutput__1500events_3files_EXT0/user.dmclaugh.23006070.EXT0._000002.AOD.root",
# "/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/user.dmclaugh.Zprime_TrkObserver_nominalTracks_AllOutput__1500events_3files_EXT0/user.dmclaugh.23006070.EXT0._000003.AOD.root"]

# input_files = ["/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/user.dmclaugh.Zprime_TrkObserver_maxTracksPerPRD3_1000events_3files_EXT0/user.dmclaugh.22662471.EXT0._000001.AOD.root",
# "/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/user.dmclaugh.Zprime_TrkObserver_maxTracksPerPRD3_1000events_3files_EXT0/user.dmclaugh.22662471.EXT0._000002.AOD.root",
# "/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/user.dmclaugh.Zprime_TrkObserver_maxTracksPerPRD3_1000events_3files_EXT0/user.dmclaugh.22662471.EXT0._000003.AOD.root"]

# input_files = ["/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/user.dmclaugh.Zprime_TrkObserver_maxPRD3_minNotShared4_minUniqueSCT2_maxShared7__1000events_3files_EXT0/user.dmclaugh.22845006.EXT0._000001.AOD.root",
# "/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/user.dmclaugh.Zprime_TrkObserver_maxPRD3_minNotShared4_minUniqueSCT2_maxShared7__1000events_3files_EXT0/user.dmclaugh.22845006.EXT0._000002.AOD.root",
# "/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/user.dmclaugh.Zprime_TrkObserver_maxPRD3_minNotShared4_minUniqueSCT2_maxShared7__1000events_3files_EXT0/user.dmclaugh.22845006.EXT0._000003.AOD.root"]

# input_files = ["/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/user.dmclaugh.Zprime_loosecuts_101120_1000events_3files_EXT0/user.dmclaugh.23150341.EXT0._000001.AOD.root",
# "/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/user.dmclaugh.Zprime_loosecuts_101120_1000events_3files_EXT0/user.dmclaugh.23150341.EXT0._000002.AOD.root",
# "/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/user.dmclaugh.Zprime_loosecuts_101120_1000events_3files_EXT0/user.dmclaugh.23150341.EXT0._000003.AOD.root"]

# input_files = ['/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/user.dmclaugh.Zprime_TrkObserver_maxPRD3_minNotShared5_minUniqueSCT3_maxShared5__1000events_3files_EXT0/user.dmclaugh.22844995.EXT0._000001.AOD.root',
# "/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/user.dmclaugh.Zprime_TrkObserver_maxPRD3_minNotShared5_minUniqueSCT3_maxShared5__1000events_3files_EXT0/user.dmclaugh.22844995.EXT0._000002.AOD.root",
# "/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/user.dmclaugh.Zprime_TrkObserver_maxPRD3_minNotShared5_minUniqueSCT3_maxShared5__1000events_3files_EXT0/user.dmclaugh.22844995.EXT0._000003.AOD.root"]

# jps.AthenaCommonFlags.FilesInput = ['/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/user.dmclaugh.Zprime_nominal_Ambiguity_50files_ps_030321_EXT0/user.dmclaugh.24416217.EXT0._000001.AOD.root',
# '/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/user.dmclaugh.Zprime_nominal_Ambiguity_50files_ps_030321_EXT0/user.dmclaugh.24416217.EXT0._000002.AOD.root',
# '/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/user.dmclaugh.Zprime_nominal_Ambiguity_50files_ps_030321_EXT0/user.dmclaugh.24416217.EXT0._000003.AOD.root',
# '/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/user.dmclaugh.Zprime_nominal_Ambiguity_50files_ps_030321_EXT0/user.dmclaugh.24416217.EXT0._000004.AOD.root',
# '/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/user.dmclaugh.Zprime_nominal_Ambiguity_50files_ps_030321_EXT0/user.dmclaugh.24416217.EXT0._000005.AOD.root',
# '/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/user.dmclaugh.Zprime_nominal_Ambiguity_50files_ps_030321_EXT0/user.dmclaugh.24416217.EXT0._000006.AOD.root',
# '/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/user.dmclaugh.Zprime_nominal_Ambiguity_50files_ps_030321_EXT0/user.dmclaugh.24416217.EXT0._000007.AOD.root'
# ]

# input_files = ['/unix/atlastracking/dmcl/user.dmclaugh.Zprime_nominal_ps_Ambiguity_200files_080321_EXT0/user.dmclaugh.24489158.EXT0._000001.AOD.root',
# '/unix/atlastracking/dmcl/user.dmclaugh.Zprime_nominal_ps_Ambiguity_200files_080321_EXT0/user.dmclaugh.24489158.EXT0._000002.AOD.root',
# '/unix/atlastracking/dmcl/user.dmclaugh.Zprime_nominal_ps_Ambiguity_200files_080321_EXT0/user.dmclaugh.24489158.EXT0._000003.AOD.root',
# '/unix/atlastracking/dmcl/user.dmclaugh.Zprime_nominal_ps_Ambiguity_200files_080321_EXT0/user.dmclaugh.24489158.EXT0._000004.AOD.root',
# '/unix/atlastracking/dmcl/user.dmclaugh.Zprime_nominal_ps_Ambiguity_200files_080321_EXT0/user.dmclaugh.24489158.EXT0._000005.AOD.root',
# '/unix/atlastracking/dmcl/user.dmclaugh.Zprime_nominal_ps_Ambiguity_200files_080321_EXT0/user.dmclaugh.24489158.EXT0._000006.AOD.root',
# '/unix/atlastracking/dmcl/user.dmclaugh.Zprime_nominal_ps_Ambiguity_200files_080321_EXT0/user.dmclaugh.24489158.EXT0._000007.AOD.root',
# '/unix/atlastracking/dmcl/user.dmclaugh.Zprime_nominal_ps_Ambiguity_200files_080321_EXT0/user.dmclaugh.24489158.EXT0._000008.AOD.root',
# '/unix/atlastracking/dmcl/user.dmclaugh.Zprime_nominal_ps_Ambiguity_200files_080321_EXT0/user.dmclaugh.24489158.EXT0._000009.AOD.root'
# # '/unix/atlastracking/dmcl/user.dmclaugh.Zprime_nominal_ps_Ambiguity_200files_080321_EXT0/user.dmclaugh.24489158.EXT0._0000010.AOD.root'
# ]


# input_files = ['/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/user.dmclaugh.Zprime_nominal_Ambiguity_200files_080321_EXT0/user.dmclaugh.24488542.EXT0._000001.AOD.root',
# '/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/user.dmclaugh.Zprime_nominal_Ambiguity_200files_080321_EXT0/user.dmclaugh.24488542.EXT0._000002.AOD.root',
# '/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/user.dmclaugh.Zprime_nominal_Ambiguity_200files_080321_EXT0/user.dmclaugh.24488542.EXT0._000003.AOD.root',
# '/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/user.dmclaugh.Zprime_nominal_Ambiguity_200files_080321_EXT0/user.dmclaugh.24488542.EXT0._000004.AOD.root',
# '/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/user.dmclaugh.Zprime_nominal_Ambiguity_200files_080321_EXT0/user.dmclaugh.24488542.EXT0._000005.AOD.root',
# '/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/user.dmclaugh.Zprime_nominal_Ambiguity_200files_080321_EXT0/user.dmclaugh.24488542.EXT0._000006.AOD.root',
# '/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/user.dmclaugh.Zprime_nominal_Ambiguity_200files_080321_EXT0/user.dmclaugh.24488542.EXT0._000007.AOD.root',
# '/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/user.dmclaugh.Zprime_nominal_Ambiguity_200files_080321_EXT0/user.dmclaugh.24488542.EXT0._000008.AOD.root',
# '/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/user.dmclaugh.Zprime_nominal_Ambiguity_200files_080321_EXT0/user.dmclaugh.24488542.EXT0._000009.AOD.root',
# # '/unix/atlastracking/dmcl/user.dmclaugh.Zprime_nominal_ps_Ambiguity_200files_080321_EXT0/user.dmclaugh.24489158.EXT0._0000010.AOD.root'
# ]



# input_files = ['/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/user.dmclaugh.Zprime_nominal_noAmbiguity_50files_correction_noscorenodr_220221_EXT0/user.dmclaugh.24296710.EXT0._000001.AOD.root',
# '/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/user.dmclaugh.Zprime_nominal_noAmbiguity_50files_correction_noscorenodr_220221_EXT0/user.dmclaugh.24296710.EXT0._000002.AOD.root',
# '/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/user.dmclaugh.Zprime_nominal_noAmbiguity_50files_correction_noscorenodr_220221_EXT0/user.dmclaugh.24296710.EXT0._000003.AOD.root',
# '/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/user.dmclaugh.Zprime_nominal_noAmbiguity_50files_correction_noscorenodr_220221_EXT0/user.dmclaugh.24296710.EXT0._000004.AOD.root',
# '/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/user.dmclaugh.Zprime_nominal_noAmbiguity_50files_correction_noscorenodr_220221_EXT0/user.dmclaugh.24296710.EXT0._000005.AOD.root',
# '/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/user.dmclaugh.Zprime_nominal_noAmbiguity_50files_correction_noscorenodr_220221_EXT0/user.dmclaugh.24296710.EXT0._000006.AOD.root',
# '/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/user.dmclaugh.Zprime_nominal_noAmbiguity_50files_correction_noscorenodr_220221_EXT0/user.dmclaugh.24296710.EXT0._000007.AOD.root',
# '/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/user.dmclaugh.Zprime_nominal_noAmbiguity_50files_correction_noscorenodr_220221_EXT0/user.dmclaugh.24296710.EXT0._000008.AOD.root',
# '/unix/atlas1/dmcl/cutflow_rdsparse/trackAnalysis/GridOutput/user.dmclaugh.Zprime_nominal_noAmbiguity_50files_correction_noscorenodr_220221_EXT0/user.dmclaugh.24296710.EXT0._000009.AOD.root',]


assert DATASET in datasets, 'cannot find DSID for DATASET: "'+DATASET+'".'

# get the path to the local DID download location
input_dir = os.path.join(dataset_base_path, datasets[DATASET])

# get the .root files that are stored locally under this DID
input_files = []
for file in os.listdir(input_dir):
    if file.endswith('.root'): # ignore .part files and other
        input_files.append(os.path.join(input_dir, file))




jps.AthenaCommonFlags.FilesInput = input_files
# jps.AthenaCommonFlags.FilesInput =  ['/unix/atlastracking/dmcl/donalreco_copy/run/nominal_ambi_1event.AOD.root']


# jps.AthenaCommonFlags.FilesInput = input_files
if not os.path.isdir(os.path.join('output', VERSION)):
    os.mkdir(os.path.join('output', VERSION))

jps.AthenaCommonFlags.AccessMode = "ClassAccess"              #Choose from TreeAccess,BranchAccess,ClassAccess,AthenaAccess,POOLAccess
#jps.AthenaCommonFlags.TreeName = "MyTree"                    #when using TreeAccess, must specify the input tree name
svcMgr.THistSvc.MaxFileSize = -1 # potentially speeds up 

hist_outFileName = os.path.join('output', VERSION,OUTNAME+'.root')


jps.AthenaCommonFlags.HistOutputs = ["ANALYSIS:"+hist_outFileName]  #register output files like this. MYSTREAM is used in the code
athAlgSeq += CfgMgr.AmbiguitySolverAnalyserAlg(OutputLevel=INFO)
# athAlgSeq += CfgMgr.AmbiguitySolverAnalyserAlg()                               #adds an instance of your alg to the main alg sequence

#---- Options you could specify on command line -----
#jps.AthenaCommonFlags.EvtMax=-1                          #set on command-line with: --evtMax=-1
#jps.AthenaCommonFlags.SkipEvents=0                       #set on command-line with: --skipEvents=0
#jps.AthenaCommonFlags.FilesInput = ["/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/CommonInputs/DAOD_PHYSVAL/mc16_13TeV.410501.PowhegPythia8EvtGen_A14_ttbar_hdamp258p75_nonallhad.DAOD_PHYSVAL.e5458_s3126_r9364_r9315_AthDerivation-21.2.1.0.root"]        #set on command-line with: --filesInput=...

include("AthAnalysisBaseComps/SuppressLogging.py")              #Optional include to suppress as much athena output as possible. Keep at bottom of joboptions so that it doesn't suppress the logging of the things you have configured above


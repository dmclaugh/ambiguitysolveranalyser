
#include "GaudiKernel/DeclareFactoryEntries.h"

#include "../AmbiguitySolverAnalyserAlg.h"

DECLARE_ALGORITHM_FACTORY( AmbiguitySolverAnalyserAlg )

DECLARE_FACTORY_ENTRIES( AmbiguitySolverAnalyser ) 
{
  DECLARE_ALGORITHM( AmbiguitySolverAnalyserAlg );
}

#ifndef AMBIGUITYSOLVERANALYSER_AMBIGUITYSOLVERANALYSERALG_H
#define AMBIGUITYSOLVERANALYSER_AMBIGUITYSOLVERANALYSERALG_H 1

#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"
#include <map>
#include <algorithm>
#include <typeinfo>
#include <string>
#include <unistd.h>

#include "TH1D.h"
#include "TProfile.h"
#include "TTree.h"


// hist service
#include "GaudiKernel/ITHistSvc.h"

// tool handler
#include "AsgTools/AnaToolHandle.h"

// tools
#include "InDetTrackSystematicsTools/InDetTrackTruthOriginTool.h"
#include "InDetTrackSystematicsTools/InDetTrackTruthOriginDefs.h"
// ---------------------------------------------------
// xAOD Includes
// ---------------------------------------------------
#include "xAODEventInfo/EventInfo.h"

// tracking
#include "xAODTracking/TrackParticle.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/TrackParticleAuxContainer.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"

#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/TrackMeasurementValidationContainer.h"
#include "xAODTracking/TrackStateValidationContainer.h"

// truth 
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthVertex.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthParticleAuxContainer.h"

// calo
#include "xAODCaloEvent/CaloClusterContainer.h"
#include "xAODCaloEvent/CaloCluster.h"

// truth links
#include "AthLinks/ElementLink.h"
#include "xAODTracking/TrackStateValidation.h"

static SG::AuxElement::Accessor< int >  a_rejPlace("rejectPlace");
static SG::AuxElement::Accessor< long int >  a_parId("parentId");
static SG::AuxElement::Accessor< long int >  a_Id("Id");
static SG::AuxElement::Accessor<  int >  a_numPixelHoles("numPixelHoles");
static SG::AuxElement::Accessor<  int >  a_numSCTHoles("numSCTHoles");
static SG::AuxElement::Accessor<  int >  a_numSplitSharedPixel("numSplitSharedPixel");
static SG::AuxElement::Accessor<  int >  a_numSplitSharedSCT("numSplitSharedSCT");
static SG::AuxElement::Accessor<  int >  a_numSharedOrSplit("numSharedOrSplit");
static SG::AuxElement::Accessor<  int >  a_numSharedOrSplitPixels("numSharedOrSplitPixels");
static SG::AuxElement::Accessor<  int >  a_numShared("numShared");
static SG::AuxElement::Accessor< float >  a_truthMatchProb("truthMatchProbability");
static SG::AuxElement::Accessor< double >  a_score("score");
static SG::AuxElement::Accessor< int >  a_isPatternTrack("isPatternTrack");

static SG::AuxElement::Accessor< int >  a_totalSiHits("totalSiHits");
static SG::AuxElement::Accessor< int >  a_hassharedblayer("hassharedblayer");
static SG::AuxElement::Accessor< int >  a_hassharedpixel("hassharedpixel");
static SG::AuxElement::Accessor< int >  a_numWeightedShared("numWeightedShared");

struct SeedFamily {
  const xAOD::TrackParticle* obsTrk;
  const xAOD::TrackParticle* idoTrk;
  bool reconstructed;
  int c_numberOfSubtracks;
  int max_gen;
  std::vector< const xAOD::TrackParticle* > descendants;
  std::vector< std::string > descendants_type;
  SeedFamily() : reconstructed(false), c_numberOfSubtracks(0), max_gen(0) {}
  void MaxGen(){
    max_gen = descendants.size();
    return;
  }
  void CheckReconstructed() {
    for ( const auto ides : descendants ) {
      if ( a_rejPlace( *ides ) == 0 ) {
        reconstructed = true;
        break;
      }
    }
  return;
  } // CheckReconstructed
  void number_of_subtracks() {
      for ( const auto ides : descendants ) {
        if ( a_rejPlace( *ides ) == 113 ) {
          c_numberOfSubtracks++;
        }
      }
    return;
  } // number_of_subtracks
};


class AmbiguitySolverAnalyserAlg: public ::AthAnalysisAlgorithm { 
 public: 
  AmbiguitySolverAnalyserAlg( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~AmbiguitySolverAnalyserAlg(); 

  ///uncomment and implement methods as required

                                        //IS EXECUTED:
  virtual StatusCode  initialize();     //once, before any input is loaded
  virtual StatusCode  beginInputFile(); //start of each input file, only metadata loaded
  //virtual StatusCode  firstExecute();   //once, after first eventdata is loaded (not per file)
  virtual StatusCode  execute();        //per event
  //virtual StatusCode  endInputFile();   //end of each input file
  //virtual StatusCode  metaDataStop();   //when outputMetaStore is populated by MetaDataTools
  virtual StatusCode  finalize();       //once, after all events processed
  

  ///Other useful methods provided by base class are:
  ///evtStore()        : ServiceHandle to main event data storegate
  ///inputMetaStore()  : ServiceHandle to input metadata storegate
  ///outputMetaStore() : ServiceHandle to output metadata storegate
  ///histSvc()         : ServiceHandle to output ROOT service (writing TObjects)
  ///currentFile()     : TFile* to the currently open input file
  ///retrieveMetadata(...): See twiki.cern.ch/twiki/bin/view/AtlasProtected/AthAnalysisBase#ReadingMetaDataInCpp

protected:
  typedef ElementLink<xAOD::TruthParticleContainer> TruthLink;
  typedef std::vector<ElementLink< xAOD::TrackStateValidationContainer > > MeasurementsOnTrack;
  typedef std::vector<ElementLink< xAOD::TrackStateValidationContainer > >::const_iterator MeasurementsOnTrackIter;
  const char* measurementNames = "msosLink"; // "MeaSurement On Surface"
  


 private: 

  int c_matched_validation;
  int c_matched_already_present;
  //AmbiguitySolverAnalyser methods
  StatusCode bookAllHistograms();
  StatusCode obsTracksInformation(const xAOD::TrackParticle* track, std::string origin, std::string study);
  const xAOD::TrackParticle* matchToidoTrack(const xAOD::TrackParticle* track, const xAOD::TrackParticleContainer* idoTracks);
  void matchToidoTrackValidator(const xAOD::TrackParticle* track, const xAOD::TrackParticleContainer* idoTracks, int numSubtracksInSeed);
  void validateInputTracks(const xAOD::TrackParticle* track);
  const xAOD::TruthParticle* getTruth( const xAOD::TrackParticle* track );
  std::string getParticleOrigin(const xAOD::TruthParticle* truthParticle);
  const xAOD::TruthParticle* getParentBHadron(const xAOD::TruthParticle* particle);
  const xAOD::TruthParticle* getParentDHadron(const xAOD::TruthParticle* particle);
  const xAOD::TruthParticle* getParent(const xAOD::TruthParticle* particle);
  const xAOD::TrackStateValidation* getMSOS( MeasurementsOnTrackIter pointer );
  const xAOD::TrackParticle* getMatchingPseudo(const xAOD::TrackParticle* matched, const xAOD::TrackParticleContainer* pseudoTracks);
  std::tuple<float, bool> calculateScore(const xAOD::TrackParticle* matched, const xAOD::TrackParticle* pseudo);
    std::tuple<float, bool> calculateScore2(const xAOD::TrackParticle* matched, const xAOD::TrackParticle* pseudo);
  StatusCode trackInformation(const xAOD::TrackParticle* track, std::string origin, std::string study);
  StatusCode efficiencyPlots(const xAOD::TruthParticle* truth, bool reconstructed, std::string study, std::string origin);
  void fill(std::string id, float x, float y, std::string origin, std::string study);
  void fill(std::string id, float x, std::string origin);
  const char* replaceAll(std::string str, const std::string& from, const std::string& to);
  void study_fracGoodParentFakeRate(const xAOD::TrackParticle* s, float parent_tmp, const xAOD::TrackParticle* matched, int gen);
  std::vector< SeedFamily > createSeedFamily(const xAOD::TrackParticleContainer* obsTracks);


  std::vector<const xAOD::TrackParticle*> matched_validation;

  bool fast_mode;

  // hist params
  const int tpT_nbins = 20;
  const float tpT_max   = 500.0;
  const int hpT_nbins = 20;
  const float hpT_max   = 2000.0;
  const int PR_nbins = 18;
  const float PR_max   = 144.0;
  const int dR_nbins = 20;
  const float dR_max   = 0.1;

  std::set<std::string> m_origins;
  std::set<std::string> m_studies;
  std::set<std::string> m_hadrons;
  std::set<std::string> m_rejloc;
  std::set<int> m_rejloc_ROI;
  std::map<int, std::vector<float>> m_ptROI;

  //Counters
  int c_validation_sameNumberOfTracksInSeed;
  int m_eventCounter;
  int m_sispCounter;
  int m_obsCounter;
  int m_idoCounter;
  int m_recoCounter;
  int m_pseudoCounter;
  int m_idealCounter;
  int m_truthParticlesCounter;

  std::vector<int> used_pseudo_indices;
  std::vector<int> used_ido_indices;

  const int detPixel = 1;
  const int detSCT   = 2;

  //Validation
  std::vector<long int> validation_Test_Id;
  std::vector<long int> validation_Test_parId;
  int num_parents;

}; 

#endif //> !AMBIGUITYSOLVERANALYSER_AMBIGUITYSOLVERANALYSERALG_H

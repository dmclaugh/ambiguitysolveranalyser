// AmbiguitySolverAnalyser includes
#include "AmbiguitySolverAnalyserAlg.h"
// #include "xAODCore/tools/PrintHelpers.h"


AmbiguitySolverAnalyserAlg::AmbiguitySolverAnalyserAlg( const std::string& name, ISvcLocator* pSvcLocator ) : AthAnalysisAlgorithm( name, pSvcLocator ){

}


AmbiguitySolverAnalyserAlg::~AmbiguitySolverAnalyserAlg() {}


StatusCode AmbiguitySolverAnalyserAlg::initialize() {

  // Doesn't generate all plots 
  fast_mode = false;
  
  ATH_MSG_INFO ("Initializing " << name() << "...");

  //Particle Types
  m_origins = {"All", "B", "D", "BHad", "DHad", "GEANT", "PU&UE", "Other"};
  m_hadrons = {"B", "D"};
  // m_studies = {"AllTracks", "AllObsTracks", "OutputTracks", "ObsTracks_Reconstructed", "ObsTracks_Reconstructable", "Reconstructed", "Reconstructable", "106", "110", "114", "106_pseudo", "110_pseudo", "114_pseudo", "parent", "parent_pseudo", "pseudo_B", "rejected", "Prelim_Reconstructed_obs", "Prelim_Reconstructed_pseudo", "Prelim_Reconstructed_info", "Prelim_InputNoZeroScore_obs", "Prelim_InputNoZeroScore_pseudo", "Prelim_InputNoZeroScore_info", "Prelim_Rejected_obs", "Prelim_Rejected_pseudo", "Prelim_Rejected_info", "Just_Pseudo", "Prelim_Rejected_ps", "Prelim_Reconstructed_ps", "Prelim_InputNoZeroScore_ps", "Prelim_AllB_obs", "Prelim_AllB_info", "TMPofreconstructedandreconstructable", "TMPofreconstructed"};
  m_studies = {"AllTracks","AllTracks_pseudo","AllObsTracks", "InputTracks", "OutputTracks", "Reconstructed", "Reconstructable", "Reconstructable_pt0", "Reconstructable_pt1", "Reconstructable_pt2", "Reconstructable_pt3", "Reconstructable_pt4"};
  m_rejloc_ROI = {106, 110, 114};
  m_rejloc = {"-2","0","1", "2","4","7","3","6","8","9","5","111","112","113","114","101","102","103","104","105","106","107","108","109","110"};
  m_ptROI = {
    {0,{0.0, 1.0}},
    {1,{1.0,2.5}},
    {2,{2.5,5.0}},
    {3,{5.0,10.0}},
    {4,{10.0, 1000000.0}}
    };
  m_studies.insert(m_rejloc.begin(), m_rejloc.end());
  // m_rejloc_nonRejections = {0, 1, 113, }

  // Counters
  m_eventCounter  = 0;
  m_sispCounter   = 0;
  m_obsCounter    = 0;
  m_idoCounter    = 0;
  m_recoCounter   = 0;
  m_pseudoCounter = 0;
  m_idealCounter  = 0;
  m_truthParticlesCounter = 0;
  num_parents = 0;

  // book histograms
  ATH_MSG_INFO("Booking histograms...");
  CHECK( bookAllHistograms() );

  return StatusCode::SUCCESS;
}

StatusCode AmbiguitySolverAnalyserAlg::finalize() {

  ATH_MSG_INFO ("Finalizing " << name() << "...");
  
  ATH_MSG_INFO ("Finished processing " << m_eventCounter<< " events \n"
      << "\t sisp   tracks : " << m_sispCounter << "\n"
      << "\t obs    tracks : " << m_obsCounter << "\n"
      << "\t ido    tracks : " << m_idoCounter << "\n"
      << "\t reco   tracks : " << m_recoCounter << "\n"
      << "\t pseudo tracks : " << m_pseudoCounter << "\n"
      << "\t ideal  tracks : " << m_idealCounter << "\n"
      << "\t truthParticlesCounter  tracks : " << m_truthParticlesCounter << "\n");


  // Validation - Identifiers
  if (!fast_mode){
  
    std::vector<long int>::iterator ip;
    std::vector<long int>::iterator ip2;
    ip = std::unique(validation_Test_parId.begin(), validation_Test_parId.end()); 
    ip2 = std::unique(validation_Test_Id.begin(), validation_Test_Id.end()); 
    validation_Test_parId.resize(std::distance(validation_Test_parId.begin(), ip));
    validation_Test_Id.resize(std::distance(validation_Test_Id.begin(), ip2));
    if (validation_Test_Id.size() != m_obsCounter){ATH_MSG_FATAL("Not all Ids are unique" << validation_Test_Id.size() << " / "<< m_obsCounter);}
    if (validation_Test_parId.size()+num_parents != m_obsCounter){ATH_MSG_FATAL("Multiple paretent IDs"<< validation_Test_parId.size()+num_parents << " / "<< m_obsCounter);}
      
  }

  return StatusCode::SUCCESS;
}

StatusCode AmbiguitySolverAnalyserAlg::execute() { 

  ATH_MSG_DEBUG ("Executing " << name() << "...");
  setFilterPassed(false); //optional: start with algorithm not passed
  m_eventCounter++;

  if (m_eventCounter % 100 == 0) {
    ATH_MSG_INFO("Processed "<< m_eventCounter << " events.");
  }

  // Event info
  const xAOD::EventInfo* ei = nullptr;
  CHECK( evtStore()->retrieve( ei , "EventInfo" ) );
  // hist("EventInfo/h_averagePileup")->Fill( ei->averageInteractionsPerCrossing() );

  // Input into the Ambiguity Processor - SiSp Track Candidates
  const xAOD::TrackParticleContainer* sispTracks = nullptr;
  // CHECK( evtStore()->retrieve( sispTracks , "SiSpTrackCandidatesTrackParticle" ) );     // Enable for SiSp tracks

  // ObservedTracks from the TrkObserverTool 
  //  - NO truth information 
  //  - HAS TrkObserverTool info (parentID, numShared, rejectPlace, etc.)
  const xAOD::TrackParticleContainer* obsTracks = nullptr;
  CHECK( evtStore()->retrieve( obsTracks , "ObservedTracks" ) ); 

  // InDetObservedTrackCollection from the observer tool converted to xAOD in a "standard" way
  //  - HAS truth information
  //  - NO  TrkObserverTool info (parentID, numShared, rejectPlace, etc.)
  const xAOD::TrackParticleContainer* idoTracks = nullptr; //ido = indet observed
  CHECK( evtStore()->retrieve( idoTracks , "InDetObservedTrackParticles" ) ); 

  // Reconstructed Tracks
  const xAOD::TrackParticleContainer* recoTracks = nullptr;
  CHECK( evtStore()->retrieve( recoTracks , "InDetTrackParticles" ) );

  // Pseudo-tracks
  const xAOD::TrackParticleContainer* pseudoTracks = nullptr;
  CHECK( evtStore()->retrieve( pseudoTracks , "InDetPseudoTrackParticles" ) ); 

  //Truth Particles
  const xAOD::TruthParticleContainer* truthParticles = nullptr;
  CHECK( evtStore()->retrieve( truthParticles , "TruthParticles" ) ); 

  matched_validation = std::vector<const xAOD::TrackParticle*>();

  // const xAOD::TruthParticleContainer* matched_validation = nullptr; 
  // Not running ideal-tracks at the moment
  // const xAOD::TrackParticleContainer* idealTracks = nullptr;
  // CHECK( evtStore()->retrieve( idealTracks , "InDetIdealTrackParticles" ) ); 


  //Size of TrackCollection Plots
  // hist("EventInfo/h_nSiSpTracks")    ->Fill( sispTracks  ->size() );
  // hist("EventInfo/h_nObservedTracks")->Fill( obsTracks   ->size() );
  // hist("EventInfo/h_nInDetObsTracks")->Fill( idoTracks   ->size() );
  // hist("EventInfo/h_nRecoTracks")    ->Fill( recoTracks  ->size() );
  // hist("EventInfo/h_nPseudoTracks")  ->Fill( pseudoTracks->size() );
  // hist("EventInfo/h_nTruthTracks")   ->Fill( truthParticles->size() );


  //Track Collection Counters
  // m_sispCounter   += sispTracks  ->size();
  m_obsCounter    += obsTracks   ->size();
  m_idoCounter    += idoTracks   ->size();
  m_recoCounter   += recoTracks  ->size();
  m_pseudoCounter += pseudoTracks->size();
  m_truthParticlesCounter += truthParticles->size();


  /*
  // VALIDATION - Same reco as obs ==0
  int counter =0;
  for (const auto track : *obsTracks) {
    if (a_rejPlace(*track) == 0){ 
      counter++;
    }
  }

  std::cout << "Number of rej0:" << counter << std::endl;
  */

  int count_obs(0);
  int count_match(0);
  int count_truth(0);

  std::vector<int> used_pseudo_indices;
  std::vector<int> used_ido_indices;

  std::vector< SeedFamily > allSeeds;

  // Organise tracks into seed families
  // Parent track = SiSp track candidate, Youngest child = Rejection/Reconstruction
  // N.B. One parent per child subtrack. Validated
  allSeeds = createSeedFamily(obsTracks);

  /*
  // VALIDATION - MULTIPLE PARENT STUDIES
  std::vector<int> all_parId_barZero;
  for (const auto track : *obsTracks) {
    if (a_parId( *track ) != 0){
      all_parId_barZero.push_back(a_parId(*track));
    }
  }
  int uniqueCount = std::unique(all_parId_barZero.begin(), all_parId_barZero.end()) - all_parId_barZero.begin();
  if (uniqueCount != all_parId_barZero.size()){
    ATH_MSG_FATAL("DONAL MCL: MULTIPLE PARENTS");
  }
  */

for ( auto sf : allSeeds ) {
  // Ignore low pt tracks
  if ( sf.descendants[0]->pt() < 500 ) { continue; }

  //Initialising
  float best_seed_score = 0;
  int best_seed_rej_place = 0;
  float seed_score_temp = 0;
  const xAOD::TrackParticle* best_seed;
  const xAOD::TrackParticle* best_seed_pseudo;
  const xAOD::TrackParticle* best_seed_matched;
  const xAOD::TruthParticle* best_seed_truth;
  int c_numberOfSubtracks_total = 0;

  sf.CheckReconstructed();  //Does seed eventually become reconstructed?
  sf.number_of_subtracks(); //Number of real ambi subtracks?
  sf.MaxGen();              //How many generations are created?
  c_numberOfSubtracks_total = sf.c_numberOfSubtracks;   //Real ambi subtracks created in Ambiguity Solver.
  int c_numberOfObsSubtracks_total = sf.max_gen; //Obs subtracks created in Observer Tool
  std::string parent_origin = "";

  int gen = 0;
  int ambigen = 0;
  int gen_all = 0;

  //Looking at all descendents including parent.
  for (const auto s : sf.descendants){

    bool reconstructed_track = false;
    bool reconstructable = false;
    gen_all++; 

    int rejection_loc = a_rejPlace(*s);
    if (rejection_loc == 113){ gen++; } // Real ambi Subtrack creation = rej loc 113

    //Matching ObsTracks to InDetObs
    const xAOD::TrackParticle* matched = nullptr;
    matched = matchToidoTrack(s, idoTracks); // All matched, multiple matchings due to nature of ObsTool. Track changes after subtrack (113) creation.
    if (!matched) continue;
    // Get Truth
    auto truth = getTruth(matched);
    std::string origin = getParticleOrigin(truth);
    // Truth Validation study
    if (!truth){
      hist("ValidationStudy/h_NoTruthInfoInMatchedIdo")->Fill(0.0, 1); 
      continue;
    }
    hist("ValidationStudy/h_NoTruthInfoInMatchedIdo")->Fill(0.0, 0); 

    const xAOD::TrackParticle* pseudo = nullptr;
    pseudo = getMatchingPseudo(matched, pseudoTracks);

    if (a_rejPlace(*s) == 0){ 
      // ========== STUDY 0: Reconstructed TRACKS ====================
      reconstructed_track = true;
      std::string study = "Reconstructed";
      obsTracksInformation(s, origin, study);
      trackInformation(matched, origin, study);
    }

    // ========== STUDY 1: ALL TRACKS ====================

    std::string study = "AllTracks";
    obsTracksInformation(s, origin, study);
    trackInformation(matched, origin, study);

    study = "AllTracks_pseudo";  
    if (pseudo)
      trackInformation(pseudo, origin, study);

    // ========== STUDY 2: INPUT TRACKS ====================
    if (gen_all == 0){

      study = "InputTracks";
      obsTracksInformation(s, origin, study);
      trackInformation(matched, origin, study);
    }

    // ========== STUDY 3: OUTPUT TRACKS ====================

    if (gen_all == (sf.descendants.size())){

      study = "OutputTracks";
      obsTracksInformation(s, origin, study);
      trackInformation(matched, origin, study);

      // ========== STUDY 3.1: OUTPUT TRACKS - Reconstrutable tracks but not reconstructed ====================
      study = "Reconstructable";
      if (pseudo and !reconstructed_track){
        obsTracksInformation(s, origin, study);
        trackInformation(matched, origin, study);

        // ========== STUDY 3.2: OUTPUT TRACKS - Pt ROI studies ====================
          int temp_counter=0;
          for ( auto ROI : m_ptROI) { 
            float pt = static_cast<float>(s->pt());
            study = "Reconstructable_pt" + std::to_string(temp_counter);
            // std::cout<< ROI.second[0] << " " << ROI.second[1] << std::endl;
            // std::cout<< study << std::endl;
            // std::cout<< pt << std::endl;
            if (pt>ROI.second[0] && pt<ROI.second[1]){
              obsTracksInformation(s, origin, study);
              trackInformation(matched, origin, study);
            }
            temp_counter++;
        }

      }

    }




    /*

    if (a_rejPlace(*s) == 0){ 
      reconstructed_track = true;
      // fill("EventInfo/TMP/From_B/p_truthMatchProbability_pT_reconstructed_From_B", matched->pt()*0.001, matched->auxdata<float>("truthMatchProbability"), "B", "TMP");
    }

    // ========== STUDY 1: ALL TRACKS ====================

    std::string study = "AllTracks";
    obsTracksInformation(s, origin, study);
    trackInformation(s, origin, study);

    // ========== STUDY 2: INPUT TRACKS ====================
    if (gen_all == 0){

      study = "InputTracks";
      obsTracksInformation(s, origin, study);
      trackInformation(s, origin, study);
    }

    // ========== STUDY 3: OUTPUT TRACKS ====================

    if (gen_all == (sf.descendants.size())){

      study = "OutputTracks";
      obsTracksInformation(s, origin, study);
      trackInformation(s, origin, study);

      // ========== STUDY 3.1: OUTPUT TRACKS - Reconstrutable tracks but not reconstructed ====================
      study = "ObsTracks_Reconstructable";
      if (pseudo and !reconstructed_track){
        obsTracksInformation(s, origin, study);
        trackInformation(s, origin, study);
      }

    }    

    */
     

    // FINAL STATE...  LAST CHILD ... Either Rejected or Accepted =========================================
/*
    if (gen_all == (sf.descendants.size())){
    // if (gen_all == 1){

    // auto matched = sf.descendants_matched;
    const xAOD::TrackParticle* matched = nullptr;
    matched = matchToidoTrack(s, idoTracks); // All matched, multiple matchings due to nature of ObsTool. Track changes after subtrack (113) creation.
    if (!matched) continue;
    // ATH_MSG_FATAL(typeid(matched).name());
    auto truth = getTruth(matched);
    std::string origin = getParticleOrigin(truth);

    const xAOD::TrackParticle* pseudo = nullptr;

    //~12% not matched to truth track.
    if (!truth){
      hist("ValidationStudy/h_NoTruthInfoInMatchedIdo")->Fill(0.0, 1); 
      continue;
    }
    hist("ValidationStudy/h_NoTruthInfoInMatchedIdo")->Fill(0.0, 0); 
    pseudo = getMatchingPseudo(matched, pseudoTracks);

    if (a_rejPlace(*s) == 0){ 
      reconstructed_track = true;
      // fill("EventInfo/TMP/From_B/p_truthMatchProbability_pT_reconstructed_From_B", matched->pt()*0.001, matched->auxdata<float>("truthMatchProbability"), "B", "TMP");
    }
    study = "ObsTracks_Reconstructable";
    if (pseudo and !reconstructed_track){
      obsTracksInformation(s, origin, study);
      trackInformation(s, origin, study);
    }

    }
*/
    }
}


  setFilterPassed(true); //if got here, assume that means algorithm passed
  return StatusCode::SUCCESS;
}

StatusCode AmbiguitySolverAnalyserAlg::beginInputFile() { 
  
  return StatusCode::SUCCESS;
}



StatusCode AmbiguitySolverAnalyserAlg::obsTracksInformation(const xAOD::TrackParticle* track, std::string origin, std::string study){

  auto o = "From_" + origin;
  std::string study_name = "TrackInformation/"+study+"/"+o+"/TrkObserverTool/"+study+"_";

  hist(study_name+"_h_rejectPlace_"+o)->           Fill( a_rejPlace(*track) );
  hist(study_name+"_h_ambiScore_"+o)->             Fill( a_score( *track ) );
  hist(study_name+"_h_numPixelHoles_"+o)->         Fill( a_numPixelHoles( *track ) );
  hist(study_name+"_h_numSCTHoles_"+o)->           Fill( a_numSCTHoles( *track ) );
  hist(study_name+"_h_numSplitSharedPixel_"+o)->   Fill( a_numSplitSharedPixel( *track ) );
  hist(study_name+"_h_numSplitSharedSCT_"+o)->     Fill( a_numSplitSharedSCT( *track ) );
  hist(study_name+"_h_numSharedOrSplit_"+o)->      Fill( a_numSharedOrSplit( *track ) );
  hist(study_name+"_h_numSharedOrSplitPixels_"+o)->Fill( a_numSharedOrSplitPixels( *track ) );
  hist(study_name+"_h_numShared_"+o)->             Fill( a_numShared( *track ) );

  hist(study_name+"_h_totalSiHits_"+o)->             Fill( a_totalSiHits( *track ) );
  hist(study_name+"_h_hassharedblayer_"+o)->         Fill( a_hassharedblayer( *track ) );
  hist(study_name+"_h_hassharedpixel_"+o)->          Fill( a_hassharedpixel( *track ) );
  hist(study_name+"_h_numWeightedShared_"+o)->       Fill( a_numWeightedShared( *track ) );

  //pT studies

  // study_name = "TrackInformation/"+study+"/"+o+"/TrkObserverTool/pT_"+study+"_";
  // hist(study_name+"_h_rejectPlace_"+o)->           Fill(  track->pt(), a_rejPlace(*track) );
  // hist(study_name+"_h_ambiScore_"+o)->             Fill( track->pt(), a_score( *track ) );
  // hist(study_name+"_h_numPixelHoles_"+o)->         Fill( track->pt(), a_numPixelHoles( *track ) );
  // hist(study_name+"_h_numSCTHoles_"+o)->           Fill( track->pt(), a_numSCTHoles( *track ) );
  // hist(study_name+"_h_numSplitSharedPixel_"+o)->   Fill( track->pt(), a_numSplitSharedPixel( *track ) );
  // hist(study_name+"_h_numSplitSharedSCT_"+o)->     Fill( track->pt(), a_numSplitSharedSCT( *track ) );
  // hist(study_name+"_h_numSharedOrSplit_"+o)->      Fill( track->pt(), a_numSharedOrSplit( *track ) );
  // hist(study_name+"_h_numSharedOrSplitPixels_"+o)->Fill( track->pt(), a_numSharedOrSplitPixels( *track ) );
  // hist(study_name+"_h_numShared_"+o)->             Fill( track->pt(), a_numShared( *track ) );

  // hist(study_name+"_h_totalSiHits_"+o)->             Fill( track->pt(), a_totalSiHits( *track ) );
  // hist(study_name+"_h_hassharedblayer_"+o)->         Fill( track->pt(), a_hassharedblayer( *track ) );
  // hist(study_name+"_h_hassharedpixel_"+o)->          Fill( track->pt(), a_hassharedpixel( *track ) );
  // hist(study_name+"_h_numWeightedShared_"+o)->       Fill( track->pt(), a_numWeightedShared( *track ) );

  if (origin != "All")
    obsTracksInformation(track, "All", study);

  return StatusCode::SUCCESS;
}

StatusCode AmbiguitySolverAnalyserAlg::trackInformation(const xAOD::TrackParticle* track, std::string origin, std::string study){

  auto o = "From_" + origin;
  std::string study_name = "TrackInformation/"+study+"/"+o+"/TrkInformation/"+study+"_";

  hist(study_name+"h_numberOfInnermostPixelLayerHits_"+o)->Fill( track->auxdata<uint8_t>("numberOfInnermostPixelLayerHits") );
  hist(study_name+"h_numberOfInnermostPixelLayerOutliers_"+o)->Fill( track->auxdata<uint8_t>("numberOfInnermostPixelLayerOutliers") );
  hist(study_name+"h_numberOfInnermostPixelLayerSharedHits_"+o)->Fill( track->auxdata<uint8_t>("numberOfInnermostPixelLayerSharedHits") );
  hist(study_name+"h_numberOfInnermostPixelLayerSplitHits_"+o)->Fill( track->auxdata<uint8_t>("numberOfInnermostPixelLayerSplitHits") );

  hist(study_name+"h_numberOfNextToInnermostPixelLayerHits_"+o)->Fill( track->auxdata<uint8_t>("numberOfNextToInnermostPixelLayerHits") );
  hist(study_name+"h_numberOfNextToInnermostPixelLayerOutliers_"+o)->Fill( track->auxdata<uint8_t>("numberOfNextToInnermostPixelLayerOutliers") );
  hist(study_name+"h_numberOfNextToInnermostPixelLayerSharedHits_"+o)->Fill( track->auxdata<uint8_t>("numberOfNextToInnermostPixelLayerSharedHits") );
  hist(study_name+"h_numberOfNextToInnermostPixelLayerSplitHits_"+o)->Fill( track->auxdata<uint8_t>("numberOfNextToInnermostPixelLayerSplitHits") );


  hist(study_name+"h_numberOfPixelHits_"+o)->Fill( track->auxdata<uint8_t>("numberOfPixelHits") );
  hist(study_name+"h_numberOfPixelOutliers_"+o)->Fill( track->auxdata<uint8_t>("numberOfPixelOutliers") );
  hist(study_name+"h_numberOfPixelHoles_"+o)->Fill( track->auxdata<uint8_t>("numberOfPixelHoles") );
  hist(study_name+"h_numberOfPixelSharedHits_"+o)->Fill( track->auxdata<uint8_t>("numberOfPixelSharedHits") );
  hist(study_name+"h_numberOfPixelSplitHits_"+o)->Fill( track->auxdata<uint8_t>("numberOfPixelSplitHits") );
  hist(study_name+"h_numberOfPixelDeadSensors_"+o)->Fill( track->auxdata<uint8_t>("numberOfPixelDeadSensors") );

  hist(study_name+"h_numSharedTotal_"+o)->Fill( track->auxdata<uint8_t>("numberOfInnermostPixelLayerSharedHits") + track->auxdata<uint8_t>("numberOfPixelSharedHits") + track->auxdata<uint8_t>("numberOfSCTSharedHits") );

  hist(study_name+"h_numberOfSCTHits_"+o)->Fill( track->auxdata<uint8_t>("numberOfSCTHits") );
  hist(study_name+"h_numberOfSCTOutliers_"+o)->Fill( track->auxdata<uint8_t>("numberOfSCTOutliers") );
  hist(study_name+"h_numberOfSCTHoles_"+o)->Fill( track->auxdata<uint8_t>("numberOfSCTHoles") );
  hist(study_name+"h_numberOfSCTSharedHits_"+o)->Fill( track->auxdata<uint8_t>("numberOfSCTSharedHits") );
  // hist(study_name+"h_numberOfSCTSplitHits_"+o)->Fill( track->auxdata<uint8_t>("numberOfSCTSplitHits") );
  hist(study_name+"h_numberOfSCTDeadSensors_"+o)->Fill( track->auxdata<uint8_t>("numberOfSCTDeadSensors") );

  hist(study_name+"h_pt_"+o)->Fill( track->pt() );
  hist(study_name+"h_eta_"+o)->Fill( track->eta() );
  hist(study_name+"h_z0_"+o)->Fill( track->z0() );
  hist(study_name+"h_d0_"+o)->Fill( track->d0() );

  //pT studies
  study_name = "TrackInformation/"+study+"/"+o+"/TrkInformation/pT_"+study+"_";

  // hist(study_name+"h_numberOfInnermostPixelLayerHits_"+o)->Fill( track->pt(), track->auxdata<uint8_t>("numberOfInnermostPixelLayerHits") );
  // hist(study_name+"h_numberOfInnermostPixelLayerOutliers_"+o)->Fill( track->pt(),  track->auxdata<uint8_t>("numberOfInnermostPixelLayerOutliers") );
  // hist(study_name+"h_numberOfInnermostPixelLayerSharedHits_"+o)->Fill( track->pt(), track->auxdata<uint8_t>("numberOfInnermostPixelLayerSharedHits") );
  // hist(study_name+"h_numberOfInnermostPixelLayerSplitHits_"+o)->Fill( track->pt(), track->auxdata<uint8_t>("numberOfInnermostPixelLayerSplitHits") );

  // hist(study_name+"h_numberOfNextToInnermostPixelLayerHits_"+o)->Fill( track->pt(), track->auxdata<uint8_t>("numberOfNextToInnermostPixelLayerHits") );
  // hist(study_name+"h_numberOfNextToInnermostPixelLayerOutliers_"+o)->Fill(  track->pt(), track->auxdata<uint8_t>("numberOfNextToInnermostPixelLayerOutliers") );
  // hist(study_name+"h_numberOfNextToInnermostPixelLayerSharedHits_"+o)->Fill( track->pt(),  track->auxdata<uint8_t>("numberOfNextToInnermostPixelLayerSharedHits") );
  // hist(study_name+"h_numberOfNextToInnermostPixelLayerSplitHits_"+o)->Fill( track->pt(), track->auxdata<uint8_t>("numberOfNextToInnermostPixelLayerSplitHits") );


  // hist(study_name+"h_numberOfPixelHits_"+o)->Fill( track->pt(), track->auxdata<uint8_t>("numberOfPixelHits") );
  // hist(study_name+"h_numberOfPixelOutliers_"+o)->Fill( track->pt(), track->auxdata<uint8_t>("numberOfPixelOutliers") );
  // hist(study_name+"h_numberOfPixelHoles_"+o)->Fill( track->pt(), track->auxdata<uint8_t>("numberOfPixelHoles") );
  // hist(study_name+"h_numberOfPixelSharedHits_"+o)->Fill( track->pt(), track->auxdata<uint8_t>("numberOfPixelSharedHits") );
  // hist(study_name+"h_numberOfPixelSplitHits_"+o)->Fill( track->pt(),  track->auxdata<uint8_t>("numberOfPixelSplitHits") );
  // hist(study_name+"h_numberOfPixelDeadSensors_"+o)->Fill( track->pt(), track->auxdata<uint8_t>("numberOfPixelDeadSensors") );

  // hist(study_name+"h_numSharedTotal_"+o)->Fill( track->pt(), track->auxdata<uint8_t>("numberOfInnermostPixelLayerSharedHits") + track->auxdata<uint8_t>("numberOfPixelSharedHits") + track->auxdata<uint8_t>("numberOfSCTSharedHits") );

  // hist(study_name+"h_numberOfSCTHits_"+o)->Fill( track->pt(), track->auxdata<uint8_t>("numberOfSCTHits") );
  // hist(study_name+"h_numberOfSCTOutliers_"+o)->Fill( track->pt(), track->auxdata<uint8_t>("numberOfSCTOutliers") );
  // hist(study_name+"h_numberOfSCTHoles_"+o)->Fill( track->pt(), track->auxdata<uint8_t>("numberOfSCTHoles") );
  // hist(study_name+"h_numberOfSCTSharedHits_"+o)->Fill(track->pt(), track->auxdata<uint8_t>("numberOfSCTSharedHits") );
  // // hist(study_name+"h_numberOfSCTSplitHits_"+o)->Fill( track->auxdata<uint8_t>("numberOfSCTSplitHits") );
  // hist(study_name+"h_numberOfSCTDeadSensors_"+o)->Fill( track->pt(), track->auxdata<uint8_t>("numberOfSCTDeadSensors") );

  // hist(study_name+"h_pt_"+o)->Fill( track->pt(), track->pt() );
  // hist(study_name+"h_eta_"+o)->Fill(track->pt(), track->eta() );
  // hist(study_name+"h_z0_"+o)->Fill( track->pt(), track->z0() );
  // hist(study_name+"h_d0_"+o)->Fill( track->pt(), track->d0() );



  auto truth = getTruth(track);
  auto parent = getParent(truth);
  if (parent){
  double DL = sqrt(pow(parent->decayVtx()->x(),2) + pow(parent->decayVtx()->y(),2));
  // std::cout << study << " " << DL << std::endl;
  hist(study_name+"h_DL_"+o)->Fill( DL );
  }

  if (origin != "All")
    trackInformation(track, "All", study);
  
  return StatusCode::SUCCESS;
}

const xAOD::TrackParticle* AmbiguitySolverAnalyserAlg::matchToidoTrack(const xAOD::TrackParticle* track, const xAOD::TrackParticleContainer* idoTracks){
  /* N.B. Multiple tracks matched to truth InDo track collection.
  */
  int ido_index = -1;
  int ido_index_temp =0;
  const xAOD::TrackParticle* matched = nullptr;

  for ( const auto itrk : *idoTracks) { 
    ido_index++;
    // if (std::find(used_ido_indices.begin(), used_ido_indices.end(), ido_index) != used_ido_indices.end()){ continue; } //Best Ido already assigned
    if( fabs(track->pt()-itrk->pt())   > 1  )   { continue; } // more than 1 MeV different, not a match
    if( fabs(track->eta()-itrk->eta()) > 0.1  ) { continue; } // etc
    if( fabs(track->z0()-itrk->z0())   > 0.1  ) { continue; } // etc
    if( fabs(track->d0()-itrk->d0())   > 0.1  ) { continue; } // etc
    // if( fabs(track->phi()-itrk->phi())   > 0.1  ) { continue; } // etc
    // if( fabs(track->m()-itrk->m())   > 0.1  ) { continue; } // etc
    // if( fabs(track->e()-itrk->e())   > 0.1  ) { continue; } // etc
    // if( fabs(track->rapidity()-itrk->rapidity())   > 0.1  ) { continue; } // etc
    // if( fabs(track->theta()-itrk->theta())   > 0.1  ) { continue; } // etc

    
    ido_index_temp = ido_index;
    matched = itrk;
    break;
  }
  //Cant find matching track with truth information
  //~0.28% not matched.
  if(!matched) {
    hist("ValidationStudy/h_NotMatchedIdoObs")->Fill(0.0, 1 ); 
    return nullptr;
  }
  used_ido_indices.push_back(ido_index_temp);
  hist("ValidationStudy/h_NotMatchedIdoObs")->Fill(0.0, 0); 
  return matched;
}

void AmbiguitySolverAnalyserAlg::matchToidoTrackValidator(const xAOD::TrackParticle* track, const xAOD::TrackParticleContainer* idoTracks, int numSubtracksInSeed){
  /* Check how many tracks in Obs are matched to tracks in InDetObs.
  Result:
  */
 int ido_index = -1;
 int ido_index_temp =0;
 
  int numObsTracksMatchedToInDetTracks = 0;
  for ( const auto itrk : *idoTracks) { 
    ido_index++;
    // if (track != itrk){ continue; }
    if (std::find(used_ido_indices.begin(), used_ido_indices.end(), ido_index) != used_ido_indices.end()){continue; } //Best Pseudo already assigned
    if( fabs(track->pt()-itrk->pt())   > 1  )   { continue; } // more than 1 MeV different, not a match
    if( fabs(track->eta()-itrk->eta()) > 0.1  ) { continue; } // etc
    if( fabs(track->z0()-itrk->z0())   > 0.1  ) { continue; } // etc
    if( fabs(track->d0()-itrk->d0())   > 0.1  ) { continue; } // etc
    // if( fabs(track->phi()-itrk->phi())   > 0.1  ) { continue; } // etc
    //   if( fabs(track->m()-itrk->m())   > 0.1  ) { continue; } // etc
    //   if( fabs(track->e()-itrk->e())   > 0.1  ) { continue; } // etc
    //   if( fabs(track->rapidity()-itrk->rapidity())   > 0.1  ) { continue; } // etc
    //   if( fabs(track->theta()-itrk->theta())   > 0.1  ) { continue; } // etc
    ido_index_temp = ido_index;
    numObsTracksMatchedToInDetTracks++;
  }
  used_ido_indices.push_back(ido_index_temp);
  // hist("ValidationStudy/h_DifferenceInNumOfInDetObsAndObs")->Fill( numObsTracksMatchedToInDetTracks-numSubtracksInSeed ); 
  // hist("ValidationStudy/h_DifferenceInNumOfInDetObsAndObs")->Fill( numObsTracksMatchedToInDetTracks ); 
  // return numObsTracksMatchedToInDetTracks;
}

const xAOD::TruthParticle* AmbiguitySolverAnalyserAlg::getTruth( const xAOD::TrackParticle* track ) {

    // create a pointer to a truth particle which will correspond to this track
    const xAOD::TruthParticle* linkedTruthParticle = nullptr;

    // if the track doesnt't have a valid truth link, skip to the next track
    // in practice, all tracks seem to have a truth link, but we need to also
    // check whether it's valid
    if ( !track->isAvailable<TruthLink>("truthParticleLink") ) { 
      ATH_MSG_DEBUG ("truthParticleLink is not available!");
      return nullptr;
    }

    // retrieve the link and check its validity
    // in this way, we only fill plots for tracks that are truth matched
    const TruthLink &link = track->auxdata<TruthLink>("truthParticleLink");

    // a missing or invalid link implies truth particle has been dropped from 
    // the truth record at some stage - probably it was from pilup which by
    // default we don't store truth information for
    if(!link or !link.isValid()) {
      ATH_MSG_DEBUG ("truthParticleLink is not valid!");
      return nullptr;
    }

    // seems safe to access and return the linked truth particle
    linkedTruthParticle = (*link);
    return linkedTruthParticle;
}

const xAOD::TruthParticle* AmbiguitySolverAnalyserAlg::getParent(const xAOD::TruthParticle* particle) {
  if ( particle == nullptr ) {
    return nullptr;
  }

  auto origin = getParticleOrigin(particle);

  if ( origin == "B" ) {
    return getParentBHadron(particle);
  } 
  else if ( origin == "D" ) {
    return getParentDHadron(particle);
  }
  else {
    return nullptr;
  }
}

const xAOD::TruthParticle* AmbiguitySolverAnalyserAlg::getParentBHadron(const xAOD::TruthParticle* particle) {
  if ( particle == nullptr ) {
    return nullptr;
  }
  if ( particle->isBottomHadron() ) {
    return particle;
  }
  for(unsigned int p = 0; p < particle->nParents(); p++) {
    const xAOD::TruthParticle* parent = particle->parent(p);
    auto result = getParentBHadron(parent);
    if ( result!= nullptr ) {
      return result;
    }
  }
  return nullptr;
}

const xAOD::TruthParticle* AmbiguitySolverAnalyserAlg::getParentDHadron(const xAOD::TruthParticle* particle) {
  if ( particle == nullptr ) {
    return nullptr;
  }
  if ( particle->isCharmHadron() ) {
    return particle;
  }
  for(unsigned int p = 0; p < particle->nParents(); p++) {
    const xAOD::TruthParticle* parent = particle->parent(p);
    auto result = getParentDHadron(parent);
    if ( result!= nullptr ) {
      return result;
    }
  }
  return nullptr;
}

bool isFromB(const xAOD::TruthParticle* particle) {
  if ( particle == nullptr ) {
    return false;
  }
  if ( particle->isBottomHadron() ) {
    return true;
  }
  for(unsigned int p = 0; p < particle->nParents(); p++) {
    const xAOD::TruthParticle* parent = particle->parent(p);
    if( isFromB(parent) ) {
      return true;
    }
  }
  return false;
}

bool isFromD(const xAOD::TruthParticle* particle) {
  if ( particle == nullptr ) {
    return false;
  }
  if( particle->isCharmHadron() ) {
    return true;
  }
  for(unsigned int p = 0; p < particle->nParents(); p++) {
    const xAOD::TruthParticle* parent = particle->parent(p);
    if( isFromD(parent) ) {
      return true;
    }
  }
  return false;
}

bool isNotFromPileup(const xAOD::TruthParticle* particle) {
  if ( particle == nullptr ) {
    return false;
  }
  // sample specific! Z' has a pdgid of 32
  if ( particle->pdgId() == 32 or particle->pdgId() == 33 ) {
    return true;
  }
  for (unsigned int p=0; p<particle->nParents(); p++) {
    const xAOD::TruthParticle* parent = particle->parent(p);
    if ( isNotFromPileup( parent ) ) {
      return true;
    }
  }
  return false;
}

std::string identifyParticle(const xAOD::TruthParticle* truthParticle) {
  
  if ( !isNotFromPileup(truthParticle) ) {
    return "PU&UE";
  }
  else if ( truthParticle->barcode() > 2e5 ) {
    return "GEANT";
  }  

  // ok, we have a primary particle
  else if (truthParticle->isBottomHadron()) {

    // of the B doesn't decay, it cannot decay to a C
    if ( !truthParticle->hasDecayVtx() ) return "Other";

    // get the decay vertex
    const xAOD::TruthVertex* decayVtx = truthParticle->decayVtx();

    // check if any decay product of the B is also B, if so return other
    for ( unsigned int i = 0; i < decayVtx->nOutgoingParticles(); i++ ) {
      const xAOD::TruthParticle* thisOutgoingParticle = decayVtx->outgoingParticle(i);
      if ( thisOutgoingParticle->isBottomHadron() ) return "Other";
    }
    // if we didn't already return, must have a weakly decaying B
    return "BHad";
  }
  else if (truthParticle->isCharmHadron()) {

    // of the B doesn't decay, it cannot decay to a C
    if ( !truthParticle->hasDecayVtx() ) return "Other";

    // get the decay vertex
    const xAOD::TruthVertex* decayVtx = truthParticle->decayVtx();

    // check if any decay product of the B is also B, if so return other
    for ( unsigned int i = 0; i < decayVtx->nOutgoingParticles(); i++ ) {
      const xAOD::TruthParticle* thisOutgoingParticle = decayVtx->outgoingParticle(i);
      if ( thisOutgoingParticle->isCharmHadron() ) return "Other";
    }
    // if we didn't already return, must have a weakly decaying D
    return "DHad";
  }
  else if ( isFromB(truthParticle) ) {// and !isFromD(&truthParticle) ) {
    return "B";
  }
  else if ( !isFromB(truthParticle) and isFromD(truthParticle) ) {
    return "D";
  }
  else {
    return "Other";
  }
}

std::string AmbiguitySolverAnalyserAlg::getParticleOrigin(const xAOD::TruthParticle* truthParticle) {
  std::string origin = identifyParticle(truthParticle);
  if ( m_origins.find(origin) == m_origins.end() ) {
    return "All";
  }
  else {
    return origin;  
  }
  return origin;

}

const xAOD::TrackStateValidation* AmbiguitySolverAnalyserAlg::getMSOS(MeasurementsOnTrackIter pointer) {
    // check if the element link is valid
    if ( !(*pointer).isValid() ) return nullptr;
    const xAOD::TrackStateValidation* msos = *(*pointer);
    if ( !msos->trackMeasurementValidationLink().isValid() ) return nullptr;
    if ( !(*(msos->trackMeasurementValidationLink())) ) return nullptr;
    if ( msos->type() != 0 ) { return nullptr; } // if it's not a hole or an outlier auxdata<float>("phi");
    return msos;
}

const xAOD::TrackParticle* AmbiguitySolverAnalyserAlg::getMatchingPseudo(const xAOD::TrackParticle* matched, const xAOD::TrackParticleContainer* pseudoTracks){
  const xAOD::TrackParticle* pseudo = nullptr;
  auto truth = getTruth(matched);
  int number_of_matched_pseudos = 0;
  bool reconstructable = false;
  float seed_score_temp = 0.0;
  float seed_score = 0.0;
  int pseudo_index = -1;
  int pseudo_index_temp = 0;
  
  for ( const auto ptrk : *pseudoTracks ) {
    pseudo_index++;
    auto ptruth = getTruth(ptrk);
    if(!ptruth) { continue; }
    if(truth->barcode() != ptruth->barcode()) { continue; }
    // if (std::find(used_pseudo_indices.begin(), used_pseudo_indices.end(), pseudo_index) != used_pseudo_indices.end()){continue; } //Best Pseudo already assigned
    // match found
    auto [seed_score_temp, reconstructable] = calculateScore(matched, ptrk);
    // ATH_MSG_FATAL(seed_score_temp);
    if (seed_score_temp > seed_score){
      seed_score = seed_score_temp;
      pseudo = ptrk;
      pseudo_index_temp = pseudo_index;
    }
    number_of_matched_pseudos++;
    // break;
  }

  used_pseudo_indices.push_back(pseudo_index_temp);
  hist("PseudoMatchValidation/h_numOfpseudoMatchedToReconstructable")->Fill(number_of_matched_pseudos, 1);
  return pseudo;
}

std::tuple<float, bool> AmbiguitySolverAnalyserAlg::calculateScore(const xAOD::TrackParticle* matched, const xAOD::TrackParticle* pseudo){
  static SG::AuxElement::ConstAccessor< MeasurementsOnTrack > acc_MeasurementsOnTrack( measurementNames );
  bool reconstructable = false;
  // now compare cluster content
  const MeasurementsOnTrack& measurementsSeed   = acc_MeasurementsOnTrack( *matched );
  const MeasurementsOnTrack& measurementsPseudo = acc_MeasurementsOnTrack( *pseudo );
  // calculate denominator
  float denom = 10.0*pseudo->auxdata<uint8_t>("numberOfPixelHits")
    + 5.0*pseudo->auxdata<uint8_t>("numberOfSCTHits");

  // calculate numerator
  float numer = 0.0;
  // loop over hits on seed
  // std::map<int, int> matched_pseudo_indexs;
  
  for ( auto msos_iter_seed=measurementsSeed.begin(); 
      msos_iter_seed!=measurementsSeed.end(); ++msos_iter_seed ) {
      
    const xAOD::TrackStateValidation* msos_seed = getMSOS( msos_iter_seed ); 
    if ( msos_seed == nullptr) { continue; }
    // go back to cluster
    const xAOD::TrackMeasurementValidation* clus_seed =  *(msos_seed->trackMeasurementValidationLink());
    // int number_of_matched_pseudos_clusters = 0;
    // look for match on pseudo track
    bool onPseudo(false);
    // int index = -1;
    for ( auto msos_iter_pseudo=measurementsPseudo.begin(); 
        msos_iter_pseudo!=measurementsPseudo.end(); ++msos_iter_pseudo ) {
          // index++;
      const xAOD::TrackStateValidation* msos_pseudo = getMSOS( msos_iter_pseudo ); 
      if ( msos_pseudo == nullptr) { continue; }
      const xAOD::TrackMeasurementValidation* clus_pseudo =  *(msos_pseudo->trackMeasurementValidationLink());
      if ( clus_seed != clus_pseudo ) { continue; }
      onPseudo = true;

      // matched_pseudo_indexs[index]++;
      // measurementsPseudo = std::remove(measurementsPseudo.begin(), measurementsPseudo.end(), msos_iter_seed);
      // measurementsPseudo.erase(std::remove(measurementsPseudo.begin(), measurementsPseudo.end(), msos_iter_pseudo));
      // measurementsPseudo.erase(msos_iter_pseudo);
      // measurementsPseudo.erase(std::remove(measurementsPseudo.begin(), measurementsPseudo.end(), msos_iter_pseudo), measurementsPseudo.end());
      // msos_iter_pseudo= measurementsPseudo.erase(msos_iter_pseudo);
      // number_of_matched_pseudos_clusters++;
      break;
    }


    // hist("PseudoMatchValidation/h_numOfpseudoMatchedToReconstructableClustersSimple")->Fill(number_of_matched_pseudos_clusters);

    if(onPseudo) {
      reconstructable = true;
      if( msos_seed->detType() == detPixel ) { numer += 10.0; } 
      else { numer += 5.0; } // SCT
    }

  } // loop over seed MSOS

//   for (auto it = matched_pseudo_indexs.begin(); it != matched_pseudo_indexs.end(); it++ )
// {
//   //Needs to be a sum of each unique sum across all keys

//     // hist("PseudoMatchValidation/h_numOfpseudoMatchedToReconstructableClusters")->Fill(it->first, it->second);
// }
  return {numer/denom, reconstructable};

}

std::tuple<float, bool> AmbiguitySolverAnalyserAlg::calculateScore2(const xAOD::TrackParticle* matched, const xAOD::TrackParticle* pseudo){
  static SG::AuxElement::ConstAccessor< MeasurementsOnTrack > acc_MeasurementsOnTrack( measurementNames );
  bool reconstructable = false;
  // now compare cluster content
  // std::cout << "Hello1" << std::endl;
  // const MeasurementsOnTrack& measurementsSeed   = acc_MeasurementsOnTrack( *matched );
  // const MeasurementsOnTrack& measurementsPseudo = acc_MeasurementsOnTrack( *pseudo );
  // calculate denominator
  float denom = 10.0*pseudo->auxdata<uint8_t>("numberOfPixelHits")
    + 5.0*pseudo->auxdata<uint8_t>("numberOfSCTHits");

  // calculate numerator
  float numer = 0.0;
  // loop over hits on seed
  // std::map<int, int> matched_pseudo_indexs;
  numer = 10.0*matched->auxdata<uint8_t>("numberOfPixelHits")
    + 5.0*matched->auxdata<uint8_t>("numberOfSCTHits");
  
  std::cout << numer << "," << denom << std::endl;
  std::cout << matched->auxdata<float>("truthMatchProbability") << "," << pseudo->auxdata<float>("truthMatchProbability") <<std::endl;
  return {numer/denom, reconstructable};

}

const char* AmbiguitySolverAnalyserAlg::replaceAll(std::string str, const std::string& from, const std::string& to) {
  if(from.empty())
    return str.c_str();
  size_t start_pos = 0;
  while((start_pos = str.find(from, start_pos)) != std::string::npos) {
    str.replace(start_pos, from.length(), to);
    start_pos += to.length(); // In case 'to' contains 'from', like replacing 'x' with 'yx'
  }
  return str.c_str();
}


void AmbiguitySolverAnalyserAlg::fill(std::string id, float x, float y, std::string origin, std::string study)
{
  
  hist(replaceAll(id, "<o>", "From_All_" + study))->Fill(x, y);

  if ( origin != "All" and m_origins.find(origin) != m_origins.end() ) {
    hist(replaceAll(id, "<o>", "From_"+origin+ "_" + study))->Fill(x, y);    
  }
}

void AmbiguitySolverAnalyserAlg::fill(std::string id, float x, std::string origin)
{
  hist(replaceAll(id, "<o>", "From_All"))->Fill(x);

  if ( origin != "All" and m_origins.find(origin) != m_origins.end() ) {
    hist(replaceAll(id, "<o>", "From_"+origin))->Fill(x);    
  }
}

StatusCode AmbiguitySolverAnalyserAlg::efficiencyPlots(const xAOD::TruthParticle* truth, bool reconstructed, std::string study, std::string origin){

  // auto truthCounts = std::unordered_map<std::string, int>();
  // auto Binfo = std::unordered_map<const xAOD::TruthParticle*, std::unordered_map<std::string, int>>();
  // auto Dinfo = std::unordered_map<const xAOD::TruthParticle*, std::unordered_map<std::string, int>>();


      auto o = "From_" + origin + "_" + study;
      
      double prod_rad = 0;
      if ( truth->hasProdVtx() ) { // check we have a production vertex otherwise we get a segmentation fault
        prod_rad = sqrt(pow(truth->prodVtx()->x(),2) + pow(truth->prodVtx()->y(),2));
      }


      if ( reconstructed ) {
        // the particle was reconstructed
        fill("Efficiency/<o>/p_recoEfficiency_pT_<o>", truth->pt()*0.001, 1, origin, study);
        fill("Efficiency/<o>/p_recoEfficiency_PR_<o>", prod_rad, 1, origin, study);
        // fill("Efficiency/<o>/p_recoEfficiency_d0_<o>", truth->auxdata<float>("d0"), 1, origin);

        if ( m_hadrons.find(origin) != m_hadrons.end() ) {
          auto parent = getParent(truth);
          if (parent) {
            double parent_pt = parent->pt()*0.001; // GeV
            // double parent_dR = sqrt(pow(truth->phi() - parent->phi(),2) + pow(truth->eta() - parent->eta(),2));
            float d_eta = truth->eta() - parent->eta();
            float d_phi = truth->phi() - parent->phi();
            if ( std::abs(d_phi) > TMath::Pi() ) {
              d_phi = std::abs(d_phi) - 2.0*TMath::Pi();
            }
            double parent_dR = TMath::Sqrt(pow(d_eta, 2) + pow(d_phi, 2));




            double B_DL = sqrt(pow(parent->decayVtx()->x(),2) + pow(parent->decayVtx()->y(),2));

            hist("Efficiency/"+o+"/p_recoEfficiency_pT_"+origin+"_"+o)->Fill(parent_pt, 1);
            hist("Efficiency/"+o+"/p_recoEfficiency_dR_"+o)->Fill(parent_dR, 1);
            

            if ( origin == "B" ) {
              // Binfo[parent]["truth"]++; Binfo[parent]["reco"]++;
              hist("Efficiency/"+o+"/p_recoEfficiency_DL_"+o)->Fill(B_DL, 1);
            }
            else if ( origin == "D" ) {
              // Dinfo[parent]["truth"]++; Dinfo[parent]["reco"]++;
            }
          }
        }
      }
      else {
        // the particle was not reconstructed
        fill("Efficiency/<o>/p_recoEfficiency_pT_<o>", truth->pt()*0.001, 0, origin, study);
        fill("Efficiency/<o>/p_recoEfficiency_PR_<o>", prod_rad, 0, origin, study);
        // fill("Efficiency/<o>/p_recoEfficiency_d0_<o>", truth->auxdata<float>("d0"), 0, origin);

        if ( m_hadrons.find(origin) != m_hadrons.end() ) {
          auto parent = getParent(truth);
          if (parent) {
            double parent_pt = parent->pt()*0.001; // GeV
            // double parent_dR = sqrt(pow(truth->phi() - parent->phi(),2) + pow(truth->eta() - parent->eta(),2));
            float d_eta = truth->eta() - parent->eta();
            float d_phi = truth->phi() - parent->phi();
            if ( std::abs(d_phi) > TMath::Pi() ) {
              d_phi = std::abs(d_phi) - 2.0*TMath::Pi();
            }
            double parent_dR = TMath::Sqrt(pow(d_eta, 2) + pow(d_phi, 2));

            double B_DL = sqrt(pow(parent->decayVtx()->x(),2) + pow(parent->decayVtx()->y(),2));

            hist("Efficiency/"+o+"/p_recoEfficiency_pT_"+origin+"_"+o)->Fill(parent_pt, 0);
            hist("Efficiency/"+o+"/p_recoEfficiency_dR_"+o)->Fill(parent_dR, 0);
            

            if ( origin == "B" ) {
              hist("Efficiency/"+o+"/p_recoEfficiency_DL_"+o)->Fill(B_DL, 0);
              // Binfo[parent]["truth"]++;
            }
            else if ( origin == "D" ) {
              // Dinfo[parent]["truth"]++;
            }
          }
        }
      }

  return StatusCode::SUCCESS;
}
void AmbiguitySolverAnalyserAlg::study_fracGoodParentFakeRate(const xAOD::TrackParticle* s, float parent_tmp, const xAOD::TrackParticle* matched, int gen){

  if (a_rejPlace(*s) != 4 || a_rejPlace(*s) != 1 || a_rejPlace(*s) != 7){
    std::string study = "FakeSubtracks";
    //Good Parent
    if (parent_tmp >= 0.75 ){
      if (a_truthMatchProb(*matched)>= 0.75)
        hist(study+"/h_GoodParent_B")->Fill(gen, 1);
      else if (a_truthMatchProb(*matched) < 0.75)
        hist(study+"/h_GoodParent_B")->Fill(gen, 0);
    }
    //Bad Parent
    if (parent_tmp < 0.75) {
      if (a_truthMatchProb(*matched)>= 0.75)
        hist(study+"/h_BadParent_B")->Fill(gen, 1);
      else if (a_truthMatchProb(*matched) < 0.75)
        hist(study+"/h_BadParent_B")->Fill(gen, 0);
    }
  }
}

StatusCode AmbiguitySolverAnalyserAlg::bookAllHistograms() {
  std::string id = "";


  //==================================Validation Studies ==================================================================================

  id="ValidationStudy/h_DifferenceInNumOfInDetObsAndObs";
  CHECK( book( TH1D(id.c_str(), "", 21, -10.5, 10.5 ) ) );
  hist(id.c_str())->GetXaxis()->SetTitle("DifferenceInNumOfInDetObsAndObs"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

  id = "ValidationStudy/h_NotMatchedIdoObs";
  CHECK( book( TProfile(id.c_str(), ("Frac of Ido matched"), 20, -0.5, 0.5 ) ) );
  hist(id.c_str())->GetXaxis()->SetTitle("N/A"); hist(id.c_str())->GetYaxis()->SetTitle("Frac of Ido matched");

  id = "ValidationStudy/h_NoTruthInfoInMatchedIdo";
  CHECK( book( TProfile(id.c_str(), ("Frac of truth Ido matched"), 20, -0.5, 0.5 ) ) );
  hist(id.c_str())->GetXaxis()->SetTitle("N/A"); hist(id.c_str())->GetYaxis()->SetTitle("Frac of truth Ido matched");

  //==============================================================================================================================

  if (!fast_mode){
    for (std::string study : m_studies){
      for (auto origin : m_origins){

      auto o = "From_" + origin;
      std::string study_name = "TrackInformation/"+study+"/"+o+"/TrkObserverTool/"+study+"_";

      id=study_name+"_h_rejectPlace_"+o;
      CHECK( book( TH1D(id.c_str(), "Reject Locations", 201, -0.5, 200.5 ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Reject Location"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

      id=study_name+"_h_ambiScore_"+o;
      CHECK( book( TH1D(id.c_str(), "Ambiguity Solver Score for Reconstructed", 2001, -0.05, 200.05 ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Score"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

      id=study_name+"_h_numPixelHoles_"+o;
      CHECK( book( TH1D(id.c_str(), "numPixelHoles", 21, -5.5, 15.5  ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Number of Pixel Holes"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

      id=study_name+"_h_numSCTHoles_"+o;
      CHECK( book( TH1D(id.c_str(), "numSCTHoles", 21, -5.5, 15.5 ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Number of SCT Holes"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

      id=study_name+"_h_numSplitSharedPixel_"+o;
      CHECK( book( TH1D(id.c_str(), "numSplitSharedPixel", 21, -5.5, 15.5 ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Number of Pixel clusters comptaible with being split that are also shared"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

      id=study_name+"_h_numSplitSharedSCT_"+o;
      CHECK( book( TH1D(id.c_str(), "numSplitSharedSCT", 21, -5.5, 15.5) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Number of SCT clusters comptaible with being split that are also shared"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

      id=study_name+"_h_numSharedOrSplit_"+o;
      CHECK( book( TH1D(id.c_str(), "numSharedOrSplit", 21, -5.5, 15.5 ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Number of split + shared clusters"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

      id=study_name+"_h_numSharedOrSplitPixels_"+o;
      CHECK( book( TH1D(id.c_str(), "numSharedOrSplitPixels", 21, -5.5, 15.5 ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Number of pixel clusters that are either split or shared"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

      id=study_name+"_h_numShared_"+o;
      CHECK( book( TH1D(id.c_str(), "numShared", 21, -5.5, 15.5 ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Number of shared hits on a track"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

      id=study_name+"_h_totalSiHits_"+o;
      CHECK( book( TH1D(id.c_str(), "numShared", 21, -5.5, 15.5 ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Total number of Si hits"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

      id=study_name+"_h_hassharedblayer_"+o;
      CHECK( book( TH1D(id.c_str(), "numShared", 21, -5.5, 15.5 ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("hassharedblayer"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

      id=study_name+"_h_hassharedpixel_"+o;
      CHECK( book( TH1D(id.c_str(), "numShared", 21, -5.5, 15.5 ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("hassharedpixel"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

      id=study_name+"_h_numWeightedShared_"+o;
      CHECK( book( TH1D(id.c_str(), "numShared", 21, -5.5, 15.5 ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Weighted number of shared hits on track"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");


      //pT Studies

      study_name = "TrackInformation/"+study+"/"+o+"/TrkObserverTool/pT_"+study+"_";

      id=study_name+"_h_rejectPlace_"+o;
      CHECK( book( TProfile(id.c_str(), "Reject Locations", 20, 0.0, hpT_max ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Reject Location"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

      id=study_name+"_h_ambiScore_"+o;
      CHECK( book( TProfile(id.c_str(), "Ambiguity Solver Score for Reconstructed", 20, 0.0, hpT_max ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Score"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

      id=study_name+"_h_numPixelHoles_"+o;
      CHECK( book( TProfile(id.c_str(), "numPixelHoles", 20, 0.0, hpT_max  ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Number of Pixel Holes"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

      id=study_name+"_h_numSCTHoles_"+o;
      CHECK( book( TProfile(id.c_str(), "numSCTHoles", 20, 0.0, hpT_max ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Number of SCT Holes"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

      id=study_name+"_h_numSplitSharedPixel_"+o;
      CHECK( book( TProfile(id.c_str(), "numSplitSharedPixel", 20, 0.0, hpT_max ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Number of Pixel clusters comptaible with being split that are also shared"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

      id=study_name+"_h_numSplitSharedSCT_"+o;
      CHECK( book( TProfile(id.c_str(), "numSplitSharedSCT", 20, 0.0, hpT_max) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Number of SCT clusters comptaible with being split that are also shared"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

      id=study_name+"_h_numSharedOrSplit_"+o;
      CHECK( book( TProfile(id.c_str(), "numSharedOrSplit", 20, 0.0, hpT_max ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Number of split + shared clusters"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

      id=study_name+"_h_numSharedOrSplitPixels_"+o;
      CHECK( book( TProfile(id.c_str(), "numSharedOrSplitPixels", 20, 0.0, hpT_max ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Number of pixel clusters that are either split or shared"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

      id=study_name+"_h_numShared_"+o;
      CHECK( book( TProfile(id.c_str(), "numShared", 20, 0.0, hpT_max ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Number of shared hits on a track"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

      id=study_name+"_h_totalSiHits_"+o;
      CHECK( book( TProfile(id.c_str(), "numShared", 20, 0.0, hpT_max ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Total number of Si hits"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

      id=study_name+"_h_hassharedblayer_"+o;
      CHECK( book( TProfile(id.c_str(), "numShared", 20, 0.0, hpT_max ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("hassharedblayer"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

      id=study_name+"_h_hassharedpixel_"+o;
      CHECK( book( TProfile(id.c_str(), "numShared", 20, 0.0, hpT_max ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("hassharedpixel"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

      id=study_name+"_h_numWeightedShared_"+o;
      CHECK( book( TProfile(id.c_str(), "numShared", 20, 0.0, hpT_max ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Weighted number of shared hits on track"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

      //end


      o = "From_" + origin + "_" + study;


      id = "Efficiency/"+o+"/p_recoEfficiency_pT_"+o;
      CHECK( book( TProfile(id.c_str(), ("Fraction of "+origin+" tracks that are reconstructed").c_str(), 20, 0.0, tpT_max ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Stable Particle p_{T} [GeV]"); hist(id.c_str())->GetYaxis()->SetTitle("Reconstruction Efficiency");

      id = "Efficiency/"+o+"/p_recoEfficiency_TMP_"+o;
      CHECK( book( TProfile(id.c_str(), ("Fraction of "+origin+" tracks that are reconstructed").c_str(), 50, 0.0, 1.0 ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("TMP"); hist(id.c_str())->GetYaxis()->SetTitle("Reconstruction Efficiency");

      id = "Efficiency/"+o+"/p_recoEfficiency_PR_"+o;
      CHECK( book( TProfile(id.c_str(), ("Fraction of "+origin+" tracks that are reconstructed").c_str(), PR_nbins, 0.0, PR_max ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Stable Particle Production Radius [mm]"); hist(id.c_str())->GetYaxis()->SetTitle("Reconstruction Efficiency");

      id = "Efficiency/"+o+"/p_recoEfficiency_d0_"+o;
      CHECK( book( TProfile(id.c_str(), ("Fraction of "+origin+" tracks that are reconstructed").c_str(), 10, 0.0, 10.0  ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Stable Particle d_{0} [mm]"); hist(id.c_str())->GetYaxis()->SetTitle("Reconstruction Efficiency");

      

      o = "From_" + origin;
      study_name = "TrackInformation/"+study+"/"+o+"/TrkInformation/"+study+"_";

      id=study_name+"h_numberOfContribPixelLayers_"+o;
      CHECK( book( TH1D(id.c_str(), "numShared", 21, -5.5, 15.5 ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("numWeightedShared"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

     id=study_name+"h_numberOfInnermostPixelLayerHits_"+o;
     CHECK( book( TH1D(id.c_str(), "numShared", 21, -5.5, 15.5 ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Number of IBL hits"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");
    id=study_name+"h_numberOfInnermostPixelLayerOutliers_"+o;
    CHECK( book( TH1D(id.c_str(), "numShared", 21, -5.5, 15.5 ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Number of IBL Outliers"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");
    id=study_name+"h_numberOfInnermostPixelLayerSharedHits_"+o;
    CHECK( book( TH1D(id.c_str(), "numShared", 21, -5.5, 15.5 ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Number of IBL shared hits"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");
    id=study_name+"h_numberOfInnermostPixelLayerSplitHits_"+o;
    CHECK( book( TH1D(id.c_str(), "numShared", 21, -5.5, 15.5 ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Number of IBL split hits"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

    id=study_name+"h_numberOfNextToInnermostPixelLayerHits_"+o;
    CHECK( book( TH1D(id.c_str(), "numShared", 21, -5.5, 15.5 ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Number of BL hits"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");
    id=study_name+"h_numberOfNextToInnermostPixelLayerOutliers_"+o;
    CHECK( book( TH1D(id.c_str(), "numShared", 21, -5.5, 15.5 ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Number of BL outliers"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");
    id=study_name+"h_numberOfNextToInnermostPixelLayerSharedHits_"+o;
    CHECK( book( TH1D(id.c_str(), "numShared", 21, -5.5, 15.5 ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Number of BL shared hits"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");
    id=study_name+"h_numberOfNextToInnermostPixelLayerSplitHits_"+o;
    CHECK( book( TH1D(id.c_str(), "numShared", 21, -5.5, 15.5 ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Number of BL shared split hits"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");


    id=study_name+"h_numberOfPixelHits_"+o;
    CHECK( book( TH1D(id.c_str(), "numShared", 21, -5.5, 15.5 ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Number of pixel hits"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");
    id=study_name+"h_numberOfPixelOutliers_"+o;
    CHECK( book( TH1D(id.c_str(), "numShared", 21, -5.5, 15.5 ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Number of pixel outlier"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");
    id=study_name+"h_numberOfPixelHoles_"+o;
    CHECK( book( TH1D(id.c_str(), "numShared", 21, -5.5, 15.5 ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Number of pixel holes"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");
    id=study_name+"h_numberOfPixelSharedHits_"+o;
    CHECK( book( TH1D(id.c_str(), "numShared", 21, -5.5, 15.5 ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Number of pixel shared hits"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");
    id=study_name+"h_numberOfPixelSplitHits_"+o;
    CHECK( book( TH1D(id.c_str(), "numShared", 21, -5.5, 15.5 ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Number of pixel split hits"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");
    id=study_name+"h_numberOfPixelDeadSensors_"+o;
    CHECK( book( TH1D(id.c_str(), "numShared", 21, -5.5, 15.5 ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Number of pixel dead sensor hits"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

    id=study_name+"h_numSharedTotal_"+o;
      CHECK( book( TH1D(id.c_str(), "h_numSharedTotal", 21, -5.5, 15.5 ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("totalSiHits"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");


    id=study_name+"h_numberOfSCTHits_"+o;
    CHECK( book( TH1D(id.c_str(), "numShared", 21, -5.5, 15.5 ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Number of SCT hits"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");
    id=study_name+"h_numberOfSCTOutliers_"+o;
    CHECK( book( TH1D(id.c_str(), "numShared", 21, -5.5, 15.5 ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Number of SCT outliers"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");
    id=study_name+"h_numberOfSCTHoles_"+o;
    CHECK( book( TH1D(id.c_str(), "numShared", 21, -5.5, 15.5 ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Number of SCT holes"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");
    id=study_name+"h_numberOfSCTSharedHits_"+o;
    CHECK( book( TH1D(id.c_str(), "numShared", 21, -5.5, 15.5 ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Number of SCT shared hits"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");
    // id=study_name+"h_numberOfSCTSplitHits_"+o;
    // CHECK( book( TH1D(id.c_str(), "numShared", 21, -5.5, 15.5 ) ) );
    //   hist(id.c_str())->GetXaxis()->SetTitle("numWeightedShared"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");
    id=study_name+"h_numberOfSCTDeadSensors_"+o;
    CHECK( book( TH1D(id.c_str(), "numShared", 21, -5.5, 15.5 ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Number of SCT dead sensor hits"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

    id = study_name+"h_pt_"+o;
    CHECK( book( TH1F(id.c_str(), "pt", 20, 0.0, hpT_max ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("p_{T} [GeV]"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");
    id = study_name+"h_eta_"+o;
    CHECK( book( TH1F(id.c_str(), "eta", 20, -2.5, 2.5 ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("#eta"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");
    id = study_name+"h_z0_"+o;
    CHECK( book( TH1F(id.c_str(), "numShared", 50, -60.0, 60.0) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("numWeightedShared"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");
    id = study_name+"h_d0_"+o;
    CHECK( book( TH1F(id.c_str(), "numShared", 50, -5.0, 5.0  ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("numWeightedShared"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

    id = study_name+"h_DL_"+o;
    CHECK( book( TH1F(id.c_str(), "numShared", PR_nbins, 0.0, PR_max  ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("numWeightedShared"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");


    //pT Studies

    study_name = "TrackInformation/"+study+"/"+o+"/TrkInformation/pT_"+study+"_";

      id=study_name+"h_numberOfContribPixelLayers_"+o;
      CHECK( book( TProfile(id.c_str(), "numShared", 20, 0.0, hpT_max ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("numWeightedShared"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

     id=study_name+"h_numberOfInnermostPixelLayerHits_"+o;
     CHECK( book( TProfile(id.c_str(), "numShared", 20, 0.0, hpT_max ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Number of IBL hits"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");
    id=study_name+"h_numberOfInnermostPixelLayerOutliers_"+o;
    CHECK( book( TProfile(id.c_str(), "numShared", 20, 0.0, hpT_max ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Number of IBL Outliers"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");
    id=study_name+"h_numberOfInnermostPixelLayerSharedHits_"+o;
    CHECK( book( TProfile(id.c_str(), "numShared", 20, 0.0, hpT_max ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Number of IBL shared hits"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");
    id=study_name+"h_numberOfInnermostPixelLayerSplitHits_"+o;
    CHECK( book( TProfile(id.c_str(), "numShared", 20, 0.0, hpT_max ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Number of IBL split hits"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

    id=study_name+"h_numberOfNextToInnermostPixelLayerHits_"+o;
    CHECK( book( TProfile(id.c_str(), "numShared", 20, 0.0, hpT_max ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Number of BL hits"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");
    id=study_name+"h_numberOfNextToInnermostPixelLayerOutliers_"+o;
    CHECK( book( TProfile(id.c_str(), "numShared", 20, 0.0, hpT_max ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Number of BL outliers"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");
    id=study_name+"h_numberOfNextToInnermostPixelLayerSharedHits_"+o;
    CHECK( book( TProfile(id.c_str(), "numShared", 20, 0.0, hpT_max ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Number of BL shared hits"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");
    id=study_name+"h_numberOfNextToInnermostPixelLayerSplitHits_"+o;
    CHECK( book( TProfile(id.c_str(), "numShared", 20, 0.0, hpT_max ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Number of BL shared split hits"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");


    id=study_name+"h_numberOfPixelHits_"+o;
    CHECK( book( TProfile(id.c_str(), "numShared", 20, 0.0, hpT_max ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Number of pixel hits"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");
    id=study_name+"h_numberOfPixelOutliers_"+o;
    CHECK( book( TProfile(id.c_str(), "numShared", 20, 0.0, hpT_max ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Number of pixel outlier"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");
    id=study_name+"h_numberOfPixelHoles_"+o;
    CHECK( book( TProfile(id.c_str(), "numShared", 20, 0.0, hpT_max ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Number of pixel holes"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");
    id=study_name+"h_numberOfPixelSharedHits_"+o;
    CHECK( book( TProfile(id.c_str(), "numShared", 20, 0.0, hpT_max ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Number of pixel shared hits"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");
    id=study_name+"h_numberOfPixelSplitHits_"+o;
    CHECK( book( TProfile(id.c_str(), "numShared", 20, 0.0, hpT_max ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Number of pixel split hits"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");
    id=study_name+"h_numberOfPixelDeadSensors_"+o;
    CHECK( book( TProfile(id.c_str(), "numShared", 20, 0.0, hpT_max ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Number of pixel dead sensor hits"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

    id=study_name+"h_numSharedTotal_"+o;
      CHECK( book( TProfile(id.c_str(), "h_numSharedTotal", 20, 0.0, hpT_max ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("totalSiHits"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");


    id=study_name+"h_numberOfSCTHits_"+o;
    CHECK( book( TProfile(id.c_str(), "numShared", 20, 0.0, hpT_max ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Number of SCT hits"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");
    id=study_name+"h_numberOfSCTOutliers_"+o;
    CHECK( book( TProfile(id.c_str(), "numShared", 20, 0.0, hpT_max ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Number of SCT outliers"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");
    id=study_name+"h_numberOfSCTHoles_"+o;
    CHECK( book( TProfile(id.c_str(), "numShared", 20, 0.0, hpT_max ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Number of SCT holes"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");
    id=study_name+"h_numberOfSCTSharedHits_"+o;
    CHECK( book( TProfile(id.c_str(), "numShared", 20, 0.0, hpT_max ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Number of SCT shared hits"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");
    // id=study_name+"h_numberOfSCTSplitHits_"+o;
    // CHECK( book( TProfile(id.c_str(), "numShared", 20, 0.0, hpT_max ) ) );
    //   hist(id.c_str())->GetXaxis()->SetTitle("numWeightedShared"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");
    id=study_name+"h_numberOfSCTDeadSensors_"+o;
    CHECK( book( TProfile(id.c_str(), "numShared", 20, 0.0, hpT_max ) ) );
      hist(id.c_str())->GetXaxis()->SetTitle("Number of SCT dead sensor hits"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

    id = study_name+"h_pt_"+o;
    CHECK( book( TProfile(id.c_str(), "pt", 20, 0.0, hpT_max ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("p_{T} [GeV]"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");
    id = study_name+"h_eta_"+o;
    CHECK( book( TProfile(id.c_str(), "eta", 20, -2.5, 2.5 ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("#eta"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");
    id = study_name+"h_z0_"+o;
    CHECK( book( TProfile(id.c_str(), "numShared", 50, -60.0, 60.0) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("numWeightedShared"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");
    id = study_name+"h_d0_"+o;
    CHECK( book( TProfile(id.c_str(), "numShared", 50, -5.0, 5.0  ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("numWeightedShared"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

    id = study_name+"h_DL_"+o;
    CHECK( book( TProfile(id.c_str(), "numShared", PR_nbins, 0.0, PR_max  ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("numWeightedShared"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");


      }//End studies
    }// End origins

    for (int gen = 0; gen <10; gen++)
    {
      id = "FakeSubtracks/h_GoodParent_B_"+std::to_string(gen);
    CHECK( book( TH1D(id.c_str(), id.c_str() , 2, -0.5, 1.5  ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("Bad/Good"); hist(id.c_str())->GetYaxis()->SetTitle("Tracks");
    id = "FakeSubtracks/h_BadParent_B_"+std::to_string(gen);
    CHECK( book( TH1D(id.c_str(), id.c_str() , 2, -0.5, 1.5  ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("Bad/Good"); hist(id.c_str())->GetYaxis()->SetTitle("Tracks");
    }

  // for (const std::string origin : m_origins) {
  //   auto o = "From_" + origin + "_" + study;

  //     id = "Efficiency/"+o+"/p_recoEfficiency_pT_"+o;
  //     CHECK( book( TProfile(id.c_str(), ("Fraction of "+origin+" tracks that are reconstructed").c_str(), 20, 0.0, tpT_max ) ) );
  //     hist(id.c_str())->GetXaxis()->SetTitle("Stable Particle p_{T} [GeV]"); hist(id.c_str())->GetYaxis()->SetTitle("Reconstruction Efficiency");

  //     id = "Efficiency/"+o+"/p_recoEfficiency_PR_"+o;
  //     CHECK( book( TProfile(id.c_str(), ("Fraction of "+origin+" tracks that are reconstructed").c_str(), PR_nbins, 0.0, PR_max ) ) );
  //     hist(id.c_str())->GetXaxis()->SetTitle("Stable Particle Production Radius [mm]"); hist(id.c_str())->GetYaxis()->SetTitle("Reconstruction Efficiency");

  //     id = "Efficiency/"+o+"/p_recoEfficiency_d0_"+o;
  //     CHECK( book( TProfile(id.c_str(), ("Fraction of "+origin+" tracks that are reconstructed").c_str(), 10, 0.0, 10.0  ) ) );
  //     hist(id.c_str())->GetXaxis()->SetTitle("Stable Particle d_{0} [mm]"); hist(id.c_str())->GetYaxis()->SetTitle("Reconstruction Efficiency");
  // }

for (std::string study : m_studies){
  for (const auto& hadron : m_hadrons) {
    auto o = "From_" + hadron + "_" + study;
  id = "Efficiency/"+o+"/p_recoEfficiency_pT_"+hadron+"_"+o;
  CHECK( book( TProfile(id.c_str(), ("Fraction of "+hadron+" decays that are reconstructed").c_str(), 20, 0.0, hpT_max ) ) );
  hist(id.c_str())->GetXaxis()->SetTitle(("p_{T}^{"+hadron+"} [GeV]").c_str()); hist(id.c_str())->GetYaxis()->SetTitle("Reconstruction Efficiency");

    id = "Efficiency/"+o+"/p_recoEfficiency_TMP_"+hadron+"_"+o;
  CHECK( book( TProfile(id.c_str(), ("Fraction of "+hadron+" decays that are reconstructed").c_str(), 50, 0.0, 1.0 ) ) );
  hist(id.c_str())->GetXaxis()->SetTitle(("p_{T}^{"+hadron+"} [GeV]").c_str()); hist(id.c_str())->GetYaxis()->SetTitle("Reconstruction Efficiency");

  id = "Efficiency/"+o+"/p_recoEfficiency_dR_"+o;
  CHECK( book( TProfile(id.c_str(), ("Fraction of "+hadron+" decays that are reconstructed").c_str(), dR_nbins, 0.0, dR_max ) ) );
  hist(id.c_str())->GetXaxis()->SetTitle(("Stable Particle dR to "+hadron+" Hadron").c_str()); hist(id.c_str())->GetYaxis()->SetTitle("Reconstruction Efficiency");

  id = "Efficiency/"+o+"/p_recoEfficiency_DL_"+o;
  CHECK( book( TProfile(id.c_str(), ("Fraction of "+hadron+" decays that are reconstructed").c_str(), PR_nbins, 0.0, PR_max  ) ) );
  hist(id.c_str())->GetXaxis()->SetTitle((hadron+" Hadron Decay Radius [mm]").c_str()); hist(id.c_str())->GetYaxis()->SetTitle("Reconstruction Efficiency");
  }
}

    id = "EventInfo/TMP/From_B/p_truthMatchProbability_reconstructed_From_B";
    CHECK( book( TH1D(id.c_str(), "Truth Match Probability of Tracks", 101, -0.01, 1.01 ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("Truth Match Probability"); hist(id.c_str())->GetYaxis()->SetTitle("Number of Tracks");

    id = "EventInfo/TMP/From_B/p_truthMatchProbability_SiSp_From_B";
    CHECK( book( TH1D(id.c_str(), "Truth Match Probability of Tracks", 101, -0.01, 1.01 ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("Truth Match Probability"); hist(id.c_str())->GetYaxis()->SetTitle("Number of Tracks");

    id = "EventInfo/TMP/From_B/p_truthMatchProbability_pT_reconstructed_From_B";
    CHECK( book( TProfile(id.c_str(), "Truth Match Probability of Tracks", 20, 0.0, 2000.0 ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("p_{T}^{Track}"); hist(id.c_str())->GetYaxis()->SetTitle("Truth Match Probability");

    id = "EventInfo/TMP/From_B/p_truthMatchProbability_pT_reconstructable_From_B";
    CHECK( book( TProfile(id.c_str(), "Truth Match Probability of Tracks", 20, 0.0, 2000.0 ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("p_{T}^{Track}"); hist(id.c_str())->GetYaxis()->SetTitle("Truth Match Probability");

    id = "FakeSubtracks/h_GoodParent_B";
    CHECK( book( TProfile(id.c_str(), id.c_str() , 10.0, 0.0, 10.0  ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("Generation"); hist(id.c_str())->GetYaxis()->SetTitle("Fraction of Good (TMP > 0.75) Tracks");
    id = "FakeSubtracks/h_BadParent_B";
    CHECK( book( TProfile(id.c_str(), id.c_str() , 10.0, 0.0, 10.0 ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("Generation"); hist(id.c_str())->GetYaxis()->SetTitle("Fraction of Good (TMP > 0.75) Tracks");

    id = "AmbiScore/AmbiScore_Reconstructed_B";
    CHECK( book( TH1D(id.c_str(), id.c_str() ,  201, -100.5, 100.5  ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("Difference in score"); hist(id.c_str())->GetYaxis()->SetTitle("Tracks");

    id = "SubtrackStudies/h_NumOfSubtracks_B";
    CHECK( book( TProfile(id.c_str(), id.c_str() , 1, -0.5, 0.5  ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("n/a"); hist(id.c_str())->GetYaxis()->SetTitle("Fraction of subtracks reoncstructed");

    //Possible depracated code from now on.
    id = "PseudoMatchValidation/h_numOfpseudoMatchedToReconstructable";
    CHECK( book( TH1D(id.c_str(), id.c_str() ,  10, -5, 5  ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("Number of Pseudo tracks"); hist(id.c_str())->GetYaxis()->SetTitle("Tracks");

    id = "PseudoMatchValidation/h_numOfpseudoMatchedToReconstructableClusters";
    CHECK( book( TH1D(id.c_str(), id.c_str() ,  101, -0.5, 100.5  ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("Number of Pseudo tracks"); hist(id.c_str())->GetYaxis()->SetTitle("Tracks");

    id = "PseudoMatchValidation/h_numOfpseudoMatchedToReconstructableClustersSimple";
    CHECK( book( TH1D(id.c_str(), id.c_str() ,  101, -0.5, 100.5  ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("Number of Pseudo tracks"); hist(id.c_str())->GetYaxis()->SetTitle("Tracks");    
    //Cant match
    id = "CantMatch/h_NoMatchTruthSeedIdo_subtracks";
    CHECK( book( TH1D(id.c_str(), id.c_str() ,  10, -0.5, 9.5  ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("Generation"); hist(id.c_str())->GetYaxis()->SetTitle("Tracks");

    id = "CantMatch/h_NoMatchPseudoSeedIdo";
    CHECK( book( TH1D(id.c_str(), "Number of tracks in each generation not matched to psuedo", 10, -0.5, 9.5 ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("Generation"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

    id = "GoodTracks/h_GoodTracks_reconstructed_pT_B";
    CHECK( book( TProfile(id.c_str(), "Fraction of B decays that are reconstructed Good Tracks", 20, 0.0, hpT_max ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("p_{T}^{B} [GeV]"); hist(id.c_str())->GetYaxis()->SetTitle("Frac of Good Tracks");

    id = "GoodTracks/h_GoodTracks_reconstructed_DL_B";
    CHECK( book( TProfile(id.c_str(), "Fraction of B decays that are reconstructed Good Tracks", PR_nbins, 0.0, PR_max ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("B Hadron Decay Radius [mm]"); hist(id.c_str())->GetYaxis()->SetTitle("Frac of Good Tracks");

    id = "GoodTracks/h_GoodTracks_reconstructed_dR_B";
    CHECK( book( TProfile(id.c_str(), "Fraction of B decays that are reconstructed Good Tracks", dR_nbins, 0.0, dR_max ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("Stable Particle dR to B Hadron"); hist(id.c_str())->GetYaxis()->SetTitle("Frac of Good Tracks");

    id = "GoodTracks/h_GoodTracks_reconstructable_pT_B";
    CHECK( book( TProfile(id.c_str(), "Fraction of B decays that are reconstructable Good Tracks", 20, 0.0, hpT_max ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("p_{T}^{B} [GeV]"); hist(id.c_str())->GetYaxis()->SetTitle("Frac of Good Tracks");

    id = "GoodTracks/h_GoodTracks_reconstructable_DL_B";
    CHECK( book( TProfile(id.c_str(), "Fraction of B decays that are reconstructable Good Tracks", PR_nbins, 0.0, PR_max ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("B Hadron Decay Radius [mm]"); hist(id.c_str())->GetYaxis()->SetTitle("Frac of Good Tracks");

    id = "GoodTracks/h_GoodTracks_reconstructable_dR_B";
    CHECK( book( TProfile(id.c_str(), "Fraction of B decays that are reconstructable Good Tracks", dR_nbins, 0.0, dR_max ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("Stable Particle dR to B Hadron"); hist(id.c_str())->GetYaxis()->SetTitle("Frac of Good Tracks");

    id = "Counts/h_pseudo_pT_B";
    CHECK( book( TH1D(id.c_str(), "h_pseudo_pT_B", 20, 0.0, hpT_max) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("h_pseudo_pT_B"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");
    id = "Counts/h_seeded_pT_B";
    CHECK( book( TH1D(id.c_str(), "h_seeded_pT_B", 20, 0.0, hpT_max) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("h_seeded_pT_B"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");
    id = "Counts/h_reconstructed_pT_B";
    CHECK( book( TH1D(id.c_str(), "h_reconstructed_pT_B", 20, 0.0, hpT_max ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("h_reconstructed_pT_B"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

    id = "idoToObsMatched";
    CHECK( book( TH1D(id.c_str(), "h_seeded_pT_B", 101, -0.5, 100.5) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("Number of Shared IDO-OBS"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

    id = "numOfSubtracks";
    CHECK( book( TH1D(id.c_str(), "numOfSubtracks", 101, -0.5, 100.5) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("numOfSubtracks"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

  } // End of fast mode

  return StatusCode::SUCCESS;
}

std::vector< SeedFamily > AmbiguitySolverAnalyserAlg::createSeedFamily(const xAOD::TrackParticleContainer* obsTracks) {
  std::vector< SeedFamily > allSeeds;
  // N.B. One parent per child subtrack. Validated
  for (const auto track : *obsTracks) {
    int num_of_subtracks = 0;
    // Validation
    if (!fast_mode){
      obsTracksInformation(track, "All", "AllObsTracks");
      validation_Test_Id.push_back((long int)(a_Id( *track ) ));
      if(a_parId( *track ) == 0){ num_parents++;
      } else { validation_Test_parId.push_back((long int)(a_parId( *track ) ) ); }
    }

    //Only look at SiSp original tracks in TrkObsTool TrackCollection
    if ( a_parId( *track ) != 0 ) { continue; }

    SeedFamily seed = SeedFamily();
    int gen = 0; 

    seed.descendants.push_back(track);
    long int ids_temp = a_Id( *track );

    while(gen < 50){
      gen++;

      for ( const auto otrk : *obsTracks) {
        //Loop through ids already stored
        if ( a_parId( *otrk ) == ids_temp ) {
          seed.descendants.push_back(otrk);
          ids_temp =  a_Id(*otrk);
          num_of_subtracks = num_of_subtracks + 1;
        }
      }
      if (!ids_temp) break;
    }
    hist("numOfSubtracks")->Fill( num_of_subtracks );
    allSeeds.push_back( seed ); 
  } //End of Obs loop
  return allSeeds;
}



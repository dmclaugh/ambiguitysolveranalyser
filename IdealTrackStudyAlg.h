#ifndef TRACKTRACKSTUDY_IDEALTRACKSTUDYALG_H
#define TRACKTRACKSTUDY_IDEALTRACKSTUDYALG_H

// #include "trackAnalysisAlg.h"
#include "xAODTracking/Vertex.h"
#include "xAODJet/JetContainer.h"

#include "GaudiKernel/ITHistSvc.h"

namespace InDet {
  class IInDetTrackTruthOriginTool;
  class IInDetTrackSelectionTool;
}
class IJetCalibrationTool;

// Inherit from trackAnalysisAlg to get all the nice functions already there
class IdealTrackStudyAlg: public ::trackAnalysisAlg { 
 public: 
  IdealTrackStudyAlg( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~IdealTrackStudyAlg(); 

  ///uncomment and implement methods as required

                                        //IS EXECUTED:
  virtual StatusCode  initialize();     //once, before any input is loaded
  //virtual StatusCode  firstExecute();   //once, after first eventdata is loaded (not per file)
  virtual StatusCode  execute();        //per event
  //virtual StatusCode  endInputFile();   //end of each input file
  //virtual StatusCode  metaDataStop();   //when outputMetaStore is populated by MetaDataTools
  virtual StatusCode  finalize();       //once, after all events processed
  

  ///Other useful methods provided by base class are:
  ///evtStore()        : ServiceHandle to main event data storegate
  ///inputMetaStore()  : ServiceHandle to input metadata storegate
  ///outputMetaStore() : ServiceHandle to output metadata storegate
  ///histSvc()         : ServiceHandle to output ROOT service (writing TObjects)
  ///currentFile()     : TFile* to the currently open input file
  ///retrieveMetadata(...): See twiki.cern.ch/twiki/bin/view/AtlasProtected/AthAnalysisBase#ReadingMetaDataInCpp


 private: 

  ServiceHandle<ITHistSvc> m_thistSvc{this, "THistSvc", "THistSvc"};

  // hit pattern and residual hists
  StatusCode bookEventHistograms();
  StatusCode bookPullHistograms();
  StatusCode bookRateHistograms();
  StatusCode bookResidualHists( const std::string trkColName, const std::string type );
  StatusCode bookHitPatternHists( const std::string trkColName, const std::string type );
  StatusCode bookTrkPResidualHists( const std::string trkColName, const std::string type );
  StatusCode bookRateHists( const std::string trkColName, const std::string type, bool useTruth );

  StatusCode fillResiduals( const xAOD::TrackParticleContainer *tracks, const std::string trkColName);
  StatusCode fillResiduals( const xAOD::TrackParticle *track, 
      const std::string trkColName, const std::string type);
  StatusCode fillHitPatterns( const xAOD::TrackParticleContainer *tracks, const std::string trkColName);
  StatusCode fillHitPatterns( const xAOD::TrackParticle *track, 
      const std::string trkColName, const std::string type);
  StatusCode fillTrkPResiduals( const xAOD::TrackParticleContainer *tracks, const std::string trkColName);
  StatusCode fillTrkPResiduals( const xAOD::TrackParticle *track, 
      const std::string trkColName, const std::string type);

  StatusCode fillRateHists( const xAOD::TrackParticleContainer *tracks, const std::string trkColName);
  StatusCode fillRateHists( const xAOD::TrackParticle *track,
      const std::string trkColName, const std::string type,
      bool fillReco, bool isFake);
  StatusCode fillRateHists( const xAOD::TrackParticle *track,
      const std::string trkColName, const std::string type,
      bool useTruth);

  StatusCode fillRateHists( const xAOD::TruthParticleContainer *particles, 
      const std::string truColName);
  StatusCode fillRateHists( const xAOD::TruthParticle *truth, 
      const std::string truColName, const std::string type );


  // z0 bias hists
  StatusCode bookZ0BiasHistograms();
  StatusCode fillZ0Bias( const xAOD::TrackParticle *track, const std::string trkColName );



  bool isFrom(const xAOD::TruthParticle* part, int flav);
  std::string getOriginLabel( int origin );


  // some information is not saved in the AOD - recover it here!
  bool SolveForErrorsAndLocations(float rbx, float rby, float pbx, float pby, float rux, float ruy, float pux, float puy, float trkbx, float trkby, float trkux, float trkuy,
    bool firstPass,
    float& trkBiasedCovXY, float& trkBiasedCovXX, float& trkBiasedCovYY,
    float& clusCovXX, float& clusCovYY, 
    float& trkUnbiasedCovXX, float& trkUnbiasedCovYY);
  std::pair<float,float> GetTrackBiasedCovXY(float rbx, float rby, float pbx, float pby, float rux, float pux, float trkbx, float trkux);
  float GetTrackBiasedCovXX(float rbx, float rby, float pbx, float pby, float trkbx, float trkux, float covTrkbxy);
  float GetTrackBiasedCovYY(float rbx, float rby, float pbx, float pby, float trkby, float trkuy, float covTrkbxy);
  float GetClusterCovXX(float rbx, float pbx, float covTrkbxx);
  float GetClusterCovYY(float rby, float pby, float covTrkbyy);
  float GetTrackUnbiasedCovXX(float rbx, float rux, float pbx, float pux, float covTrkbxx);
  float GetTrackUnbiasedCovYY(float rby, float ruy, float pby, float puy, float covTrkbyy);

  // copied from pub note code
  ConstDataVector< xAOD::TruthParticleContainer > GetPrimaryTruth( const xAOD::TruthParticleContainer* truthParts );
  // store barcode of either pseudo-tracks or ideal-tracks
  StatusCode GetListOfReconstructableBarcodes( const xAOD::TrackParticleContainer* tracks );
  // were hits correctly shared or correctly split
  StatusCode DecorateSplitShareQual( const xAOD::TrackParticleContainer* tracks, int trkColNum );
  // decorate each track with the minDR to a jet 
  StatusCode DecorateMinDR( const xAOD::TruthParticleContainer* particles, const xAOD::JetContainer* jets);
  StatusCode DecorateMinDR( const xAOD::TrackParticleContainer* tracks, const xAOD::JetContainer* jets);

  ToolHandle<InDet::IInDetTrackTruthOriginTool> m_trackOriginTool;
  ToolHandle<InDet::IInDetTrackSelectionTool>   m_trackSelTool;
  ToolHandle< IJetCalibrationTool > m_jetCalibrationTool;

  int m_eventCounter;
  int m_recoCounter;
  int m_pseudoCounter;
  int m_idealCounter;

  bool m_applyTrackSelection;
  bool m_runPulls;
  bool m_hasIdeal;
  bool m_hasJets;
  bool m_runRates;
  bool m_runZ0Bias;
  bool m_studyMatchedTracks;
  bool m_addLargeD0Trks;

  int m_nJetCut;

  float m_pvZ0;
  float m_trackPtMin;
  float m_jetPtMin;

  std::string m_streamName;

  std::vector<int> m_canRecoBarcodes;

  std::multimap< const xAOD::TrackMeasurementValidation*, const xAOD::TrackParticle* >* m_recoClusterMap;
  std::multimap< const xAOD::TrackMeasurementValidation*, const xAOD::TrackParticle* >* m_pseudoClusterMap;
  std::multimap< const xAOD::TrackMeasurementValidation*, const xAOD::TrackParticle* >* m_idealClusterMap;

  const xAOD::Vertex* m_primaryVertex;

}; 

#endif //> !TRACKTRACKSTUDY_IDEALTRACKSTUDYALG_H

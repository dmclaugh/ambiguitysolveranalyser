#!/bin/bash

setupATLAS
lsetup git 
cd build
acmSetup
acm compile
cd ../run
acm compile; nohup athena AmbiguitySolverAnalyser/AmbiguitySolverAnalyserAlgJobOptions.py > logs/AmbiAnalyserLogs.log

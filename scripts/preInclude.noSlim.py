from ParticleBuilderOptions.AODFlags import AODFlags
AODFlags.TrackParticleSlimmer=False 
AODFlags.TrackParticleLastHitAndPerigeeSlimmer=False
AODFlags.ThinGeantTruth=False

# deactivate slimming 
InDetFlags.doSlimming.set_Value_and_Lock                           (False)
